# Adatrix Android SDK

Thanks for taking a look at Adatrix Android SDK. Adatrix is a scalable mobile ad platform and audience data monetization solution, this library allows you to integrate Adatrix into your Android app.

Learn more about the provided samples, documentation, integrating the SDK into your app, accessing source code, and more at http://wiki.adatrix.com/display/DOC/Android+SDK

## Download

The Adatrix Android SDK is distributed as jar file which can included as library or as source code that you can include in your application.

## Integrate

Integration instructions are available on the [wiki](http://wiki.adatrix.com/display/DOC/Integrating+Android+SDK).

## New in this Version


## Requirements

- Android 2.3.1 (API Version 9) and up
- android-support-v4.jar
- android-support-annotations.jar

## License

Copyright (c) 2015-present, Adatrix. All rights reserved.

You are hereby granted a non-exclusive, worldwide, royalty-free license to use, copy, modify, and distribute this software in source code or binary form for use in connection with the web services and APIs provided by Adatrix.

As with any software that integrates with the Adatrix platform, your use of this software is subject to the Adatrix Platform Policies [http://wiki.adatrix.com/display/DOC/Adatrix+Platform+Policy]. This copyright notice shall be included in all copies or substantial portions of the software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.