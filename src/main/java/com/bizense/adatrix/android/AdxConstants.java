package com.bizense.adatrix.android;

public class AdxConstants {
    public static final String HOST_URL = "adx1.exchange.adatrix.com";
	public static final String HTML_RESPONSE_BODY_KEY = "Html-Response-Body";
    public static final String REDIRECT_URL_KEY = "Redirect-Url";
    public static final String CLICKTHROUGH_URL_KEY = "Clickthrough-Url";
    public static final String SCROLLABLE_KEY = "Scrollable";
    public static final String AD_CONFIGURATION_KEY = "Ad-Configuration";
    public static final String SDK_VERSION = "1.0";
    
    public static final int SOCKET_SIZE = 8192;
}
