package com.bizense.adatrix.android.video;

import java.util.ArrayList;
import java.util.Map;

import android.net.Uri;
import android.util.Log;

import com.bizense.adatrix.android.core.InterstitialListener;
import com.bizense.adatrix.android.html.ResponseBodyInterstitial;
import com.bizense.adatrix.android.managers.ListManager;
import com.bizense.adatrix.android.mraid.MraidVideoPlayerActivity;
import com.bizense.adatrix.android.network.AdFetcher;
import com.bizense.adatrix.android.utils.DiskLRUCache;
import com.bizense.adatrix.android.utils.ErrorCode;

class VastVideoInterstitial extends ResponseBodyInterstitial implements VastManager.VastManagerListener, VastVideoDownloadTask.OnDownloadCompleteListener {
    public static final int CACHE_MAX_SIZE = 100 * 1000 * 1000;
    public static final String VIDEO_CACHE_DIRECTORY_NAME = "adx_vast_video_cache";
    private InterstitialListener mInterstitialListener;
    private VastVideoDownloadTask mVastVideoDownloadTask;
    private DiskLRUCache mVideoCache;
    private String mVastResponse;
    private String mVideoUrl;
    private VastManager mVastManager;
    private ArrayList<String> mVideoStartTrackers;
    private ArrayList<String> mVideoFirstQuartileTrackers;
    private ArrayList<String> mVideoMidpointTrackers;
    private ArrayList<String> mVideoThirdQuartileTrackers;
    private ArrayList<String> mVideoCompleteTrackers;
    private ArrayList<String> mImpressionTrackers;
    private String mClickThroughUrl;
    private ArrayList<String> mClickTrackers;

    @Override
    protected void extractExtras(Map<String, String> serverExtras) {
        mVastResponse = Uri.decode(serverExtras.get(AdFetcher.HTML_RESPONSE_BODY_KEY));
    }

    @Override
    public void preRenderHtml(InterstitialListener interstitialListener) {
        mInterstitialListener = interstitialListener;

        if (mVideoCache == null) {
            try {
                mVideoCache = new DiskLRUCache(mContext, VIDEO_CACHE_DIRECTORY_NAME, CACHE_MAX_SIZE);
            } catch (Exception e) {
                Log.d("Adatrix", "Unable to create VAST video cache.");
                mInterstitialListener.onInterstitialFailed(ErrorCode.VIDEO_CACHE_ERROR);
                return;
            }
        }

        mVastManager = new VastManager();
        mVastManager.processVast(mVastResponse, this);
    }

    @Override
    public void showInterstitial() {
        MraidVideoPlayerActivity.startVast(mContext,
                mVideoUrl,
                mVideoStartTrackers,
                mVideoFirstQuartileTrackers,
                mVideoMidpointTrackers,
                mVideoThirdQuartileTrackers,
                mVideoCompleteTrackers,
                mImpressionTrackers,
                mClickThroughUrl,
                mClickTrackers
        );
    }

    @Override
    public void onInvalidate() {
        if (mVastManager != null) {
            mVastManager.cancel();
        }

        super.onInvalidate();
    }

    /*
     * VastManager.VastManagerListener implementation
     */

    @Override
    public void onComplete(VastManager vastManager) {
        mVideoUrl = vastManager.getMediaFileUrl();

        Uri uri = mVideoCache.getUri(mVideoUrl);
        if (uri != null) {
            onDownloadSuccess();
        } else {
            mVastVideoDownloadTask = VastManager.createDownloadTask(this, mVideoCache);
            mVastVideoDownloadTask.execute(mVideoUrl);
        }
    }

    /*
     * VastVideoDownloadTask.OnDownloadCompleteListener implementation
     */

    @Override
    public void onDownloadSuccess() {
        mVideoStartTrackers = ListManager.asStringArrayList(mVastManager.getVideoStartTrackers());
        mVideoFirstQuartileTrackers = ListManager.asStringArrayList(mVastManager.getVideoFirstQuartileTrackers());
        mVideoMidpointTrackers = ListManager.asStringArrayList(mVastManager.getVideoMidpointTrackers());
        mVideoThirdQuartileTrackers = ListManager.asStringArrayList(mVastManager.getVideoThirdQuartileTrackers());
        mVideoCompleteTrackers = ListManager.asStringArrayList(mVastManager.getVideoCompleteTrackers());

        mImpressionTrackers = ListManager.asStringArrayList(mVastManager.getImpressionTrackers());

        mClickThroughUrl = mVastManager.getClickThroughUrl();
        mClickTrackers = ListManager.asStringArrayList(mVastManager.getClickTrackers());

        mInterstitialListener.onInterstitialLoaded();
    }

    @Override
    public void onDownloadFailed() {
        mInterstitialListener.onInterstitialFailed(ErrorCode.VIDEO_DOWNLOAD_ERROR);
    }
    
    public DiskLRUCache getVideoCache(){
    	return mVideoCache;
    }
    
    public String getVastResponse(){
    	return mVastResponse;
    }
    
    public void setVastManager(VastManager vastManager){
    	mVastManager = vastManager;
    }
}
