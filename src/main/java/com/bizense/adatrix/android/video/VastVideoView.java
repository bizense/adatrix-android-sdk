package com.bizense.adatrix.android.video;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.bizense.adatrix.android.core.AbstractVideoView;
import com.bizense.adatrix.android.managers.HttpClientManager;
import com.bizense.adatrix.android.mraid.MraidBrowser;
import com.bizense.adatrix.android.mraid.MraidVideoPlayerActivity;

public class VastVideoView extends AbstractVideoView {
    public static final String VIDEO_START_TRACKERS = "video_start_trackers";
    public static final String VIDEO_FIRST_QUARTER_TRACKERS = "video_first_quarter_trackers";
    public static final String VIDEO_MID_POINT_TRACKERS = "video_mid_point_trackers";
    public static final String VIDEO_THIRD_QUARTER_TRACKERS = "video_third_quarter_trackers";
    public static final String VIDEO_COMPLETE_TRACKERS = "video_complete_trackers";
    public static final String VIDEO_IMPRESSION_TRACKERS = "video_impression_trackers";
    public static final String VIDEO_CLICK_THROUGH_URL = "video_click_through_url";
    public static final String VIDEO_CLICK_THROUGH_TRACKERS = "video_click_through_trackers";

    private static final float FIRST_QUARTER_MARKER = 0.25f;
    private static final float MID_POINT_MARKER = 0.50f;
    private static final float THIRD_QUARTER_MARKER = 0.75f;
    private static final long VIDEO_PROGRESS_TIMER_CHECKER_DELAY = 50;

    private static final ThreadPoolExecutor sThreadPoolExecutor = new ThreadPoolExecutor(10, 50, 1, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
    public static final int MAX_VIDEO_DURATION_FOR_CLOSE_BUTTON = 15 * 1000;
    public static final int DEFAULT_VIDEO_DURATION_FOR_CLOSE_BUTTON = 5 * 1000;
    private final VideoViewListener mVideoViewListener;

    private String mVideoUrl;
    private ArrayList<String> mVideoStartTrackers;
    private ArrayList<String> mFirstQuarterTrackers;
    private ArrayList<String> mMidPointTrackers;
    private ArrayList<String> mThirdQuarterTrackers;
    private ArrayList<String> mCompletionTrackers;
    private ArrayList<String> mImpressionTrackers;
    private String mClickThroughUrl;
    private ArrayList<String> mClickThroughTrackers;
    private Handler mHandler;
    private Runnable mVideoProgressCheckerRunnable;
    private boolean mIsVideoProgressShouldBeChecked;
    private int mShowCloseButtonDelay = DEFAULT_VIDEO_DURATION_FOR_CLOSE_BUTTON;

    private boolean mIsFirstMarkHit;
    private boolean mIsSecondMarkHit;
    private boolean mIsThirdMarkHit;
    private int mSeekerPositionOnPause;
    private boolean mIsVideoFinishedPlaying;

    public VastVideoView(final Context context, final Intent intent, final VideoViewListener videoViewListener) {
        super(context);

        mVideoViewListener = videoViewListener;
        mHandler = new Handler();
        mIsVideoProgressShouldBeChecked = true;
        mSeekerPositionOnPause = -1;

        mVideoUrl = intent.getStringExtra(MraidVideoPlayerActivity.VIDEO_URL);
        mVideoStartTrackers = intent.getStringArrayListExtra(VIDEO_START_TRACKERS);
        mFirstQuarterTrackers = intent.getStringArrayListExtra(VIDEO_FIRST_QUARTER_TRACKERS);
        mMidPointTrackers = intent.getStringArrayListExtra(VIDEO_MID_POINT_TRACKERS);
        mThirdQuarterTrackers = intent.getStringArrayListExtra(VIDEO_THIRD_QUARTER_TRACKERS);
        mCompletionTrackers = intent.getStringArrayListExtra(VIDEO_COMPLETE_TRACKERS);
        mImpressionTrackers = intent.getStringArrayListExtra(VIDEO_IMPRESSION_TRACKERS);
        mClickThroughUrl = intent.getStringExtra(VIDEO_CLICK_THROUGH_URL);
        mClickThroughTrackers = intent.getStringArrayListExtra(VIDEO_CLICK_THROUGH_TRACKERS);

        setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                stopProgressChecker();
                if (mVideoViewListener != null) {
                    mVideoViewListener.videoCompleted(false);
                }
                pingOnBackgroundThread(mCompletionTrackers);

                mIsVideoFinishedPlaying = true;
            }
        });

        setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
                stopProgressChecker();

                if (videoViewListener != null) {
                    videoViewListener.videoError(false);
                }

                return false;
            }
        });

        setVideoPath(mVideoUrl);
        requestFocus();

        setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    pingOnBackgroundThread(mClickThroughTrackers);

                    if (mVideoViewListener != null) {
                        mVideoViewListener.videoClicked();
                    }

                    Intent mraidBrowserIntent = new Intent(context, MraidBrowser.class);
                    mraidBrowserIntent.putExtra(MraidBrowser.URL_EXTRA, mClickThroughUrl);
                    context.startActivity(mraidBrowserIntent);
                }

                return true;
            }
        });

        mVideoProgressCheckerRunnable = new Runnable() {
            @Override
            public void run() {
                float videoLength = getDuration();
                if (videoLength > 0) {
                    float progressPercentage = getCurrentPosition() / videoLength;

                    if (progressPercentage > FIRST_QUARTER_MARKER && !mIsFirstMarkHit) {
                        mIsFirstMarkHit = true;
                        pingOnBackgroundThread(mFirstQuarterTrackers);
                    }

                    if (progressPercentage > MID_POINT_MARKER && !mIsSecondMarkHit) {
                        mIsSecondMarkHit = true;
                        pingOnBackgroundThread(mMidPointTrackers);
                    }

                    if (progressPercentage > THIRD_QUARTER_MARKER && !mIsThirdMarkHit) {
                        mIsThirdMarkHit = true;
                        pingOnBackgroundThread(mThirdQuarterTrackers);
                    }

                    if (getCurrentPosition() > mShowCloseButtonDelay) {
                        if (mVideoViewListener != null) {
                            mVideoViewListener.showCloseButton();
                        }
                    }
                }

                if (mIsVideoProgressShouldBeChecked) {
                    mHandler.postDelayed(mVideoProgressCheckerRunnable, VIDEO_PROGRESS_TIMER_CHECKER_DELAY);
                }
            }
        };

        setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                if(getDuration() < MAX_VIDEO_DURATION_FOR_CLOSE_BUTTON) {
                    mShowCloseButtonDelay = getDuration();
                }
            }
        });

        pingOnBackgroundThread(mVideoStartTrackers);
        pingOnBackgroundThread(mImpressionTrackers);

        mHandler.post(mVideoProgressCheckerRunnable);
    }

    @Override
    public void onPause() {
        super.onPause();
        stopProgressChecker();

        mSeekerPositionOnPause = getCurrentPosition();
    }

    @Override
    public void onResume() {
        super.onResume();

        mIsVideoProgressShouldBeChecked = true;
        mHandler.post(mVideoProgressCheckerRunnable);

        seekTo(mSeekerPositionOnPause);

        if (!mIsVideoFinishedPlaying) {
            start();
        }
    }

    private void pingOnBackgroundThread(List<String> urls) {
        if (urls == null) {
            return;
        }

        for (final String url : urls) {
            sThreadPoolExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        HttpClientManager.ping(url);
                    } catch (Exception e) {
                        Log.d("Adatrix", "Unable to track video impression url: " + url);
                    }
                }
            });
        }
    }

    private void stopProgressChecker() {
        mIsVideoProgressShouldBeChecked = false;
        mHandler.removeCallbacks(mVideoProgressCheckerRunnable);
    }
}
