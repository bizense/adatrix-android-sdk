package com.bizense.adatrix.android.managers;

import java.util.concurrent.Executor;

import android.os.AsyncTask;

import com.bizense.adatrix.android.core.AdViewController;
import com.bizense.adatrix.android.network.AdFetchTask;
import com.bizense.adatrix.android.network.TaskTracker;
import com.bizense.adatrix.android.utils.AndroidVersion;
import com.bizense.adatrix.android.utils.Reflection.MethodBuilder;

public class AsyncTaskManager {
    public static <P> void safeExecuteOnExecutor(AsyncTask<P, ?, ?> asyncTask, P... params) throws Exception {
        if (asyncTask == null) {
            throw new IllegalArgumentException("Unable to execute null AsyncTask.");
        }

        if (AndroidVersion.currentApiLevel().isAtLeast(AndroidVersion.ICE_CREAM_SANDWICH)) {
            Executor threadPoolExecutor = (Executor) AsyncTask.class.getField("THREAD_POOL_EXECUTOR").get(AsyncTask.class);

            new MethodBuilder(asyncTask, "executeOnExecutor")
                    .addParam(Executor.class, threadPoolExecutor)
                    .addParam(Object[].class, params)
                    .execute();
        } else {
            asyncTask.execute(params);
        }
    }
    
    public static AdFetchTask createAdFetchTask(TaskTracker taskTracker, AdViewController adViewController, String userAgent, int timeoutMilliseconds) {
        return new AdFetchTask(taskTracker, adViewController, userAgent, timeoutMilliseconds);
    }
}
