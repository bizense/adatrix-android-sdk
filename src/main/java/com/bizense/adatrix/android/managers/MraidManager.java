package com.bizense.adatrix.android.managers;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Environment.MEDIA_MOUNTED;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.mraid.MraidVideoPlayerActivity;
import com.bizense.adatrix.android.mraid.MraidView;
import com.bizense.adatrix.android.mraid.MraidView.NativeCloseButtonStyle;
import com.bizense.adatrix.android.utils.AndroidVersion;
import com.bizense.adatrix.android.utils.Device;

public class MraidManager {
    public static final String ANDROID_CALENDAR_CONTENT_TYPE = "vnd.android.cursor.item/event";

    public static boolean isTelAvailable(Context context) {
        Intent telIntent = new Intent(Intent.ACTION_DIAL);
        telIntent.setData(Uri.parse("tel:"));

        return Device.canHandleIntent(context, telIntent);
    }

    public static boolean isSmsAvailable(Context context) {
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.setData(Uri.parse("sms:"));

        return Device.canHandleIntent(context, smsIntent);
    }

    public static boolean isStorePictureSupported(Context context) {
        return MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                && context.checkCallingOrSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isCalendarAvailable(Context context) {
        Intent calendarIntent = new Intent(Intent.ACTION_INSERT).setType(ANDROID_CALENDAR_CONTENT_TYPE);

        return AndroidVersion.currentApiLevel().isAtLeast(AndroidVersion.ICE_CREAM_SANDWICH)
                && Device.canHandleIntent(context, calendarIntent);
    }

    public static boolean isInlineVideoAvailable(Context context) {
        Intent mraidVideoIntent = new Intent(context, MraidVideoPlayerActivity.class);

        return Device.canHandleIntent(context, mraidVideoIntent);
    }
    
    public static MraidView create(Context context, AdxConfiguration adxConfiguration) {
        return new MraidView(context, adxConfiguration);
    }
    
    public static MraidView create(Context context,
            AdxConfiguration adxConfiguration,
            MraidView.ExpansionStyle expansionStyle,
            NativeCloseButtonStyle buttonStyle,
            MraidView.PlacementType placementType) {
        return new MraidView(context, adxConfiguration, expansionStyle, buttonStyle, placementType);
    }
}
