package com.bizense.adatrix.android.managers;

import java.util.*;

public class ListManager {
    public static ArrayList<String> asStringArrayList(final List<String> list) {
        ArrayList<String> result = new ArrayList<String>();

        if (list == null) {
            return result;
        }

        if (list instanceof ArrayList) {
            return (ArrayList<String>) list;
        }

        Collections.copy(list, result);
        return result;
    }
}
