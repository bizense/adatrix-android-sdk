package com.bizense.adatrix.android.managers;

import java.io.IOException;
import java.util.Arrays;

import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.cookie.ClientCookie;
import org.apache.http.cookie.CookieSpec;
import org.apache.http.cookie.CookieSpecFactory;
import org.apache.http.cookie.MalformedCookieException;
import org.apache.http.cookie.SetCookie;
import org.apache.http.cookie.params.CookieSpecPNames;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicExpiresHandler;
import org.apache.http.impl.cookie.BrowserCompatSpec;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.text.TextUtils;

public class HttpClientManager {
	public static void safeShutdown(final HttpClient httpClient) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				if (httpClient != null && httpClient.getConnectionManager() != null) {
					httpClient.getConnectionManager().shutdown();
				}
			}
		}).start();
	}

	public static HttpResponse ping(String url) throws IOException, IllegalArgumentException {
		if (url == null) {
			throw new IllegalArgumentException("Url must not be null.");
		}

		HttpGet request = new HttpGet(url);
		DefaultHttpClient defaultHttpClient = HttpClientManager.create();
		return defaultHttpClient.execute(request);
	}

	public static final int SOCKET_SIZE = 8192;

	public static DefaultHttpClient create() {
		return create(0);
	}

	private static CookieStore cookieStore = new BasicCookieStore();
	public static DefaultHttpClient create(int timeoutMilliseconds) {
		HttpParams httpParameters = new BasicHttpParams();

		if (timeoutMilliseconds > 0) {
			// Set timeouts to wait for connection establishment / receiving data.
			HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutMilliseconds);
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutMilliseconds);
		}

		// Set the buffer size to avoid OutOfMemoryError exceptions on certain HTC devices.
		// http://stackoverflow.com/questions/5358014/android-httpclient-oom-on-4g-lte-htc-thunderbolt
		HttpConnectionParams.setSocketBufferSize(httpParameters, SOCKET_SIZE);
		httpParameters.setParameter(CookieSpecPNames.DATE_PATTERNS, 
				Arrays.asList("EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd MMM yyyy HH:mm:ss z"));
		
		DefaultHttpClient client = new DefaultHttpClient(httpParameters);
		client.setCookieStore(cookieStore);
		client.getCookieSpecs().register("lenient", new CookieSpecFactory() {
			public CookieSpec newInstance(HttpParams params) {
				return new LenientCookieSpec();
			}
		});
		HttpClientParams.setCookiePolicy(client.getParams(), "lenient");		
		return client;
	}

	static class LenientCookieSpec extends BrowserCompatSpec {
		public LenientCookieSpec() {
			super();
			registerAttribHandler(ClientCookie.EXPIRES_ATTR, new BasicExpiresHandler(DATE_PATTERNS) {
				@Override public void parse(SetCookie cookie, String value) throws MalformedCookieException {
					if (TextUtils.isEmpty(value)) {
						// You should set whatever you want in cookie
						cookie.setExpiryDate(null);
					} else {
						super.parse(cookie, value);
					}
				}
			});
		}
	}
}
