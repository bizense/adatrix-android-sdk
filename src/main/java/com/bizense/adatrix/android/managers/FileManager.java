package com.bizense.adatrix.android.managers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class FileManager {
	public static File createDirectory(String absolutePath) {
		if (absolutePath == null) {
			return null;
		}

		File directory = new File(absolutePath);

		if (directory.exists() && directory.isDirectory() ||
				directory.mkdirs() && directory.isDirectory()) {
			return directory;
		}

		return null;
	}

	public static int intLength(File file) {
		if (file == null) {
			return 0;
		}

		long length = file.length();

		if (length < Integer.MAX_VALUE) {
			return (int) length;
		} else {
			return Integer.MAX_VALUE;
		}
	}
	public static void copyFile(String sourceFile, String destinationFile) {
		try {
			StreamManager.copyContent(new FileInputStream(sourceFile), new FileOutputStream(destinationFile));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
