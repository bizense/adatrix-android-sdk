package com.bizense.adatrix.android.model;

public class AdTypeMedia extends AdxEntity{
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 8085756118641470580L;
	private String adTypeMediaName;
	private TextMedia textMedia;
	private Media media;
	/**
	 * @return the adTypeMediaName
	 */
	public String getAdTypeMediaName() {
		return adTypeMediaName;
	}
	/**
	 * @param adTypeMediaName the adTypeMediaName to set
	 */
	public void setAdTypeMediaName(String adTypeMediaName) {
		this.adTypeMediaName = adTypeMediaName;
	}
	/**
	 * @return the textMedia
	 */
	public TextMedia getTextMedia() {
		return textMedia;
	}
	/**
	 * @param textMedia the textMedia to set
	 */
	public void setTextMedia(TextMedia textMedia) {
		this.textMedia = textMedia;
	}
	/**
	 * @return the media
	 */
	public Media getMedia() {
		return media;
	}
	/**
	 * @param media the media to set
	 */
	public void setMedia(Media media) {
		this.media = media;
	}
}
