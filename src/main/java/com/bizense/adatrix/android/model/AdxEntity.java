package com.bizense.adatrix.android.model;

import java.io.Serializable;

public class AdxEntity implements Serializable{
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -9179962330677539987L;
	private String id;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
}
