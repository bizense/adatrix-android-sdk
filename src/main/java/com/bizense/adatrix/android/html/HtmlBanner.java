package com.bizense.adatrix.android.html;

import java.util.Map;

import android.content.Context;
import android.net.Uri;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.core.AbstractBanner;
import com.bizense.adatrix.android.core.AdViewController;
import com.bizense.adatrix.android.core.BannerListener;
import com.bizense.adatrix.android.utils.ErrorCode;

public class HtmlBanner extends AbstractBanner {

    private HtmlBannerWebView mHtmlBannerWebView;

    @Override
    protected void loadBanner(
            Context context,
            BannerListener bannerListener,
            Map<String, Object> localExtras,
            Map<String, String> serverExtras) {

        String htmlData;
        String redirectUrl;
        String clickthroughUrl;
        Boolean isScrollable;
        if (extrasAreValid(serverExtras)) {
            htmlData = Uri.decode(serverExtras.get(AdxConstants.HTML_RESPONSE_BODY_KEY));
            redirectUrl = serverExtras.get(AdxConstants.REDIRECT_URL_KEY);
            clickthroughUrl = serverExtras.get(AdxConstants.CLICKTHROUGH_URL_KEY);
            isScrollable = Boolean.valueOf(serverExtras.get(AdxConstants.SCROLLABLE_KEY));
        } else {
            bannerListener.onBannerFailed(ErrorCode.NETWORK_INVALID_STATE);
            return;
        }

        AdxConfiguration adxConfiguration = AdxConfiguration.extractFromMap(localExtras);
        mHtmlBannerWebView = new HtmlBannerWebView(context, adxConfiguration);
        mHtmlBannerWebView.init(bannerListener, isScrollable, redirectUrl, clickthroughUrl);
        AdViewController.setShouldHonorServerDimensions(mHtmlBannerWebView);
        mHtmlBannerWebView.loadHtmlResponse(htmlData);
    }

    @Override
    protected void onInvalidate() {
        if (mHtmlBannerWebView != null) {
            mHtmlBannerWebView.destroy();
        }
    }

    private boolean extrasAreValid(Map<String, String> serverExtras) {
        return serverExtras.containsKey(AdxConstants.HTML_RESPONSE_BODY_KEY);
    }
}
