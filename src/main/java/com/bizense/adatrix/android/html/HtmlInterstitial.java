package com.bizense.adatrix.android.html;

import java.util.Map;

import android.net.Uri;

import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.activities.InterstitialActivity;
import com.bizense.adatrix.android.core.InterstitialListener;

public class HtmlInterstitial extends ResponseBodyInterstitial {
    private String mHtmlData;
    private boolean mIsScrollable;
    private String mRedirectUrl;
    private String mClickthroughUrl;

    @Override
    protected void extractExtras(Map<String, String> serverExtras) {
        mHtmlData = Uri.decode(serverExtras.get(AdxConstants.HTML_RESPONSE_BODY_KEY));
        mIsScrollable = Boolean.valueOf(serverExtras.get(AdxConstants.SCROLLABLE_KEY));
        mRedirectUrl = serverExtras.get(AdxConstants.REDIRECT_URL_KEY);
        mClickthroughUrl = serverExtras.get(AdxConstants.CLICKTHROUGH_URL_KEY);
    }

    @Override
    public void preRenderHtml(InterstitialListener customEventInterstitialListener) {
        InterstitialActivity.preRenderHtml(mContext, customEventInterstitialListener, mHtmlData);
    }

    @Override
    public void showInterstitial() {
        InterstitialActivity.start(mContext, mHtmlData, mIsScrollable, mRedirectUrl, mClickthroughUrl, mAdxConfiguration);
    }
}
