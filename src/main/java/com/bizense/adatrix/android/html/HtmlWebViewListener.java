package com.bizense.adatrix.android.html;

import com.bizense.adatrix.android.core.AbstractHtmlWebView;
import com.bizense.adatrix.android.utils.ErrorCode;

public interface HtmlWebViewListener {
	void onLoaded(AbstractHtmlWebView mHtmlWebView);
	void onFailed(ErrorCode unspecified);
	void onClicked();
	void onCollapsed();
}
