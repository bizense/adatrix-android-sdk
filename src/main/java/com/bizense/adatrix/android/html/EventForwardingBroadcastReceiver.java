package com.bizense.adatrix.android.html;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.bizense.adatrix.android.activities.AbstractInterstitialActivity;
import com.bizense.adatrix.android.core.InterstitialListener;
import com.bizense.adatrix.android.utils.ErrorCode;

class EventForwardingBroadcastReceiver extends BroadcastReceiver {
    private final InterstitialListener mInterstitialListener;
    private Context mContext;

    public EventForwardingBroadcastReceiver(InterstitialListener interstitialListener) {
        mInterstitialListener = interstitialListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (mInterstitialListener == null) {
            return;
        }

        String action = intent.getAction();
        if (action.equals(AbstractInterstitialActivity.ACTION_INTERSTITIAL_FAIL)) {
            mInterstitialListener.onInterstitialFailed(ErrorCode.NETWORK_INVALID_STATE);
        } else if (action.equals(AbstractInterstitialActivity.ACTION_INTERSTITIAL_SHOW)) {
            mInterstitialListener.onInterstitialShown();
        } else if (action.equals(AbstractInterstitialActivity.ACTION_INTERSTITIAL_DISMISS)) {
            mInterstitialListener.onInterstitialDismissed();
        } else if (action.equals(AbstractInterstitialActivity.ACTION_INTERSTITIAL_CLICK)) {
            mInterstitialListener.onInterstitialClicked();
        }

    }

    public void register(Context context) {
        mContext = context;
        LocalBroadcastManager.getInstance(mContext).registerReceiver(this, AbstractInterstitialActivity.HTML_INTERSTITIAL_INTENT_FILTER);
    }

    public void unregister() {
        if (mContext != null) {
            LocalBroadcastManager.getInstance(mContext).unregisterReceiver(this);
            mContext = null;
        }
    }
}
