package com.bizense.adatrix.android.html;

import java.util.Map;

import android.content.Context;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.core.AbstractInterstitial;
import com.bizense.adatrix.android.core.InterstitialListener;
import com.bizense.adatrix.android.utils.ErrorCode;

public abstract class ResponseBodyInterstitial extends AbstractInterstitial {
    private EventForwardingBroadcastReceiver mBroadcastReceiver;
    protected Context mContext;
    protected AdxConfiguration mAdxConfiguration;

    abstract protected void extractExtras(Map<String, String> serverExtras);
    abstract public void preRenderHtml(InterstitialListener interstitialListener);
    abstract public void showInterstitial();

    @Override
    public void loadInterstitial(Context context,
    		InterstitialListener interstitialListener,
            Map<String, Object> localExtras,
            Map<String, String> serverExtras) {

        mContext = context;

        if (extrasAreValid(serverExtras)) {
            extractExtras(serverExtras);
        } else {
        	interstitialListener.onInterstitialFailed(ErrorCode.NETWORK_INVALID_STATE);
            return;
        }

        mAdxConfiguration = AdxConfiguration.extractFromMap(localExtras);
        mBroadcastReceiver = new EventForwardingBroadcastReceiver(interstitialListener);
        mBroadcastReceiver.register(context);

        preRenderHtml(interstitialListener);
    }

    @Override
    public void onInvalidate() {
        if (mBroadcastReceiver != null) {
            mBroadcastReceiver.unregister();
        }
    }

    private boolean extrasAreValid(Map<String,String> serverExtras) {
        return serverExtras.containsKey(AdxConstants.HTML_RESPONSE_BODY_KEY);
    }
}
