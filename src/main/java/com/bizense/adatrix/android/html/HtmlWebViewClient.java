package com.bizense.adatrix.android.html;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.bizense.adatrix.android.core.AbstractHtmlWebView;
import com.bizense.adatrix.android.mraid.MraidBrowser;
import com.bizense.adatrix.android.utils.Device;
import com.bizense.adatrix.android.utils.ErrorCode;

public class HtmlWebViewClient extends WebViewClient {
    public static final String ADX_FINISH_LOAD = "adx://finishLoad";
    public static final String ADX_FAIL_LOAD = "adx://failLoad";

    private final Context mContext;
    private HtmlWebViewListener mHtmlWebViewListener;
    private AbstractHtmlWebView mHtmlWebView;
    private final String mClickthroughUrl;
    private final String mRedirectUrl;

    HtmlWebViewClient(HtmlWebViewListener htmlWebViewListener, AbstractHtmlWebView htmlWebView, String clickthrough, String redirect) {
        mHtmlWebViewListener = htmlWebViewListener;
        mHtmlWebView = htmlWebView;
        mClickthroughUrl = clickthrough;
        mRedirectUrl = redirect;
        mContext = htmlWebView.getContext();
    }
    
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (handleSpecialAdxScheme(url) || handlePhoneScheme(url) || handleNativeBrowserScheme(url)) {
            return true;
        }

        url = urlWithClickTrackingRedirect(url);
        Log.d("Adatrix", "Ad clicked. Click URL: " + url);

        // this is added because http/s can also be intercepted
        if (!isWebSiteUrl(url) && canHandleApplicationUrl(url)) {
            if (launchApplicationUrl(url)) {
                return true;
            }
        }

        showMraidBrowserForUrl(url);
        return true;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        // If the URL being loaded shares the redirectUrl prefix, open it in the browser.
        if (mRedirectUrl != null && url.startsWith(mRedirectUrl)) {
            url = urlWithClickTrackingRedirect(url);
            view.stopLoading();
            showMraidBrowserForUrl(url);
        }
    }
    
    @Override
    public void onPageFinished(WebView view, String url){
    	super.onPageFinished(view, url);
    	mHtmlWebViewListener.onLoaded((AbstractHtmlWebView) view);
    }
    
    @Override
    public void onReceivedError(android.webkit.WebView view, int errorCode, java.lang.String description, java.lang.String failingUrl){
    	super.onReceivedError(view, errorCode, description, failingUrl);
    }

    private boolean isSpecialAdxScheme(String url) {
        return url.startsWith("adx://");
    }

    private boolean handleSpecialAdxScheme(String url) {
        if (!isSpecialAdxScheme(url)) {
            return false;
        }
        Uri uri = Uri.parse(url);
        String host = uri.getHost();

        if ("finishLoad".equals(host)) {
            mHtmlWebViewListener.onLoaded(mHtmlWebView);
        } else if ("close".equals(host)) {
            mHtmlWebViewListener.onCollapsed();
        } else if ("failLoad".equals(host)) {
            mHtmlWebViewListener.onFailed(ErrorCode.UNSPECIFIED);
        } else if ("custom".equals(host)) {
            handleCustomIntentFromUri(uri);
        }

        return true;
    }

    private boolean isPhoneScheme(String url) {
        return url.startsWith("tel:") || url.startsWith("voicemail:") ||
                url.startsWith("sms:") || url.startsWith("mailto:") ||
                url.startsWith("geo:") || url.startsWith("google.streetview:");
    }

    private boolean handlePhoneScheme(String url) {
        if (!isPhoneScheme(url)) {
            return false;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        String errorMessage = "Could not handle intent with URI: " + url
                + ". Is this intent supported on your phone?";

        launchIntentForUserClick(mContext, intent, errorMessage);

        return true;
    }

    private boolean isNativeBrowserScheme(String url) {
        return url.startsWith("adxnativebrowser://");
    }

    private boolean handleNativeBrowserScheme(String url) {
        if (!isNativeBrowserScheme(url)) {
            return false;
        }

        Uri uri = Uri.parse(url);

        String urlToOpenInNativeBrowser;
        try {
            urlToOpenInNativeBrowser = uri.getQueryParameter("url");
        } catch (UnsupportedOperationException e) {
            Log.w("Adatrix", "Could not handle url: " + url);
            return false;
        }

        if (!"navigate".equals(uri.getHost()) || urlToOpenInNativeBrowser == null) {
            return false;
        }

        Uri intentUri = Uri.parse(urlToOpenInNativeBrowser);

        Intent intent = new Intent(Intent.ACTION_VIEW, intentUri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        String errorMessage = "Could not handle intent with URI: " + url
                + ". Is this intent supported on your phone?";

        launchIntentForUserClick(mContext, intent, errorMessage);

        return true;
    }

    private boolean isWebSiteUrl(String url) {
        return url.startsWith("http://") || url.startsWith("https://");
    }

    private boolean canHandleApplicationUrl(String url) {
        // Determine which activities can handle the intent
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));

        // If there are no relevant activities, don't follow the link
        if (!Device.canHandleIntent(mContext, intent)) {
            Log.w("Adatrix", "Could not handle application specific action: " + url + ". " +
                    "You may be running in the emulator or another device which does not " +
                    "have the required application.");
            return false;
        }

        return true;
    }

    private String urlWithClickTrackingRedirect(String url) {
        if (mClickthroughUrl == null) {
            return url;
        } else {
            String encodedUrl = Uri.encode(url);
            return mClickthroughUrl + "&r=" + encodedUrl;
        }
    }

    private boolean launchApplicationUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        String errorMessage = "Unable to open intent.";

        return launchIntentForUserClick(mContext, intent, errorMessage);
    }

    private void showMraidBrowserForUrl(String url) {
        if (url == null || url.equals("")) url = "about:blank";
        Log.d("Adatrix", "Final URI to show in browser: " + url);
        Intent intent = new Intent(mContext, MraidBrowser.class);
        intent.putExtra(MraidBrowser.URL_EXTRA, url);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        String errorMessage = "Could not handle intent action. "
                + ". Perhaps you forgot to declare com.bizense.adatrix.android.mraid.MraidBrowser"
                + " in your Android manifest file.";

        boolean handledByMraidBrowser = launchIntentForUserClick(mContext, intent, errorMessage);

        if (!handledByMraidBrowser) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("about:blank"));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            launchIntentForUserClick(mContext, intent, null);
        }
    }

    private void handleCustomIntentFromUri(Uri uri) {
        String action;
        String adData;
        try {
            action = uri.getQueryParameter("fnc");
            adData = uri.getQueryParameter("data");
        } catch (UnsupportedOperationException e) {
            Log.w("Adatrix", "Could not handle custom intent with uri: " + uri);
            return;
        }

        Intent customIntent = new Intent(action);
        customIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        customIntent.putExtra(HtmlBannerWebView.EXTRA_AD_CLICK_DATA, adData);

        String errorMessage = "Could not handle custom intent: " + action
                + ". Is your intent spelled correctly?";

        launchIntentForUserClick(mContext, customIntent, errorMessage);
    }

    boolean launchIntentForUserClick(Context context, Intent intent, String errorMessage) {
        if (!mHtmlWebView.wasClicked()) {
            return false;
        }

        boolean wasIntentStarted = executeIntent(context, intent, errorMessage);
        if (wasIntentStarted) {
            mHtmlWebViewListener.onClicked();
            mHtmlWebView.onResetUserClick();
        }

        return wasIntentStarted;
    }

    private boolean executeIntent(Context context, Intent intent, String errorMessage) {
        try {
            context.startActivity(intent);
        } catch (Exception e) {
            Log.d("Adatrix", (errorMessage != null)
                    ? errorMessage
                    : "Unable to start intent.");
            return false;
        }
        return true;
    }
}
