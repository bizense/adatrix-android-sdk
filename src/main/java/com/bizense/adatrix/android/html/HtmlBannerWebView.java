package com.bizense.adatrix.android.html;

import android.content.Context;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.core.AbstractHtmlWebView;
import com.bizense.adatrix.android.core.BannerListener;
import com.bizense.adatrix.android.utils.ErrorCode;

public class HtmlBannerWebView extends AbstractHtmlWebView {
    public static final String EXTRA_AD_CLICK_DATA = "com.bizense.adatrix.intent.extra.AD_CLICK_DATA";

    public HtmlBannerWebView(Context context, AdxConfiguration adxConfiguration) {
        super(context, adxConfiguration);
    }

    public void init(BannerListener bannerListener, boolean isScrollable, String redirectUrl, String clickthroughUrl) {
        super.init(isScrollable);

        setWebViewClient(new HtmlWebViewClient(new HtmlBannerWebViewListener(bannerListener), this, clickthroughUrl, redirectUrl));
    }

    static class HtmlBannerWebViewListener implements HtmlWebViewListener {
        private final BannerListener mBannerListener;

        public HtmlBannerWebViewListener(BannerListener bannerListener) {
            mBannerListener = bannerListener;
        }

        @Override
        public void onLoaded(AbstractHtmlWebView htmlWebView) {
            mBannerListener.onBannerLoaded(htmlWebView);
        }

        @Override
        public void onFailed(ErrorCode errorCode) {
            mBannerListener.onBannerFailed(errorCode);
        }

        @Override
        public void onClicked() {
            mBannerListener.onBannerClicked();
        }

        @Override
        public void onCollapsed() {
            mBannerListener.onBannerCollapsed();
        }

    }
}
