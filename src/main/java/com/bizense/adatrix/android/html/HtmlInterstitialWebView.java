package com.bizense.adatrix.android.html;

import android.content.Context;
import android.os.Handler;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.core.AbstractHtmlWebView;
import com.bizense.adatrix.android.core.InterstitialListener;
import com.bizense.adatrix.android.utils.ErrorCode;

public class HtmlInterstitialWebView extends AbstractHtmlWebView {
    private Handler mHandler;

    public interface AdxUriJavascriptFireFinishLoadListener {
        abstract void onInterstitialLoaded();
    }

    public HtmlInterstitialWebView(Context context, AdxConfiguration adConfiguration) {
        super(context, adConfiguration);

        mHandler = new Handler();
    }

    public void init(final InterstitialListener interstitialListener, boolean isScrollable, String redirectUrl, String clickthroughUrl) {
        super.init(isScrollable);

        HtmlInterstitialWebViewListener htmlInterstitialWebViewListener = new HtmlInterstitialWebViewListener(interstitialListener);
        HtmlWebViewClient htmlWebViewClient = new HtmlWebViewClient(htmlInterstitialWebViewListener, this, clickthroughUrl, redirectUrl);
        setWebViewClient(htmlWebViewClient);

        addAdxUriJavascriptInterface(new AdxUriJavascriptFireFinishLoadListener() {
            @Override
            public void onInterstitialLoaded() {
            	interstitialListener.onInterstitialLoaded();
            }
        });
    }

    private void postHandlerRunnable(Runnable r) {
        mHandler.post(r);
    }

    /*
     * XXX (2/15/12): This is a workaround for a problem on ICS devices where
     * WebViews with layout height WRAP_CONTENT can mysteriously render with
     * zero height. This seems to happen when calling loadData() with HTML that
     * sets window.location during its "onload" event. We use loadData() when
     * displaying interstitials, and our creatives use window.location to
     * communicate ad loading status to AdViews. This results in zero-height
     * interstitials. We counteract this by using a Javascript interface object
     * to signal loading status, rather than modifying window.location.
     */
    public void addAdxUriJavascriptInterface(final AdxUriJavascriptFireFinishLoadListener adxUriJavascriptFireFinishLoadListener) {
        final class AdxUriJavascriptInterface {
            // This method appears to be unused, since it will only be called from JavaScript.
            @SuppressWarnings("unused")
            public boolean fireFinishLoad() {
                HtmlInterstitialWebView.this.postHandlerRunnable(new Runnable() {
                    @Override
                    public void run() {
                        adxUriJavascriptFireFinishLoadListener.onInterstitialLoaded();
                    }
                });
                return true;
            }
        }

        addJavascriptInterface(new AdxUriJavascriptInterface(), "adxUriInterface");
    }

    static class HtmlInterstitialWebViewListener implements HtmlWebViewListener {
        private final InterstitialListener mInterstitialListener;

        public HtmlInterstitialWebViewListener(InterstitialListener customEventInterstitialListener) {
            mInterstitialListener = customEventInterstitialListener;
        }

        @Override
        public void onLoaded(AbstractHtmlWebView mHtmlWebView) {
            mInterstitialListener.onInterstitialLoaded();
        }

        @Override
        public void onFailed(ErrorCode errorCode) {
            mInterstitialListener.onInterstitialFailed(errorCode);
        }

        @Override
        public void onClicked() {
            mInterstitialListener.onInterstitialClicked();
        }

        @Override
        public void onCollapsed() {
            // Ignored
        }
    }
}
