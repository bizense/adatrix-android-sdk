package com.bizense.adatrix.android;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.apache.http.HttpResponse;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.webkit.WebView;

import com.bizense.adatrix.android.utils.AndroidVersion;
import com.bizense.adatrix.android.utils.Crypto;
import com.bizense.adatrix.android.utils.ResponseHeader;

public class AdxConfiguration implements Serializable{
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -8146517187048376929L;
	
	private static final int MINIMUM_REFRESH_TIME_MILLISECONDS = 10000;
    private static final int DEFAULT_REFRESH_TIME_MILLISECONDS = 60000;
    private static final String mPlatform = "Android";
    private final String mSdkVersion;
    
    private final String mHashedUdid;
    private final String mUserAgent;
    private final String mDeviceLocale;
    private final String mDeviceModel;
    private final int mPlatformVersion;

    private String mResponseString;
    private String mZone;
    private String mCategory;
    private String mViewId;
    private String mAaid;

    private String mAdType;
    private String mNetworkType;
    private String mRedirectUrl;
    private String mClickthroughUrl;
    private String mFailUrl;
    private String mImpressionUrl;
    private long mTimeStamp;
    private int mWidth;
    private int mHeight;
    private Integer mAdTimeoutDelay;
    private int mRefreshTimeMilliseconds;
    private String mDspCreativeId;

    public static AdxConfiguration extractFromMap(Map<String,Object> map) {
        if (map == null) {
            return null;
        }

        Object adxConfiguration = map.get(AdxConstants.AD_CONFIGURATION_KEY);

        if (adxConfiguration instanceof AdxConfiguration) {
            return (AdxConfiguration) adxConfiguration;
        }

        return null;
    }

    public AdxConfiguration(final Context context) {
        setDefaults();

        if (context != null) {
            String udid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            mHashedUdid = Crypto.sha1((udid != null) ? udid : "");

            mUserAgent = new WebView(context).getSettings().getUserAgentString();
            mDeviceLocale = context.getResources().getConfiguration().locale.toString();
        } else {
            mHashedUdid = null;
            mUserAgent = null;
            mDeviceLocale = null;
        }
        mAaid = null;
        mDeviceModel = Build.MANUFACTURER + " " + Build.MODEL;
        mPlatformVersion = AndroidVersion.currentApiLevel().getApiLevel();
        mSdkVersion = AdxConstants.SDK_VERSION;
    }

    public void cleanup() {
        setDefaults();
    }

    public void addHttpResponse(final HttpResponse httpResponse) {
        mAdType = ResponseHeader.AD_TYPE.extractString(httpResponse);

        // Set the network type of the ad.
        mNetworkType = ResponseHeader.NETWORK_TYPE.extractString(httpResponse);

        // Set the redirect URL prefix: navigating to any matching URLs will send us to the browser.
        mRedirectUrl = ResponseHeader.REDIRECT_URL.extractString(httpResponse);

        // Set the URL that is prepended to links for click-tracking purposes.
        mClickthroughUrl = ResponseHeader.CLICKTHROUGH_URL.extractString(httpResponse);

        // Set the fall-back URL to be used if the current request fails.
        mFailUrl = ResponseHeader.FAIL_URL.extractString(httpResponse);

        // Set the URL to be used for impression tracking.
        mImpressionUrl = ResponseHeader.IMPRESSION_URL.extractString(httpResponse);

        // Set the timestamp used for Ad Alert Reporting.
        mTimeStamp = (new Date()).getTime();

        // Set the width and height.
        mWidth = ResponseHeader.WIDTH.extractInteger(httpResponse, 0);
        mHeight = ResponseHeader.HEIGHT.extractInteger(httpResponse, 0);

        // Set the allowable amount of time an ad has before it automatically fails.
        mAdTimeoutDelay = ResponseHeader.AD_TIMEOUT.extractInteger(httpResponse);

        // Set the auto-refresh time. A timer will be scheduled upon ad success or failure.
        if (!httpResponse.containsHeader(ResponseHeader.REFRESH_TIME.getKey())) {
            mRefreshTimeMilliseconds = 0;
        } else {
            mRefreshTimeMilliseconds = ResponseHeader.REFRESH_TIME.extractInteger(httpResponse, 0) * 1000;
            mRefreshTimeMilliseconds = Math.max(
                    mRefreshTimeMilliseconds,
                    MINIMUM_REFRESH_TIME_MILLISECONDS);
        }

        // Set the unique identifier for the creative that was returned.
        mDspCreativeId = ResponseHeader.DSP_CREATIVE_ID.extractString(httpResponse);
    }

    /*
     * AdView
     */

    public String getViewId() {
    	return mViewId;
    }
    
    public void setViewId(String viewId) {
    	mViewId = viewId;
    }
    
    public String getZone() {
        return mZone;
    }

    public void setZone(String zone) {
        mZone = zone;
    }
    
    public void setAaid(String aaid) {
    	mAaid = aaid;
    }
    public String getAaid() {
    	return mAaid;
    }
    
    public String getCategory() {
    	return mCategory;
    }
    
    public void setCategory(String category) {
    	mCategory = category;
    }

    public String getResponseString() {
        return mResponseString;
    }

    public void setResponseString(String responseString) {
        mResponseString = responseString;
    }

    /*
     * HttpResponse
     */

    public String getAdType() {
        return mAdType;
    }

    public String getNetworkType() {
        return mNetworkType;
    }

    public String getRedirectUrl() {
        return mRedirectUrl;
    }

    public String getClickthroughUrl() {
        return mClickthroughUrl;
    }
    public String getFailUrl() {
        return mFailUrl;
    }

    public void setFailUrl(String failUrl) {
        mFailUrl = failUrl;
    }

    public String getImpressionUrl() {
        return mImpressionUrl;
    }

    public long getTimeStamp() {
        return mTimeStamp;
    }
    
    public void setTimeStamp(long timeStamp){
    	mTimeStamp = timeStamp;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public Integer getAdTimeoutDelay() {
        return mAdTimeoutDelay;
    }

    public int getRefreshTimeMilliseconds() {
        return mRefreshTimeMilliseconds;
    }

    public String getDspCreativeId() {
        return mDspCreativeId;
    }

    /*
     * Context
     */

    public String getHashedUdid() {
        return mHashedUdid;
    }

    public String getUserAgent() {
        return mUserAgent;
    }

    public String getDeviceLocale() {
        return mDeviceLocale;
    }

    public String getDeviceModel() {
        return mDeviceModel;
    }

    public int getPlatformVersion() {
        return mPlatformVersion;
    }

    public String getPlatform() {
        return mPlatform;
    }

    /*
     * Misc.
     */

    public String getSdkVersion() {
        return mSdkVersion;
    }

    private void setDefaults() {
    	mViewId = null;
        mZone = null;
        mCategory = null;
        mResponseString = null;
        mAdType = null;
        mNetworkType = null;
        mRedirectUrl = null;
        mClickthroughUrl = null;
        mImpressionUrl = null;
        mTimeStamp = (new Date()).getTime();
        mWidth = 0;
        mHeight = 0;
        mAdTimeoutDelay = null;
        mRefreshTimeMilliseconds = DEFAULT_REFRESH_TIME_MILLISECONDS;
        mFailUrl = null;
        mDspCreativeId = null;
    }
    
    public void setRefreshTimeMilliseconds(int refreshTimeMilliseconds) {
        mRefreshTimeMilliseconds = refreshTimeMilliseconds;
    }
}
