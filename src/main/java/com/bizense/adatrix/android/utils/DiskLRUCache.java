package com.bizense.adatrix.android.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.net.Uri;
import android.support.v4.util.LruCache;
import android.util.Log;

import com.bizense.adatrix.android.managers.FileManager;
import com.bizense.adatrix.android.managers.StreamManager;

/*
 * Please use putStream, getUri, and removeStream (instead of put, get, and remove).
 * The original methods do not perform necessary hashing of fileNames
 */
public class DiskLRUCache extends LruCache<String, File> {
    private final Context mContext;
    private final String mCacheDirectoryName;
    private final File mCacheDirectory;

    public DiskLRUCache(Context context, String cacheDirectoryName, int maxSizeBytes) throws IllegalArgumentException, IOException {
        super(maxSizeBytes);

        if (context == null) {
            throw new IllegalArgumentException("context may not be null.");
        } else if (cacheDirectoryName == null) {
            throw new IllegalArgumentException("cacheDirectoryName may not be null.");
        } else if (maxSizeBytes < 0) {
            throw new IllegalArgumentException("maxSizeBytes must be positive.");
        }

        mContext = context;
        mCacheDirectoryName = cacheDirectoryName;
        mCacheDirectory = FileManager.createDirectory(context.getFilesDir() + File.separator + mCacheDirectoryName);

        if (mCacheDirectory == null) {
            throw new IOException("Unable to obtain access to directory " + mCacheDirectoryName);
        }

        loadFilesFromDisk();
    }

    public File getCacheDirectory() {
        return mCacheDirectory;
    }

    public Uri getUri(final String key) {
        File value = get(Crypto.sha1(key));

        if (value == null) {
            return null;
        }

        return Uri.parse(value.getAbsolutePath());
    }

    public synchronized boolean putStream(final String fileName, final InputStream content) {
        if (fileName == null || content == null) {
            return false;
        }

        String hashedFileName = Crypto.sha1(fileName);

        if (getUri(hashedFileName) != null) {
            return false;
        }

        File file = createFile(hashedFileName, content);

        if (file == null || !file.exists()) {
            return false;
        }

        put(hashedFileName, file);
        return true;
    }

    public synchronized File removeStream(final String fileName) {
        if (fileName == null) {
            return null;
        }

        return remove(Crypto.sha1(fileName));
    }

    private File createFile(String fileName, InputStream content) {
        File file = new File(mContext.getFilesDir() + File.separator + mCacheDirectoryName + File.separator + fileName);

        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            return null;
        }

        try {
            StreamManager.copyContent(content, fileOutputStream);
        } catch (IOException e) {
            file.delete();
            return null;
        } finally {
            StreamManager.closeStream(fileOutputStream);
        }

        return file;
    }

    private void loadFilesFromDisk() {
        File[] allFiles = mCacheDirectory.listFiles();

        if (allFiles != null) {
            for (final File file : allFiles) {
                put(file.getName(), file);
            }
        }
    }

    /*
     * From android.support.v4.util.LruCache
     */

    @Override
    protected void entryRemoved(final boolean evicted, final String key, final File oldValue, final File newValue) {
        super.entryRemoved(evicted, key, oldValue, newValue);

        if (oldValue != null) {
            if (!oldValue.delete()) {
                Log.d("Adatrix", "Unable to delete file from cache: " + oldValue.getName());
            }
        }
    }

    @Override
    protected int sizeOf(String key, File value) {
        if (value != null && value.exists() && value.length() > 0) {
            return FileManager.intLength(value);
        }

        return super.sizeOf(key, value);
    }
}

