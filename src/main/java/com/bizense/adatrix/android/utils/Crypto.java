package com.bizense.adatrix.android.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Crypto {
	public static String sha1(String s) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                hexString.append(Integer.toHexString((0xFF & messageDigest[i]) | 0x100).substring(1));
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
        catch (NullPointerException e) {
            return "";
        }
    }
}
