package com.bizense.adatrix.android.utils;

import java.text.NumberFormat;
import java.util.Locale;

import org.apache.http.Header;
import org.apache.http.HttpResponse;

public enum ResponseHeader{
	AD_TIMEOUT("X-AdTimeout"),
	AD_TYPE("X-Adtype"),
	CLICKTHROUGH_URL("X-Clickthrough"),
	CUSTOM_EVENT_DATA("X-Custom-Event-Class-Data"),
	CUSTOM_EVENT_NAME("X-Custom-Event-Class-Name"),
	CUSTOM_EVENT_HTML_DATA("X-Custom-Event-Html-Data"),
	DSP_CREATIVE_ID("X-DspCreativeid"),
	FAIL_URL("X-Failurl"),
	FULL_AD_TYPE("X-Fulladtype"),
	HEIGHT("X-Height"),
	IMPRESSION_URL("X-Imptracker"),
	REDIRECT_URL("X-Launchpage"),
	NATIVE_PARAMS("X-Nativeparams"),
	NETWORK_TYPE("X-Networktype"),
	REFRESH_TIME("X-Refreshtime"),
	SCROLLABLE("X-Scrollable"),
	WARMUP("X-Warmup"),
	WIDTH("X-Width"),

	LOCATION("Location"),
	USER_AGENT("User-Agent");

	private final String key;	    
	private ResponseHeader(String key) {
		this.key = key;
	}

	public String getKey() {
		return this.key;
	}
	// string headers
	public String extractString(HttpResponse response){
		return extract(response, null, String.class);
	}
	public String extractString(HttpResponse response, String defaultValue){
		return extract(response, defaultValue, String.class);
	}
	// integer headers
	public Integer extractInteger(HttpResponse response){
		return extract(response, null, Integer.class);
	}
	public Integer extractInteger(HttpResponse response, Integer defaultValue){
		return extract(response, defaultValue, Integer.class);
	}
	// boolean headers
	public Boolean extractBoolean(HttpResponse response){
		return extract(response, null, Boolean.class);
	}
	public Boolean extractBoolean(HttpResponse response, Boolean defaultValue){
		return extract(response, defaultValue, Boolean.class);
	}
	
	public <T> T extract(HttpResponse response, T defaultValue, Class<T> returnClass){
		Header header = response.getFirstHeader(key);
		String headerValue = header != null ? header.getValue() : null;
		if(headerValue == null) return defaultValue;	    	
		else if(returnClass == String.class) return (T) (headerValue);
		else if(returnClass == Double.class){
			NumberFormat numberFormat = NumberFormat.getInstance(Locale.US);		        
			try {
				Number value = numberFormat.parse(headerValue.trim());
				return (T)(Double)value.doubleValue();
			} catch (Exception e) {
				return defaultValue;
			}
		}
		else if(returnClass == Boolean.class){	    		
			return (T)(Boolean)"1".equals(header.getValue());
		}
		else if(returnClass == Integer.class){
			NumberFormat numberFormat = NumberFormat.getInstance(Locale.US);
			numberFormat.setParseIntegerOnly(true);
			try {
				Number value = numberFormat.parse(headerValue.trim());
				return (T)(Integer)value.intValue();
			} catch (Exception e) {
				return defaultValue;
			}
		}	
		else return null;
	}
}
