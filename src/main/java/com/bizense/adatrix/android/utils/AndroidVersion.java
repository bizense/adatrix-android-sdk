package com.bizense.adatrix.android.utils;

import android.os.Build;

public enum AndroidVersion {
    BASE(1),
    BASE_1_1(2),
    CUPCAKE(3),
    DONUT(4),
    ECLAIR(5),
    ECLAIR_0_1(6),
    ECLAIR_MR1(7),
    FROYO(8),
    GINGERBREAD(9),
    GINGERBREAD_MR1(10),
    HONEYCOMB(11),
    HONEYCOMB_MR1(12),
    HONEYCOMB_MR2(13),
    ICE_CREAM_SANDWICH(14),
    ICE_CREAM_SANDWICH_MR1(15),
    JELLY_BEAN(16),
    JELLY_BEAN_MR1(17),
    JELLY_BEAN_MR2(18),
    CUR_DEVELOPMENT(10000);

    private int mApiLevel;

    public static AndroidVersion currentApiLevel() {
        return forApiLevel(Build.VERSION.SDK_INT);
    }

    private static AndroidVersion forApiLevel(int targetApiLevel) {
        for (AndroidVersion versionCode : AndroidVersion.values()) {
            if (versionCode.getApiLevel() == targetApiLevel) {
                return versionCode;
            }
        }
        return CUR_DEVELOPMENT;
    }

    private AndroidVersion(int apiLevel) {
        this.mApiLevel = apiLevel;
    }

    public int getApiLevel() {
        return mApiLevel;
    }

    public boolean isAtMost(AndroidVersion that) {
        return this.getApiLevel() <= that.getApiLevel();
    }

    public boolean isAtLeast(AndroidVersion that) {
        return this.getApiLevel() >= that.getApiLevel();
    }

    public boolean isBelow(AndroidVersion that) {
        return this.getApiLevel() < that.getApiLevel();
    }
}
