package com.bizense.adatrix.android.mraid;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;

import com.bizense.adatrix.android.core.AbstractVideoView;

public class MraidVideoView extends AbstractVideoView {

    public MraidVideoView(Context context, Intent intent, final VideoViewListener baseVideoViewListener) {
        super(context);

        setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (baseVideoViewListener != null) {
                    baseVideoViewListener.videoCompleted(true);
                }
            }
        });

        setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
                if (baseVideoViewListener != null) {
                    baseVideoViewListener.videoError(false);
                }
                return false;
            }
        });

        setVideoPath(intent.getStringExtra(MraidVideoPlayerActivity.VIDEO_URL));
    }
}
