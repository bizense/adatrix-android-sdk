package com.bizense.adatrix.android.mraid;

import java.util.Map;

import android.net.Uri;

import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.core.InterstitialListener;
import com.bizense.adatrix.android.html.ResponseBodyInterstitial;

class MraidInterstitial extends ResponseBodyInterstitial {
    private String mHtmlData;

    @Override
    protected void extractExtras(Map<String, String> serverExtras) {
        mHtmlData = Uri.decode(serverExtras.get(AdxConstants.HTML_RESPONSE_BODY_KEY));
    }

    @Override
    public void preRenderHtml(InterstitialListener interstitialListener) {
        MraidActivity.preRenderHtml(mContext, interstitialListener, mHtmlData);
    }

    @Override
    public void showInterstitial() {
        MraidActivity.start(mContext, mHtmlData, mAdxConfiguration);
    }
}
