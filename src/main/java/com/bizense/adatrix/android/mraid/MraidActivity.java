package com.bizense.adatrix.android.mraid;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.activities.AbstractInterstitialActivity;
import com.bizense.adatrix.android.core.InterstitialListener;
import com.bizense.adatrix.android.managers.MraidManager;
import com.bizense.adatrix.android.managers.WebViewManager;
import com.bizense.adatrix.android.mraid.MraidView.ExpansionStyle;
import com.bizense.adatrix.android.mraid.MraidView.NativeCloseButtonStyle;
import com.bizense.adatrix.android.mraid.MraidView.PlacementType;
import com.bizense.adatrix.android.mraid.MraidView.ViewState;
import com.bizense.adatrix.android.utils.AndroidVersion;

public class MraidActivity extends AbstractInterstitialActivity {
    private MraidView mMraidView;

    static void preRenderHtml(final Context context, final InterstitialListener interstitialListener, final String htmlData) {
        MraidView dummyMraidView = MraidManager.create(context, null, ExpansionStyle.DISABLED, NativeCloseButtonStyle.ALWAYS_VISIBLE, PlacementType.INTERSTITIAL);

        dummyMraidView.enablePlugins(false);
        dummyMraidView.setMraidListener(new MraidView.MraidListener() {
            @Override
            public void onReady(MraidView view) {
                interstitialListener.onInterstitialLoaded();
            }

            @Override
            public void onFailure(MraidView view) {
                interstitialListener.onInterstitialFailed(null);
            }

            @Override
            public void onExpand(MraidView view) {
            }

            @Override
            public void onClose(MraidView view, MraidView.ViewState newViewState) {
            }
        });
        dummyMraidView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                interstitialListener.onInterstitialLoaded();
            }
        });
        dummyMraidView.loadHtmlData(htmlData);
    }

    public static void start(Context context, String htmlData, AdxConfiguration adxConfiguration) {
        Intent intent = createIntent(context, htmlData, adxConfiguration);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException anfe) {
            Log.d("MraidInterstitial", "MraidActivity.class not found. Did you declare MraidActivity in your manifest?");
        }
    }

    private static Intent createIntent(Context context, String htmlData, AdxConfiguration adxConfiguration) {
        Intent intent = new Intent(context, MraidActivity.class);
        intent.putExtra(AdxConstants.HTML_RESPONSE_BODY_KEY, htmlData);
        intent.putExtra(AdxConstants.AD_CONFIGURATION_KEY, adxConfiguration);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    public View getAdView() {
        mMraidView = MraidManager.create(this, getAdxConfiguration(), ExpansionStyle.DISABLED, NativeCloseButtonStyle.AD_CONTROLLED, PlacementType.INTERSTITIAL);

        mMraidView.setMraidListener(new MraidView.BaseMraidListener(){
            public void onReady(MraidView view) {
                mMraidView.loadUrl(AbstractInterstitialActivity.JavaScriptWebViewCallbacks.WEB_VIEW_DID_APPEAR.getUrl());
                showInterstitialCloseButton();
            }
            public void onClose(MraidView view, ViewState newViewState) {
                mMraidView.loadUrl(AbstractInterstitialActivity.JavaScriptWebViewCallbacks.WEB_VIEW_DID_CLOSE.getUrl());
                finish();
            }
        });

        mMraidView.setOnCloseButtonStateChange(new MraidView.OnCloseButtonStateChangeListener() {
            public void onCloseButtonStateChange(MraidView view, boolean enabled) {
                if (enabled) {
                    showInterstitialCloseButton();
                } else {
                    hideInterstitialCloseButton();
                }
            }
        });

        String source = getIntent().getStringExtra(AdxConstants.HTML_RESPONSE_BODY_KEY);
        mMraidView.loadHtmlData(source);

        return mMraidView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        broadcastInterstitialAction(ACTION_INTERSTITIAL_SHOW);

        if (AndroidVersion.currentApiLevel().isAtLeast(AndroidVersion.ICE_CREAM_SANDWICH)) {
            getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        WebViewManager.onPause(mMraidView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        WebViewManager.onResume(mMraidView);
    }

    @Override
    public void onDestroy() {
        mMraidView.destroy();
        broadcastInterstitialAction(ACTION_INTERSTITIAL_DISMISS);
        super.onDestroy();
    }
}
