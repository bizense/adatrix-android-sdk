package com.bizense.adatrix.android.mraid;

import java.util.Map;

import android.content.Context;
import android.net.Uri;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.core.AbstractBanner;
import com.bizense.adatrix.android.core.BannerListener;
import com.bizense.adatrix.android.managers.MraidManager;
import com.bizense.adatrix.android.mraid.MraidView.MraidListener;
import com.bizense.adatrix.android.mraid.MraidView.ViewState;
import com.bizense.adatrix.android.utils.ErrorCode;

class MraidBanner extends AbstractBanner {
    private MraidView mMraidView;
    private BannerListener mBannerListener;

    @Override
    protected void loadBanner(Context context,
                    BannerListener bannerListener,
                    Map<String, Object> localExtras,
                    Map<String, String> serverExtras) {
        mBannerListener = bannerListener;

        String htmlData;
        if (extrasAreValid(serverExtras)) {
            htmlData = Uri.decode(serverExtras.get(AdxConstants.HTML_RESPONSE_BODY_KEY));
        } else {
            mBannerListener.onBannerFailed(ErrorCode.MRAID_LOAD_ERROR);
            return;
        }

        AdxConfiguration adxConfiguration = AdxConfiguration.extractFromMap(localExtras);
        mMraidView = MraidManager.create(context, adxConfiguration);
        mMraidView.loadHtmlData(htmlData);
        initMraidListener();
    }

    @Override
    protected void onInvalidate() {
        if (mMraidView != null) {
            resetMraidListener();
            mMraidView.destroy();
        }
    }

    private void onReady() {
        mBannerListener.onBannerLoaded(mMraidView);
    }

    private void onFail() {
        mBannerListener.onBannerFailed(ErrorCode.MRAID_LOAD_ERROR);
    }

    private void onExpand() {
        mBannerListener.onBannerExpanded();
        mBannerListener.onBannerClicked();
    }

    private void onClose() {
        mBannerListener.onBannerCollapsed();
    }

    private boolean extrasAreValid(Map<String, String> serverExtras) {
        return serverExtras.containsKey(AdxConstants.HTML_RESPONSE_BODY_KEY);
    }

    private void initMraidListener() {
        mMraidView.setMraidListener(new MraidListener() {
            public void onReady(MraidView view) {
                MraidBanner.this.onReady();
            }
            public void onFailure(MraidView view) {
                onFail();
            }
            public void onExpand(MraidView view) {
                MraidBanner.this.onExpand();
            }
            public void onClose(MraidView view, ViewState newViewState) {
                MraidBanner.this.onClose();
            }
        });
    }

    private void resetMraidListener() {
        mMraidView.setMraidListener(null);
    }
}
