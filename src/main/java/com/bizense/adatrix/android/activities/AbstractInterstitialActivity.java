package com.bizense.adatrix.android.activities;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.resources.Drawables;
import com.bizense.adatrix.android.utils.Dips;

public abstract class AbstractInterstitialActivity extends Activity{
	public static final String ACTION_INTERSTITIAL_FAIL = "com.bizense.adatrix.action.interstitial.fail";
    public static final String ACTION_INTERSTITIAL_SHOW = "com.bizense.adatrix.action.interstitial.show";
    public static final String ACTION_INTERSTITIAL_DISMISS = "com.bizense.adatrix.action.interstitial.dismiss";
    public static final String ACTION_INTERSTITIAL_CLICK = "com.bizense.adatrix.action.interstitial.click";
    public static final IntentFilter HTML_INTERSTITIAL_INTENT_FILTER = createHtmlInterstitialIntentFilter();

    public enum JavaScriptWebViewCallbacks {
    	 WEB_VIEW_DID_APPEAR("javascript:(function(){console.log('webviewDidAppear')});"),
         WEB_VIEW_DID_CLOSE("javascript:(function(){console.log('webviewDidClose')});");

        private String mUrl;
        private JavaScriptWebViewCallbacks(String url) {
            mUrl = url;
        }

        public String getUrl() {
            return mUrl;
        }
    }

    private static final float CLOSE_BUTTON_SIZE = 50f;
    private static final float CLOSE_BUTTON_PADDING = 8f;

    private ImageView mCloseButton;
    private RelativeLayout mLayout;
    private int mButtonSize;
    private int mButtonPadding;

    public abstract View getAdView();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mButtonSize = Dips.asIntPixels(CLOSE_BUTTON_SIZE, this);
        mButtonPadding = Dips.asIntPixels(CLOSE_BUTTON_PADDING, this);

        mLayout = new RelativeLayout(this);
        final RelativeLayout.LayoutParams adViewLayout = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        adViewLayout.addRule(RelativeLayout.CENTER_IN_PARENT);
        mLayout.addView(getAdView(), adViewLayout);
        setContentView(mLayout);

        createInterstitialCloseButton();
    }

    @Override
    public void onDestroy() {
        mLayout.removeAllViews();
        super.onDestroy();
    }

    protected void showInterstitialCloseButton() {
        mCloseButton.setVisibility(VISIBLE);
    }

    public void hideInterstitialCloseButton() {
        mCloseButton.setVisibility(INVISIBLE);
    }

    protected void broadcastInterstitialAction(String action) {
        Intent intent = new Intent(action);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    protected AdxConfiguration getAdxConfiguration() {
        AdxConfiguration adxConfiguration;
        try {
            adxConfiguration = (AdxConfiguration) getIntent().getSerializableExtra(AdxConstants.AD_CONFIGURATION_KEY);
        } catch (ClassCastException e) {
            adxConfiguration = null;
        }
        return adxConfiguration;
    }

    private static IntentFilter createHtmlInterstitialIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_INTERSTITIAL_FAIL);
        intentFilter.addAction(ACTION_INTERSTITIAL_SHOW);
        intentFilter.addAction(ACTION_INTERSTITIAL_DISMISS);
        intentFilter.addAction(ACTION_INTERSTITIAL_CLICK);
        return intentFilter;
    }

    private void createInterstitialCloseButton() {
        mCloseButton = new ImageButton(this);
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[] {-android.R.attr.state_pressed}, Drawables.INTERSTITIAL_CLOSE_BUTTON_NORMAL.decodeImage(this));
        states.addState(new int[] {android.R.attr.state_pressed}, Drawables.INTERSTITIAL_CLOSE_BUTTON_PRESSED.decodeImage(this));
        mCloseButton.setImageDrawable(states);
        mCloseButton.setBackground(null);
        mCloseButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        RelativeLayout.LayoutParams buttonLayout = new RelativeLayout.LayoutParams(mButtonSize, mButtonSize);
        buttonLayout.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        buttonLayout.setMargins(mButtonPadding, 0, mButtonPadding, 0);
        mLayout.addView(mCloseButton, buttonLayout);
    }
}
