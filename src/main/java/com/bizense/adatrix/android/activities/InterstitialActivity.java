package com.bizense.adatrix.android.activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.core.InterstitialListener;
import com.bizense.adatrix.android.html.HtmlInterstitialWebView;
import com.bizense.adatrix.android.html.HtmlWebViewClient;
import com.bizense.adatrix.android.utils.ErrorCode;

public class InterstitialActivity extends AbstractInterstitialActivity {
    private HtmlInterstitialWebView mHtmlInterstitialWebView;

    public static void start(Context context, String htmlData, boolean isScrollable, String redirectUrl, String clickthroughUrl, AdxConfiguration adxConfiguration) {
        Intent intent = createIntent(context, htmlData, isScrollable, redirectUrl, clickthroughUrl, adxConfiguration);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException anfe) {
            Log.d("InterstitialActivity", "InterstitialActivity not found - did you declare it in AndroidManifest.xml?");
        }
    }

    static Intent createIntent(Context context, String htmlData, boolean isScrollable, String redirectUrl, String clickthroughUrl, AdxConfiguration adxConfiguration) {
        Intent intent = new Intent(context, InterstitialActivity.class);
        intent.putExtra(AdxConstants.HTML_RESPONSE_BODY_KEY, htmlData);
        intent.putExtra(AdxConstants.SCROLLABLE_KEY, isScrollable);
        intent.putExtra(AdxConstants.CLICKTHROUGH_URL_KEY, clickthroughUrl);
        intent.putExtra(AdxConstants.REDIRECT_URL_KEY, redirectUrl);
        intent.putExtra(AdxConstants.AD_CONFIGURATION_KEY, adxConfiguration);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }
    
    public static HtmlInterstitialWebView create(Context context,
            InterstitialListener interstitialListener,
            boolean isScrollable,
            String redirectUrl,
            String clickthroughUrl,
            AdxConfiguration adxConfiguration){
    	HtmlInterstitialWebView htmlInterstitialWebView = new HtmlInterstitialWebView(context, adxConfiguration);
        htmlInterstitialWebView.init(interstitialListener, isScrollable, redirectUrl, clickthroughUrl);
        return htmlInterstitialWebView;
    }

    public static void preRenderHtml(final Context context, final InterstitialListener interstitialListener, String htmlData) {
        HtmlInterstitialWebView dummyWebView = create(context, interstitialListener, false, null, null, null);
        dummyWebView.enablePlugins(false);

        dummyWebView.addAdxUriJavascriptInterface(new HtmlInterstitialWebView.AdxUriJavascriptFireFinishLoadListener() {
            @Override
            public void onInterstitialLoaded() {
                interstitialListener.onInterstitialLoaded();
            }
        });
        dummyWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.equals(HtmlWebViewClient.ADX_FINISH_LOAD)) {
                    interstitialListener.onInterstitialLoaded();
                } else if (url.equals(HtmlWebViewClient.ADX_FAIL_LOAD)) {
                    interstitialListener.onInterstitialFailed(null);
                }

                return true;
            }
            
            @Override
            public void onPageFinished(WebView view, String url){
            	super.onPageFinished(view, url);
            	interstitialListener.onInterstitialLoaded();
            }
        });
        dummyWebView.loadHtmlResponse(htmlData);
    }

    @Override
    public View getAdView() {
        Intent intent = getIntent();
        boolean isScrollable = intent.getBooleanExtra(AdxConstants.SCROLLABLE_KEY, false);
        String redirectUrl = intent.getStringExtra(AdxConstants.REDIRECT_URL_KEY);
        String clickthroughUrl = intent.getStringExtra(AdxConstants.CLICKTHROUGH_URL_KEY);
        String htmlResponse = intent.getStringExtra(AdxConstants.HTML_RESPONSE_BODY_KEY);

        mHtmlInterstitialWebView = create(getApplicationContext(), new BroadcastingInterstitialListener(), isScrollable, redirectUrl, clickthroughUrl, getAdxConfiguration());
        mHtmlInterstitialWebView.loadHtmlResponse(htmlResponse);

        return mHtmlInterstitialWebView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        broadcastInterstitialAction(ACTION_INTERSTITIAL_SHOW);
    }

    @Override
    public void onDestroy() {
        mHtmlInterstitialWebView.loadUrl(JavaScriptWebViewCallbacks.WEB_VIEW_DID_CLOSE.getUrl());
        mHtmlInterstitialWebView.destroy();
        broadcastInterstitialAction(ACTION_INTERSTITIAL_DISMISS);
        super.onDestroy();
    }

    class BroadcastingInterstitialListener implements InterstitialListener {
        @Override
        public void onInterstitialLoaded() {
            mHtmlInterstitialWebView.loadUrl(JavaScriptWebViewCallbacks.WEB_VIEW_DID_APPEAR.getUrl());
        }

        @Override
        public void onInterstitialFailed(ErrorCode errorCode) {
            broadcastInterstitialAction(ACTION_INTERSTITIAL_FAIL);
            finish();
        }

        @Override
        public void onInterstitialShown() {
        }

        @Override
        public void onInterstitialClicked() {
            broadcastInterstitialAction(ACTION_INTERSTITIAL_CLICK);
        }

        @Override
        public void onLeaveApplication() {
        }

        @Override
        public void onInterstitialDismissed() {
        }
    }
}
