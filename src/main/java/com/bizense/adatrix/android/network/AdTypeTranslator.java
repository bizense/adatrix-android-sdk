package com.bizense.adatrix.android.network;

import java.util.HashMap;
import java.util.Map;

import com.bizense.adatrix.android.core.AdView;
import com.bizense.adatrix.android.core.Interstitial;

public class AdTypeTranslator {
    public static final String ADMOB_BANNER = "com.bizense.adatrix.android.GoogleAdMobBanner";
    public static final String ADMOB_INTERSTITIAL = "com.bizense.adatrix.android.GoogleAdMobInterstitial";
    public static final String MILLENNIAL_BANNER = "com.bizense.adatrix.android.MillennialBanner";
    public static final String MILLENNIAL_INTERSTITIAL = "com.bizense.adatrix.android.MillennialInterstitial";
    public static final String MRAID_BANNER = "com.bizense.adatrix.android.mraid.MraidBanner";
    public static final String MRAID_INTERSTITIAL = "com.bizense.adatrix.android.mraid.MraidInterstitial";
    public static final String HTML_BANNER = "com.bizense.adatrix.android.html.HtmlBanner";
    public static final String HTML_INTERSTITIAL = "com.bizense.adatrix.android.html.HtmlInterstitial";
    public static final String VAST_VIDEO_INTERSTITIAL = "com.bizense.adatrix.android.video.VastVideoInterstitial";
    private static Map<String, String> customEventNameForAdType = new HashMap<String, String>();

    static {
        customEventNameForAdType.put("admob_native_banner", ADMOB_BANNER);
        customEventNameForAdType.put("admob_full_interstitial", ADMOB_INTERSTITIAL);
        customEventNameForAdType.put("millennial_native_banner", MILLENNIAL_BANNER);
        customEventNameForAdType.put("millennial_full_interstitial", MILLENNIAL_INTERSTITIAL);
        customEventNameForAdType.put("mraid_banner", MRAID_BANNER);
        customEventNameForAdType.put("mraid_interstitial", MRAID_INTERSTITIAL);
        customEventNameForAdType.put("html_banner", HTML_BANNER);
        customEventNameForAdType.put("html_interstitial", HTML_INTERSTITIAL);
        customEventNameForAdType.put("vast_interstitial", VAST_VIDEO_INTERSTITIAL);
    }

    static String getAdNetworkType(String adType, String fullAdType) {
        String adNetworkType = "interstitial".equals(adType) ? fullAdType : adType;
        return adNetworkType != null ? adNetworkType : "unknown";
    }

    static String getCustomEventNameForAdType(AdView adView, String adType, String fullAdType) {
        if ("html".equals(adType) || "mraid".equals(adType)) {
            return isInterstitial(adView)
                   ? customEventNameForAdType.get(adType + "_interstitial")
                   : customEventNameForAdType.get(adType + "_banner");
        } else {
            return "interstitial".equals(adType)
                    ? customEventNameForAdType.get(fullAdType + "_interstitial")
                    : customEventNameForAdType.get(adType + "_banner");
        }
    }

    private static boolean isInterstitial(AdView adView) {
        return adView instanceof Interstitial.InterstitialView;
    }
}
