package com.bizense.adatrix.android.network;

import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.net.ConnectivityManager.TYPE_ETHERNET;
import static android.net.ConnectivityManager.TYPE_MOBILE;
import static android.net.ConnectivityManager.TYPE_MOBILE_DUN;
import static android.net.ConnectivityManager.TYPE_MOBILE_HIPRI;
import static android.net.ConnectivityManager.TYPE_MOBILE_MMS;
import static android.net.ConnectivityManager.TYPE_MOBILE_SUPL;
import static android.net.ConnectivityManager.TYPE_WIFI;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import android.content.Context;
import android.content.res.Configuration;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;

import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.core.AdView;
import com.bizense.adatrix.android.managers.MraidManager;

public class AdUrlGenerator extends AbstractUrlGenerator {
    public static final String DEVICE_ORIENTATION_PORTRAIT = "p";
    public static final String DEVICE_ORIENTATION_LANDSCAPE = "l";
    public static final String DEVICE_ORIENTATION_SQUARE = "s";
    public static final String DEVICE_ORIENTATION_UNKNOWN = "u";
    public static final int UNKNOWN_NETWORK_TYPE = 0x00000008; // Equivalent to TYPE_DUMMY introduced in API level 14. Will generate the "unknown" code
    private Context mContext;
    private TelephonyManager mTelephonyManager;
    private ConnectivityManager mConnectivityManager;
    private String mZone;
    private String mViewId;
    private String mCategory;
    private String mKeywords;
    private Location mLocation;
    private Map<String,Object> mLocalExtras;
    private boolean mFacebookSupportEnabled;

    public static enum NetworkType {
        UNKNOWN,
        ETHERNET,
        WIFI,
        MOBILE;

        @Override
        public String toString() {
            return Integer.toString(ordinal());
        }
    }

    public AdUrlGenerator(Context context) {
        mContext = context;
        mTelephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        mConnectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public AdUrlGenerator withViewId(String viewId) {
        mViewId = viewId;
        return this;
    }
    
    public AdUrlGenerator withZone(String zone) {
        mZone = zone;
        return this;
    }
    
    public AdUrlGenerator withCategory(String category) {
    	mCategory = category;
    	return this;
    }

    public AdUrlGenerator withKeywords(String keywords) {
        mKeywords = keywords;
        return this;
    }

    public AdUrlGenerator withFacebookSupported(boolean enabled) {
        mFacebookSupportEnabled = enabled;
        return this;
    }

    public AdUrlGenerator withLocation(Location location) {
        mLocation = location;
        return this;
    }
    
    public AdUrlGenerator withLocalExtras(Map<String,Object> extras) {
    	mLocalExtras = extras;
    	return this;
    }

    @Override
    public String generateUrlString(String serverHostname) {
        initUrlString(serverHostname, AdView.AD_HANDLER);

        setApiVersion("6");

        setViewId(mViewId);
        
        setZone(mZone);
        
        setCategory(mCategory);

        setSdkVersion(AdxConstants.SDK_VERSION);

        setDeviceInfo(Build.MANUFACTURER, Build.MODEL, Build.PRODUCT);

        setUdid(getUdidFromContext(mContext));

        String keywords = AdUrlGenerator.addKeyword(mKeywords, AdUrlGenerator.getFacebookKeyword(mContext, mFacebookSupportEnabled));
        setKeywords(keywords);

        setLocation(mLocation);
        
        setLocalExtras(mLocalExtras);

        setTimezone(AdUrlGenerator.getTimeZoneOffsetString());

        setOrientation(mContext.getResources().getConfiguration().orientation);

        setDensity(mContext.getResources().getDisplayMetrics().density);

        setMraidFlag(detectIsMraidSupported());

        String networkOperator = getNetworkOperator();
        setMccCode(networkOperator);
        setMncCode(networkOperator);

        setIsoCountryCode(mTelephonyManager.getNetworkCountryIso());
        setCarrierName(mTelephonyManager.getNetworkOperatorName());

        setNetworkType(getActiveNetworkType());
        
        setAppId(getAppId(mContext));

        setAaid(getAaid(mContext));
        
        setAppVersion(getAppVersionFromContext(mContext));

        setExternalStoragePermission(MraidManager.isStorePictureSupported(mContext));

        return getFinalUrlString();
    }

    private void setViewId(String viewId) {
        addParam("viewid", viewId);
    }
    
    private void setZone(String zone) {
        addParam("zone", zone);
    }
    
    private void setCategory(String category) {
    	addParam("cat", category);
    }

    private void setSdkVersion(String sdkVersion) {
        addParam("nv", sdkVersion);
    }

    private void setKeywords(String keywords) {
        addParam("q", keywords);
    }

    private void setLocation(Location location) {
        if (location != null) {
            addParam("ll", location.getLatitude() + "," + location.getLongitude());
            addParam("lla", "" + (int) location.getAccuracy());
        }
    }
    
    private void setLocalExtras(Map<String,Object> extras) {
    	if(extras != null) {
    		addLocalExtras(extras);
    	}
    }

    private void setTimezone(String timeZoneOffsetString) {
        addParam("z", timeZoneOffsetString);
    }

    private void setOrientation(int orientation) {
        String orString = DEVICE_ORIENTATION_UNKNOWN;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            orString = DEVICE_ORIENTATION_PORTRAIT;
        } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            orString = DEVICE_ORIENTATION_LANDSCAPE;
        } else if (orientation == Configuration.ORIENTATION_SQUARE) {
            orString = DEVICE_ORIENTATION_SQUARE;
        }
        addParam("o", orString);
    }

    private void setDensity(float density) {
        addParam("sc_a", "" + density);
    }

    private void setMraidFlag(boolean mraid) {
        if (mraid) addParam("mr", "1");
    }

    private void setMccCode(String networkOperator) {
        String mcc = networkOperator == null ? "" : networkOperator.substring(0, mncPortionLength(networkOperator));
        addParam("mcc", mcc);
    }

    private void setMncCode(String networkOperator) {
        String mnc = networkOperator == null ? "" : networkOperator.substring(mncPortionLength(networkOperator));
        addParam("mnc", mnc);
    }

    private void setIsoCountryCode(String networkCountryIso) {
        addParam("iso", networkCountryIso);
    }

    private void setCarrierName(String networkOperatorName) {
        addParam("cn", networkOperatorName);
    }

    private void setNetworkType(int type) {
        switch(type) {
            case TYPE_ETHERNET:
                addParam("ct", NetworkType.ETHERNET);
                break;
            case TYPE_WIFI:
                addParam("ct", NetworkType.WIFI);
                break;
            case TYPE_MOBILE:
            case TYPE_MOBILE_DUN:
            case TYPE_MOBILE_HIPRI:
            case TYPE_MOBILE_MMS:
            case TYPE_MOBILE_SUPL:
                addParam("ct", NetworkType.MOBILE);
                break;
            default:
                addParam("ct", NetworkType.UNKNOWN);
        }
    }

    private void addParam(String key, NetworkType value) {
        addParam(key, value.toString());
    }

    private boolean detectIsMraidSupported() {
        boolean mraid = true;
        try {
            Class.forName("com.bizense.adatrix.android.mraid.MraidView");
        } catch (ClassNotFoundException e) {
            mraid = false;
        }
        return mraid;
    }

    private String getNetworkOperator() {
        String networkOperator = mTelephonyManager.getNetworkOperator();
        if (mTelephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA &&
                mTelephonyManager.getSimState() == TelephonyManager.SIM_STATE_READY) {
            networkOperator = mTelephonyManager.getSimOperator();
        }
        return networkOperator;
    }

    private int mncPortionLength(String networkOperator) {
        return Math.min(3, networkOperator.length());
    }

    private static String getTimeZoneOffsetString() {
        SimpleDateFormat format = new SimpleDateFormat("Z");
        format.setTimeZone(TimeZone.getDefault());
        return format.format(new Date());
    }

    private static String getFacebookKeyword(Context context, final boolean enabled) {
        if (!enabled) {
            return null;
        }

        try {
            Class<?> facebookKeywordProviderClass = Class.forName("com.bizense.adatrix.android.FacebookKeywordProvider");
            Method getKeywordMethod = facebookKeywordProviderClass.getMethod("getKeyword", Context.class);

            return (String) getKeywordMethod.invoke(facebookKeywordProviderClass, context);
        } catch (Exception exception) {
            return null;
        }
    }

    private int getActiveNetworkType() {
        if (mContext.checkCallingOrSelfPermission(ACCESS_NETWORK_STATE) == PERMISSION_GRANTED) {
            NetworkInfo activeNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null ? activeNetworkInfo.getType() : UNKNOWN_NETWORK_TYPE;
        }
        return UNKNOWN_NETWORK_TYPE;
    }

    private static String addKeyword(String keywords, String addition) {
        if (addition == null || addition.length() == 0) {
            return keywords;
        } else if (keywords == null || keywords.length() == 0) {
            return addition;
        } else {
            return keywords + "," + addition;
        }
    }
    
    @SuppressWarnings("unchecked")
	private void addLocalExtras(Map<String,Object> extras) {
    	for (Map.Entry<String, Object> entry : extras.entrySet()) {
    		String key = entry.getKey();
    		Object value = entry.getValue();
			if(key != null && value != null) {
				if(value instanceof String) {
					addParam(entry.getKey(), entry.getValue().toString());
				} else if(value instanceof List){
					String valueString = "";
					for (int i = 0; i < ((List<String>) value).size(); i++) {
						valueString += ((List<String>) value).get(i)+",";
					}
					addParam(entry.getKey(), valueString);
				}
					
			}
		}
    }

}
