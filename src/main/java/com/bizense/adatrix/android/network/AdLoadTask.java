package com.bizense.adatrix.android.network;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

import android.net.Uri;
import android.util.Log;

import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.core.AdViewController;
import com.bizense.adatrix.android.managers.StringManager;
import com.bizense.adatrix.android.utils.Json;
import com.bizense.adatrix.android.utils.ResponseHeader;

abstract class AdLoadTask {
    WeakReference<AdViewController> mWeakAdViewController;
    AdLoadTask(AdViewController adViewController) {
        mWeakAdViewController = new WeakReference<AdViewController>(adViewController);
    }

    abstract void execute();

    /*
     * The AsyncTask thread pool often appears to keep references to these
     * objects, preventing GC. This method should be used to release
     * resources to mitigate the GC issue.
     */
    abstract void cleanup();

    static AdLoadTask fromHttpResponse(HttpResponse response, AdViewController adViewController) throws IOException {
        return new TaskExtractor(response, adViewController).extract();
    }

    private static class TaskExtractor {
        private final HttpResponse response;
        private final AdViewController adViewController;
        private String adType;
        private String adTypeCustomEventName;
        private String fullAdType;

        TaskExtractor(HttpResponse response, AdViewController adViewController){
            this.response = response;
            this.adViewController = adViewController;
        }

        AdLoadTask extract() throws IOException {
            adType = ResponseHeader.AD_TYPE.extractString(response);
            fullAdType = ResponseHeader.FULL_AD_TYPE.extractString(response);
            if(adType == null) adType = "html";
            Log.d("Adatrix", "Loading ad type: " + AdTypeTranslator.getAdNetworkType(adType, fullAdType));

            adTypeCustomEventName = AdTypeTranslator.getCustomEventNameForAdType(
                    adViewController.getAdView(), adType, fullAdType);

            if ("custom".equals(adType)) {
                return extractCustomEventAdLoadTask();
            } else if (eventDataIsInResponseBody(adType)) {
                return extractCustomEventAdLoadTaskFromResponseBody();
            } else {
            	return extractCustomEventAdLoadTaskFromResponseBody(); // TODO
                //return extractCustomEventAdLoadTaskFromNativeParams();
            }
        }

        private AdLoadTask extractCustomEventAdLoadTask() {
            Log.i("Adatrix", "Performing custom event.");

            // If applicable, try to invoke the new custom event system (which uses custom classes)
            adTypeCustomEventName = ResponseHeader.CUSTOM_EVENT_NAME.extractString(response);
            if (adTypeCustomEventName != null) {
                String customEventData = ResponseHeader.CUSTOM_EVENT_DATA.extractString(response);
                return createCustomEventAdLoadTask(customEventData);
            }
            return null;
        }

        private AdLoadTask extractCustomEventAdLoadTaskFromResponseBody() throws IOException {
            HttpEntity entity = response.getEntity();
            String htmlData = entity != null ? StringManager.fromStream(entity.getContent()) : "";

            adViewController.getAdxConfiguration().setResponseString(htmlData);

            String redirectUrl = ResponseHeader.REDIRECT_URL.extractString(response);
            String clickthroughUrl = ResponseHeader.CLICKTHROUGH_URL.extractString(response);
            boolean scrollingEnabled = ResponseHeader.SCROLLABLE.extractBoolean(response, false);

            Map<String, String> eventDataMap = new HashMap<String, String>();
            eventDataMap.put(AdxConstants.HTML_RESPONSE_BODY_KEY, Uri.encode(htmlData));
            eventDataMap.put(AdxConstants.SCROLLABLE_KEY, Boolean.toString(scrollingEnabled));
            if (redirectUrl != null) {
                eventDataMap.put(AdxConstants.REDIRECT_URL_KEY, redirectUrl);
            }            
            if (clickthroughUrl != null) {
                eventDataMap.put(AdxConstants.CLICKTHROUGH_URL_KEY, clickthroughUrl);
            }
            
            eventDataMap.put(ResponseHeader.REFRESH_TIME.getKey(), "10");

            String eventData = Json.mapToJsonString(eventDataMap);
            return createCustomEventAdLoadTask(eventData);
        }

        private AdLoadTask extractCustomEventAdLoadTaskFromNativeParams() throws IOException {
            String eventData = ResponseHeader.NATIVE_PARAMS.extractString(response);

            return createCustomEventAdLoadTask(eventData);
        }

        private AdLoadTask createCustomEventAdLoadTask(String customEventData) {
            Map<String, String> paramsMap = new HashMap<String, String>();
            paramsMap.put(ResponseHeader.CUSTOM_EVENT_NAME.getKey(), adTypeCustomEventName);

            if (customEventData != null) {
                paramsMap.put(ResponseHeader.CUSTOM_EVENT_DATA.getKey(), customEventData);
            }

            return new AdLoadTask.CustomEventAdLoadTask(adViewController, paramsMap);
        }

        private boolean eventDataIsInResponseBody(String adType) {
            // XXX Hack
            return "mraid".equals(adType) || "html".equals(adType) || ("interstitial".equals(adType) && "vast".equals(fullAdType));
        }
    }

    /*
     * This is the new way of performing Custom Events. This will be invoked on new clients when
     * X-Adtype is "custom" and the X-Custom-Event-Class-Name header is specified.
     */
    static class CustomEventAdLoadTask extends AdLoadTask {
        private Map<String,String> mParamsMap;

        public CustomEventAdLoadTask(AdViewController adViewController, Map<String, String> paramsMap) {
            super(adViewController);
            mParamsMap = paramsMap;
        }

        @Override
        public void execute() {
            AdViewController adViewController = mWeakAdViewController.get();

            if (adViewController == null || adViewController.isDestroyed()) {
                return;
            }

            adViewController.setNotLoading();
            adViewController.getAdView().load(mParamsMap);
        }

        @Override
        void cleanup() {
            mParamsMap = null;
        }
        
        public Map<String,String> getParamsMap(){
        	return mParamsMap;
        }
    }
}