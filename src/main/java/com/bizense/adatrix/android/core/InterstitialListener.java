package com.bizense.adatrix.android.core;

import com.bizense.adatrix.android.utils.ErrorCode;

public interface InterstitialListener {
	/*
     * Your custom event subclass must call this method when it successfully loads an ad.
     * Failure to do so will disrupt the mediation waterfall and cause future ad requests to
     * stall.
     */
    void onInterstitialLoaded();
    
    /*
     * Your custom event subclass must call this method when it fails to load an ad.
     * Failure to do so will disrupt the mediation waterfall and cause future ad requests to
     * stall.
     */
    void onInterstitialFailed(ErrorCode errorCode);
    
    /*
     * Your custom event subclass should call this method when the interstitial ad is displayed.
     * This method is optional. However, if you call this method, you should ensure that
     * onInterstitialDismissed is called at a later time.
     */
    void onInterstitialShown();
    
    /*
     * Your custom event subclass should call this method when a user taps on an interstitial
     * ad. This method is optional.
     */
    void onInterstitialClicked();
    
    /*
     * This is an alias for onInterstitialClicked().
     * Your custom event subclass should call this method if the ad will cause the user to leave
     * the application (e.g. for the Play Store or browser). This method is optional.
     */
    void onLeaveApplication();
    
    /*
     * Your custom event subclass should call this method when the interstitial ad is closed.
     * This method is optional.
     */
    void onInterstitialDismissed();
}
