package com.bizense.adatrix.android.core;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.html.HtmlBannerWebView;
import com.bizense.adatrix.android.utils.ErrorCode;
import com.bizense.adatrix.android.utils.Json;

public class BannerAdapter implements BannerListener {
    public static final int DEFAULT_BANNER_TIMEOUT_DELAY = 10000;
    private boolean mInvalidated;
    private AdView mAdView;
    private Context mContext;
    private AbstractBanner mBanner;
    private Map<String, Object> mLocalExtras;
    private Map<String, String> mServerExtras;

    private final Handler mHandler;
    private final Runnable mTimeout;
    private boolean mStoredAutorefresh;

    public BannerAdapter(AdView adView, String className, String classData) {
        mHandler = new Handler();
        mAdView = adView;
        mContext = adView.getContext();
        mLocalExtras = new HashMap<String, Object>();
        mServerExtras = new HashMap<String, String>();
        mTimeout = new Runnable() {
            @Override
            public void run() {
                Log.d("Adatrix", "Third-party network timed out.");
                onBannerFailed(ErrorCode.NETWORK_TIMEOUT);
                invalidate();
            }
        };

        Log.d("Adatrix", "Attempting to invoke custom event: " + className);
        try {
            mBanner = create(className);
        } catch (Exception exception) {
            Log.d("Adatrix", "Couldn't locate or instantiate custom event: " + className + ".");
            mAdView.loadFailUrl(ErrorCode.ADAPTER_NOT_FOUND);
            return;
        }

        // Attempt to load the JSON extras into mServerExtras.
        try {
            mServerExtras = Json.jsonStringToMap(classData);
        } catch (Exception exception) {
            Log.d("Adatrix", "Failed to create Map from JSON: " + classData + exception.toString());
        }

        mLocalExtras = mAdView.getLocalExtras();
        if (mAdView.getLocation() != null) {
            mLocalExtras.put("location", mAdView.getLocation());
        }
        if (mAdView.getAdViewController() != null) {
            mLocalExtras.put(AdxConstants.AD_CONFIGURATION_KEY, mAdView.getAdViewController().getAdxConfiguration());
        }
    }
    
    AbstractBanner create(String className) throws Exception{
    	Class<? extends AbstractBanner> bannerClass = Class.forName(className)
                .asSubclass(AbstractBanner.class);
        Constructor<?> bannerConstructor = bannerClass.getDeclaredConstructor((Class[]) null);
        bannerConstructor.setAccessible(true);
        return (AbstractBanner) bannerConstructor.newInstance();
    }

    void loadAd() {
        if (isInvalidated() || mBanner == null) {
            return;
        }
        mBanner.loadBanner(mContext, this, mLocalExtras, mServerExtras);

        if (getTimeoutDelayMilliseconds() > 0) {
            mHandler.postDelayed(mTimeout, getTimeoutDelayMilliseconds());
        }
    }

    void invalidate() {
        if (mBanner != null) mBanner.onInvalidate();
        mContext = null;
        mBanner = null;
        mLocalExtras = null;
        mServerExtras = null;
        mInvalidated = true;
    }

    boolean isInvalidated() {
        return mInvalidated;
    }

    private void cancelTimeout() {
        mHandler.removeCallbacks(mTimeout);
    }

    private int getTimeoutDelayMilliseconds() {
        if (mAdView == null
                || mAdView.getAdTimeoutDelay() == null
                || mAdView.getAdTimeoutDelay() < 0) {
            return DEFAULT_BANNER_TIMEOUT_DELAY;
        }

        return mAdView.getAdTimeoutDelay() * 1000;
    }

    /*
     * BannerListener implementation
     */
    @Override
    public void onBannerLoaded(View bannerView) {
        if (isInvalidated()) return;
        
        if (mAdView != null) {
            cancelTimeout();
            mAdView.nativeAdLoaded();
            mAdView.setAdContentView(bannerView);
            if (!(bannerView instanceof HtmlBannerWebView)) {
                mAdView.trackNativeImpression();
            }
        }
    }

    @Override
    public void onBannerFailed(ErrorCode errorCode) {
        if (isInvalidated()) return;
        
        if (mAdView != null) {
            if (errorCode == null) {
                errorCode = ErrorCode.UNSPECIFIED;
            }
            cancelTimeout();
            mAdView.loadFailUrl(errorCode);
        }
    }

    @Override
    public void onBannerExpanded() {
        if (isInvalidated()) return;

        mStoredAutorefresh = mAdView.getAutorefreshEnabled();
        mAdView.setAutorefreshEnabled(false);
        mAdView.adPresentedOverlay();
    }

    @Override
    public void onBannerCollapsed() {
        if (isInvalidated()) return;

        mAdView.setAutorefreshEnabled(mStoredAutorefresh);
        mAdView.adClosed();
    }

    @Override
    public void onBannerClicked() {
        if (isInvalidated()) return;
        
        if (mAdView != null) mAdView.registerClick();
    }
    
    @Override
    public void onLeaveApplication() {
        onBannerClicked();
    }
    
    public AdView getAdView() {
        return mAdView;
    }

    public String getClassName() {
        return mBanner.getClass().getSimpleName();
    }

    public String getClassData() {
        return Json.mapToJsonString(mServerExtras);
    }
}
