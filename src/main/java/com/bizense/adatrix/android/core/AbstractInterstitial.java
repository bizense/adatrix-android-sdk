package com.bizense.adatrix.android.core;

import java.util.Map;

import android.content.Context;

/*
 * AbstractInterstitial is a base class for custom events that support interstitials. By
 * implementing subclasses of AbstractInterstitial, you can enable the Adatrix SDK to natively
 * support a wider variety of third-party ad networks, or execute any of your application code on
 * demand.
 * 
 * At runtime, the Adatrix SDK will find and instantiate a AbstractInterstitial subclass as needed
 * and invoke its loadInterstitial() method.
 */
public abstract class AbstractInterstitial {
    
    /*
     * When the Adatrix SDK receives a response indicating it should load a custom event, it will send
     * this message to your custom event class. Your implementation of this method can either load
     * an interstitial ad from a third-party ad network, or execute any application code.
     * It must also notify the provided CustomEventInterstitial.Listener Object of certain lifecycle
     * events.
     * 
     * The localExtras parameter is a Map containing additional custom data that is set within
     * your application by calling Interstitial.setLocalExtras(Map<String, Object>). Note that
     * the localExtras Map is a copy of the Map supplied to setLocalExtras().
     * 
     * The serverExtras parameter is a Map containing additional custom data configurable on the
     * Adatrix website that you want to associate with a given custom event request. This data may be
     * used to pass dynamic information, such as publisher IDs, without changes in application code.
     */
    protected abstract void loadInterstitial(Context context,
            InterstitialListener interstitialListener,
            Map<String, Object> localExtras,
            Map<String, String> serverExtras);
    
    /*
     * Display the interstitial ad.
     */
    protected abstract void showInterstitial();
    
    /*
     * Called when a Custom Event is being invalidated or destroyed. Perform any final cleanup here.
     */
    protected abstract void onInvalidate();    
}
