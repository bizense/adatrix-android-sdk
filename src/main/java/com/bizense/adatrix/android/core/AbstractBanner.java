package com.bizense.adatrix.android.core;

import java.util.Map;

import android.content.Context;

public abstract class AbstractBanner {
	/*
     * When the Adatrix SDK receives a response indicating it should load a custom event, it will send
     * this message to your custom event class. Your implementation of this method can either load
     * a banner ad from a third-party ad network, or execute any application code. It must also
     * notify the provided CustomEventBanner.Listener Object of certain lifecycle events.
     * 
     * The localExtras parameter is a Map containing additional custom data that is set within
     * your application by calling AdView.setLocalExtras(Map<String, Object>). Note that the
     * localExtras Map is a copy of the Map supplied to setLocalExtras().
     * 
     * The serverExtras parameter is a Map containing additional custom data configurable on the
     * Adatrix website that you want to associate with a given custom event request. This data may be
     * used to pass dynamic information, such as publisher IDs, without changes in application code.
     */
    protected abstract void loadBanner(Context context, 
            BannerListener bannerListener, Map<String, 
            Object> localExtras,
            Map<String, String> serverExtras);
    
    /*
     * Called when a Custom Event is being invalidated or destroyed. Perform any final cleanup here.
     */
    protected abstract void onInvalidate();
}
