package com.bizense.adatrix.android.core;

import java.util.Collections;
import java.util.Map;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.webkit.CookieSyncManager;
import android.webkit.WebViewDatabase;
import android.widget.FrameLayout;

import com.bizense.adatrix.android.utils.ErrorCode;
import com.bizense.adatrix.android.utils.ResponseHeader;

public class AdView extends FrameLayout {

    public enum LocationAwareness {
        LOCATION_AWARENESS_NORMAL, LOCATION_AWARENESS_TRUNCATED, LOCATION_AWARENESS_DISABLED
    }

    public static final String HOST_FOR_TESTING = "adx1.exchange.adatrix.com";
    public static final String AD_HANDLER = "/get.ad";
    public static final int DEFAULT_LOCATION_PRECISION = 6;

    protected AdViewController mAdViewController;
    protected BannerAdapter mBannerAdapter;

    private Context mContext;
    private BroadcastReceiver mScreenStateReceiver;
    private boolean mIsInForeground;
    private LocationAwareness mLocationAwareness;
    private boolean mPreviousAutorefreshSetting = false;
    
    private BannerListener mBannerListener;
    
    public AdView(Context context) {
        this(context, null);
    }

    public AdView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
        mIsInForeground = (getVisibility() == VISIBLE);
        mLocationAwareness = LocationAwareness.LOCATION_AWARENESS_NORMAL;

        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);

        // There is a rare bug in Froyo/2.2 where creation of a WebView causes a
        // NullPointerException. (http://code.google.com/p/android/issues/detail?id=10789)
        // It happens when the WebView can't access the local file store to make a cache file.
        // Here, we'll work around it by trying to create a file store and then just go inert
        // if it's not accessible.
        if (WebViewDatabase.getInstance(context) == null) {
            Log.e("Adatrix", "Disabling Adatrix. Local cache file is inaccessible so Adatrix will " +
                    "fail if we try to create a WebView. Details of this Android bug found at:" +
                    "http://code.google.com/p/android/issues/detail?id=10789");
            return;
        }
        CookieSyncManager.createInstance(mContext);
		CookieSyncManager.getInstance().sync();
		CookieSyncManager.getInstance().startSync();
        mAdViewController = new AdViewController(context, this);
        registerScreenStateBroadcastReceiver();
    }

    private void registerScreenStateBroadcastReceiver() {
        if (mAdViewController == null) return;

        mScreenStateReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                    if (mIsInForeground) {
                        Log.d("Adatrix", "Screen sleep with ad in foreground, disable refresh");
                        if (mAdViewController != null) {
                            mPreviousAutorefreshSetting = mAdViewController.getAutorefreshEnabled();
                            mAdViewController.setAutorefreshEnabled(false);
                        }
                    } else {
                        Log.d("Adatrix", "Screen sleep but ad in background; " +
                                "refresh should already be disabled");
                    }
                } else if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
                    if (mIsInForeground) {
                        Log.d("Adatrix", "Screen wake / ad in foreground, reset refresh");
                        if (mAdViewController != null) {
                            mAdViewController.setAutorefreshEnabled(mPreviousAutorefreshSetting);
                        }
                    } else {
                        Log.d("Adatrix", "Screen wake but ad in background; don't enable refresh");
                    }
                }
            }
        };
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_USER_PRESENT);
        mContext.registerReceiver(mScreenStateReceiver, filter);
    }

    private void unregisterScreenStateBroadcastReceiver() {
        try {
            mContext.unregisterReceiver(mScreenStateReceiver);
        } catch (Exception IllegalArgumentException) {
            Log.d("Adatrix", "Failed to unregister screen state broadcast receiver (never registered).");
        }
    }

    public void loadAd() {
        if (mAdViewController != null) mAdViewController.loadAd();
    }

    /*
     * Tears down the ad view: no ads will be shown once this method executes. The parent
     * Activity's onDestroy implementation must include a call to this method.
     */
    public void destroy() {
        unregisterScreenStateBroadcastReceiver();
        removeAllViews();

        if (mAdViewController != null) {
            mAdViewController.cleanup();
            mAdViewController = null;
        }

        if (mBannerAdapter != null) {
            mBannerAdapter.invalidate();
            mBannerAdapter = null;
        }
        
		CookieSyncManager.getInstance().stopSync();

    }

    Integer getAdTimeoutDelay() {
        return (mAdViewController != null) ? mAdViewController.getAdTimeoutDelay() : null;
    }

    protected void loadFailUrl(ErrorCode errorCode) {
        if (mAdViewController != null) mAdViewController.loadFailUrl(errorCode);
    }

    public void load(Map<String, String> paramsMap) {
        if (paramsMap == null) {
            Log.d("Adatrix", "Couldn't invoke custom event because the server did not specify one.");
            loadFailUrl(ErrorCode.ADAPTER_NOT_FOUND);
            return;
        }

        if (mBannerAdapter != null) {
            mBannerAdapter.invalidate();
        }

        Log.d("Adatrix", "Loading custom event adapter.");

        mBannerAdapter = new BannerAdapter(this, 
        		paramsMap.get(ResponseHeader.CUSTOM_EVENT_NAME.getKey()), 
        		paramsMap.get(ResponseHeader.CUSTOM_EVENT_DATA.getKey()));
        
        mBannerAdapter.loadAd();
    }

    protected void registerClick() {
        if (mAdViewController != null) {
            mAdViewController.registerClick();

            // Let any listeners know that an ad was clicked
            adClicked();
        }
    }

    protected void trackNativeImpression() {
        Log.d("Adatrix", "Tracking impression for native adapter.");
        if (mAdViewController != null) mAdViewController.trackImpression();
    }

    @Override
    protected void onWindowVisibilityChanged(int visibility) {
        if (mAdViewController == null) return;

        if (visibility == VISIBLE) {
            Log.d("Adatrix", "Ad Unit ("+ mAdViewController.getZone()+") going visible: enabling refresh");
            mIsInForeground = true;
            mAdViewController.setAutorefreshEnabled(true);
        }
        else {
            Log.d("Adatrix", "Ad Unit ("+ mAdViewController.getZone()+") going invisible: disabling refresh");
            mIsInForeground = false;
            mAdViewController.setAutorefreshEnabled(false);
        }
    }

    protected void adLoaded() {
        Log.d("Adatrix", "adLoaded");        
        if(mBannerListener != null) mBannerListener.onBannerLoaded(this);        
    }

    protected void adFailed(ErrorCode errorCode) {
    	if(mBannerListener != null) mBannerListener.onBannerFailed(errorCode);        
    }

    protected void adPresentedOverlay() {
    	if(mBannerListener != null) mBannerListener.onBannerExpanded();
    }

    protected void adClosed() {
    	if(mBannerListener != null) mBannerListener.onBannerCollapsed();       
    }

    protected void adClicked() {
    	if(mBannerListener != null) mBannerListener.onBannerClicked();        
    }

    protected void nativeAdLoaded() {
        if (mAdViewController != null) mAdViewController.scheduleRefreshTimerIfEnabled();
        adLoaded();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public void setViewId(String viewId) {
        if (mAdViewController != null) mAdViewController.setViewId(viewId);
    }

    public String getViewId() {
        return (mAdViewController != null) ? mAdViewController.getViewId() : null;
    }
    
    public void setZone(String zone) {
        if (mAdViewController != null) mAdViewController.setZone(zone);
    }

    public String getZone() {
        return (mAdViewController != null) ? mAdViewController.getZone() : null;
    }
    
    public void setCategory(String category) {
        if (mAdViewController != null) mAdViewController.setCategory(category);
    }

    public String getCategory() {
        return (mAdViewController != null) ? mAdViewController.getCategory() : null;
    }

    public void setKeywords(String keywords) {
        if (mAdViewController != null) mAdViewController.setKeywords(keywords);
    }

    public String getKeywords() {
        return (mAdViewController != null) ? mAdViewController.getKeywords() : null;
    }

    public void setFacebookSupported(boolean enabled) {
        if (mAdViewController != null) mAdViewController.setFacebookSupported(enabled);
    }

    public boolean isFacebookSupported() {
        return (mAdViewController != null) ? mAdViewController.isFacebookSupported() : false;
    }

    public void setLocation(Location location) {
        if (mAdViewController != null) mAdViewController.setLocation(location);
    }

    public Location getLocation() {
        return (mAdViewController != null) ? mAdViewController.getLocation() : null;
    }

    public void setTimeout(int milliseconds) {
        if (mAdViewController != null) mAdViewController.setTimeout(milliseconds);
    }

    public int getAdWidth() {
        return (mAdViewController != null) ? mAdViewController.getAdWidth() : 0;
    }

    public int getAdHeight() {
        return (mAdViewController != null) ? mAdViewController.getAdHeight() : 0;
    }

    public String getResponseString() {
        return (mAdViewController != null) ? mAdViewController.getResponseString() : null;
    }

    public String getClickthroughUrl() {
        return (mAdViewController != null) ? mAdViewController.getClickthroughUrl() : null;
    }

    public Activity getActivity() {
        return (Activity) mContext;
    }

    public void setBannerListener(BannerListener listener) {
        mBannerListener = listener;
    }

    public BannerListener getBannerListener() {
        return mBannerListener;
    }

    public void setLocationAwareness(LocationAwareness awareness) {
        mLocationAwareness = awareness;
    }

    public LocationAwareness getLocationAwareness() {
        return mLocationAwareness;
    }

    public void setLocationPrecision(int precision) {
        if (mAdViewController != null) {
            mAdViewController.setLocationPrecision(precision);
        }
    }

    public int getLocationPrecision() {
        return (mAdViewController != null) ? mAdViewController.getLocationPrecision() : 0;
    }

    public void setLocalExtras(Map<String, Object> localExtras) {
        if (mAdViewController != null) mAdViewController.setLocalExtras(localExtras);
    }

    public Map<String, Object> getLocalExtras() {
        if (mAdViewController != null) return mAdViewController.getLocalExtras();
        return Collections.emptyMap();
    }

    public void setAutorefreshEnabled(boolean enabled) {
        if (mAdViewController != null) mAdViewController.setAutorefreshEnabled(enabled);
    }

    public boolean getAutorefreshEnabled() {
        if (mAdViewController != null) return mAdViewController.getAutorefreshEnabled();
        else {
            Log.d("Adatrix", "Can't get autorefresh status for destroyed AdView. " +
                    "Returning false.");
            return false;
        }
    }

    public void setAdContentView(View view) {
        if (mAdViewController != null) mAdViewController.setAdContentView(view);
    }

    public void setTesting(boolean testing) {
        if (mAdViewController != null) mAdViewController.setTesting(testing);
    }

    public boolean getTesting() {
        if (mAdViewController != null) return mAdViewController.getTesting();
        else {
            Log.d("Adatrix", "Can't get testing status for destroyed AdView. " +
                    "Returning false.");
            return false;
        }
    }

    public void forceRefresh() {
        if (mBannerAdapter != null) {
            mBannerAdapter.invalidate();
            mBannerAdapter = null;
        }

        if (mAdViewController != null) mAdViewController.forceRefresh();
    }

    AdViewController getAdViewController() {
        return mAdViewController;
    }
}
