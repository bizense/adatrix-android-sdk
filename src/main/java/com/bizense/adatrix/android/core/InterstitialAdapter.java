package com.bizense.adatrix.android.core;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.utils.ErrorCode;
import com.bizense.adatrix.android.utils.Json;

public class InterstitialAdapter implements InterstitialListener {
    public static final int DEFAULT_INTERSTITIAL_TIMEOUT_DELAY = 30000;

    private final Interstitial mInterstitial;
    private boolean mInvalidated;
    private InterstitialAdapterListener mInterstitialAdapterListener;
    private AbstractInterstitial mAbstractInterstitial;
    private Context mContext;
    private Map<String, Object> mLocalExtras;
    private Map<String, String> mServerExtras;
    private final Handler mHandler;
    private final Runnable mTimeout;

    public InterstitialAdapter(Interstitial interstitial, String className, String jsonParams) {
        mHandler = new Handler();
        mInterstitial = interstitial;
        mServerExtras = new HashMap<String, String>();
        mLocalExtras = new HashMap<String, Object>();
        mContext = interstitial.getActivity();
        mTimeout = new Runnable() {
            @Override
            public void run() {
                Log.d("Adatrix", "Third-party network timed out.");
                onInterstitialFailed(ErrorCode.NETWORK_TIMEOUT);
                invalidate();
            }
        };

        Log.d("Adatrix", "Attempting to invoke custom event: " + className);
        try {
            mAbstractInterstitial = create(className);
        } catch (Exception exception) {
            Log.d("Adatrix", "Couldn't locate or instantiate custom event: " + className + ".");
            if (mInterstitialAdapterListener != null) mInterstitialAdapterListener.onInterstitialFailed(ErrorCode.ADAPTER_NOT_FOUND);
        }
        
        // Attempt to load the JSON extras into mServerExtras.
        try {
            mServerExtras = Json.jsonStringToMap(jsonParams);
        } catch (Exception exception) {
            Log.d("Adatrix", "Failed to create Map from JSON: " + jsonParams);
        }
        
        mLocalExtras = interstitial.getLocalExtras();
        if (interstitial.getLocation() != null) {
            mLocalExtras.put("location", interstitial.getLocation());
        }

        AdViewController adViewController = interstitial.getInterstitialView().getAdViewController();
        if (adViewController != null) {
            mLocalExtras.put(AdxConstants.AD_CONFIGURATION_KEY, adViewController.getAdxConfiguration());
        }
    }
    
    AbstractInterstitial create(String className) throws Exception {
    	Class<? extends AbstractInterstitial> interstitialClass = Class.forName(className).asSubclass(AbstractInterstitial.class);
        Constructor<?> interstitialConstructor = interstitialClass.getDeclaredConstructor((Class[]) null);
        interstitialConstructor.setAccessible(true);
        return (AbstractInterstitial) interstitialConstructor.newInstance();
    }
    
    void loadInterstitial() {
        if (isInvalidated() || mAbstractInterstitial == null) {
            return;
        }
        mAbstractInterstitial.loadInterstitial(mContext, this, mLocalExtras, mServerExtras);

        if (getTimeoutDelayMilliseconds() > 0) {
            mHandler.postDelayed(mTimeout, getTimeoutDelayMilliseconds());
        }
    }
    
    void showInterstitial() {
        if (isInvalidated() || mAbstractInterstitial == null) return;
        
        mAbstractInterstitial.showInterstitial();
    }

    void invalidate() {
        if (mAbstractInterstitial != null) mAbstractInterstitial.onInvalidate();
        mAbstractInterstitial = null;
        mContext = null;
        mServerExtras = null;
        mLocalExtras = null;
        mInterstitialAdapterListener = null;
        mInvalidated = true;
    }

    boolean isInvalidated() {
        return mInvalidated;
    }

    void setAdapterListener(InterstitialAdapterListener listener) {
        mInterstitialAdapterListener = listener;
    }

    private void cancelTimeout() {
        mHandler.removeCallbacks(mTimeout);
    }

    private int getTimeoutDelayMilliseconds() {
        if (mInterstitial == null
                || mInterstitial.getAdTimeoutDelay() == null
                || mInterstitial.getAdTimeoutDelay() < 0) {
            return DEFAULT_INTERSTITIAL_TIMEOUT_DELAY;
        }

        return mInterstitial.getAdTimeoutDelay() * 1000;
    }

    interface InterstitialAdapterListener {
        void onInterstitialLoaded();
        void onInterstitialFailed(ErrorCode errorCode);
        void onInterstitialShown();
        void onInterstitialClicked();
        void onInterstitialDismissed();
    }

    /*
     * CustomEventInterstitial.Listener implementation
     */
    @Override
    public void onInterstitialLoaded() {
        if (isInvalidated()) {
            return;
        }

        if (mInterstitialAdapterListener != null) {
            cancelTimeout();
            mInterstitialAdapterListener.onInterstitialLoaded();
        }
    }

    @Override
    public void onInterstitialFailed(ErrorCode errorCode) {
        if (isInvalidated()) {
            return;
        }

        if (mInterstitialAdapterListener != null) {
            if (errorCode == null) {
                errorCode = ErrorCode.UNSPECIFIED;
            }
            cancelTimeout();
            mInterstitialAdapterListener.onInterstitialFailed(errorCode);
        }
    }

    @Override
    public void onInterstitialShown() {
        if (isInvalidated()) {
            return;
        }

        if (mInterstitialAdapterListener != null) {
        	mInterstitialAdapterListener.onInterstitialShown();
        }
    }

    @Override
    public void onInterstitialClicked() {
        if (isInvalidated()) {
            return;
        }

        if (mInterstitialAdapterListener != null) {
        	mInterstitialAdapterListener.onInterstitialClicked();
        }
    }

    @Override
    public void onLeaveApplication() {
        onInterstitialClicked();
    }

    @Override
    public void onInterstitialDismissed() {
        if (isInvalidated()) return;

        if (mInterstitialAdapterListener != null) mInterstitialAdapterListener.onInterstitialDismissed();
    }

    @Deprecated
    void setCustomEventInterstitial(AbstractInterstitial interstitial) {
        mAbstractInterstitial = interstitial;
    }
}
