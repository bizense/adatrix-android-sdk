package com.bizense.adatrix.android.core;

import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.bizense.adatrix.android.core.AdView.LocationAwareness;
import com.bizense.adatrix.android.utils.ErrorCode;
import com.bizense.adatrix.android.utils.ResponseHeader;

public class Interstitial implements InterstitialAdapter.InterstitialAdapterListener {

    private enum InterstitialState {
        CUSTOM_EVENT_AD_READY,
        NOT_READY;

        boolean isReady() {
            return this != InterstitialState.NOT_READY;
        }
    }

    private InterstitialView mInterstitialView;
    private InterstitialAdapter mInterstitialAdapter;
    private InterstitialListener mInterstitialListener;
    private Activity mActivity;
    private String mZone;
    private String mViewId;
    private String mCategory;
    private InterstitialState mCurrentInterstitialState;
    private boolean mIsDestroyed;

    public Interstitial(Activity activity, String zone) {
        mActivity = activity;
        mZone = zone;
        
        mInterstitialView = new InterstitialView(mActivity);
        mInterstitialView.setZone(mZone);

        mCurrentInterstitialState = InterstitialState.NOT_READY;

    }
    
    public Interstitial(Activity activity, String zone, String category) {
    	mActivity = activity;
        mZone = zone;
        mCategory = category;

        mInterstitialView = new InterstitialView(mActivity);
        mInterstitialView.setZone(mZone);
        mInterstitialView.setCategory(mCategory);

        mCurrentInterstitialState = InterstitialState.NOT_READY;
    }

    public void load() {
        resetCurrentInterstitial();
        mInterstitialView.loadAd();
    }

    public void forceRefresh() {
        resetCurrentInterstitial();
        mInterstitialView.forceRefresh();
    }

    private void resetCurrentInterstitial() {
        mCurrentInterstitialState = InterstitialState.NOT_READY;

        if (mInterstitialAdapter != null) {
            mInterstitialAdapter.invalidate();
            mInterstitialAdapter = null;
        }

        mIsDestroyed = false;
    }

    public boolean isReady() {
        return mCurrentInterstitialState.isReady();
    }

    boolean isDestroyed() {
        return mIsDestroyed;
    }

    public boolean show() {
        switch (mCurrentInterstitialState) {
            case CUSTOM_EVENT_AD_READY:
                showCustomEventInterstitial();
                return true;
            default:
            	return false;
        }     
    }

    private void showCustomEventInterstitial() {
        if (mInterstitialAdapter != null) mInterstitialAdapter.showInterstitial();
    }

    Integer getAdTimeoutDelay() {
        return mInterstitialView.getAdTimeoutDelay();
    }

    InterstitialView getInterstitialView() {
        return mInterstitialView;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public void setViewId(String viewId) {
    	mViewId = viewId;
    	mInterstitialView.setViewId(mViewId);
    }
    
    public void setKeywords(String keywords) {
        mInterstitialView.setKeywords(keywords);
    }

    public String getKeywords() {
        return mInterstitialView.getKeywords();
    }

    public void setFacebookSupported(boolean enabled) {
        mInterstitialView.setFacebookSupported(enabled);
    }

    public boolean isFacebookSupported() {
        return mInterstitialView.isFacebookSupported();
    }

    public Activity getActivity() {
        return mActivity;
    }

    public Location getLocation() {
        return mInterstitialView.getLocation();
    }

    public void destroy() {
        mIsDestroyed = true;

        if (mInterstitialAdapter != null) {
            mInterstitialAdapter.invalidate();
            mInterstitialAdapter = null;
        }

        mInterstitialView.setBannerListener(null);
        mInterstitialView.destroy();
    }

    public void setInterstitialAdListener(InterstitialListener listener) {
        mInterstitialListener = listener;
    }

    public InterstitialListener getInterstitialAdListener() {
        return mInterstitialListener;
    }

    public void setLocationAwareness(LocationAwareness awareness) {
        mInterstitialView.setLocationAwareness(awareness);
    }

    public LocationAwareness getLocationAwareness() {
        return mInterstitialView.getLocationAwareness();
    }

    public void setLocationPrecision(int precision) {
        mInterstitialView.setLocationPrecision(precision);
    }

    public int getLocationPrecision() {
        return mInterstitialView.getLocationPrecision();
    }

    public void setTesting(boolean testing) {
        mInterstitialView.setTesting(testing);
    }

    public boolean getTesting() {
        return mInterstitialView.getTesting();
    }

    public void setLocalExtras(Map<String, Object> extras) {
        mInterstitialView.setLocalExtras(extras);
    }

    public Map<String, Object> getLocalExtras() {
        return mInterstitialView.getLocalExtras();
    }

    /*
     * Implements CustomEventInterstitialAdapter.CustomEventInterstitialListener
     */

    @Override
    public void onInterstitialLoaded() {
        if (mIsDestroyed) return;

        mCurrentInterstitialState = InterstitialState.CUSTOM_EVENT_AD_READY;
        if(mInterstitialListener != null)
        	mInterstitialListener.onInterstitialLoaded();
    }

    @Override
    public void onInterstitialFailed(ErrorCode errorCode) {
        if (isDestroyed()) return;

        mCurrentInterstitialState = InterstitialState.NOT_READY;
        mInterstitialView.loadFailUrl(errorCode);
    }

    @Override
    public void onInterstitialShown() {
        if (isDestroyed()) return;

        mInterstitialView.trackImpression();
        if(mInterstitialListener != null)
       		mInterstitialListener.onInterstitialShown();
    }

    @Override
    public void onInterstitialClicked() {
        if (isDestroyed()) return;

        mInterstitialView.registerClick();
        if(mInterstitialListener != null) 
        	mInterstitialListener.onInterstitialClicked();        
    }

    @Override
    public void onInterstitialDismissed() {
        if (isDestroyed()) return;

        mCurrentInterstitialState = InterstitialState.NOT_READY;
        if(mInterstitialListener != null) 
        	mInterstitialListener.onInterstitialDismissed();        
    }
    
    public class InterstitialView extends AdView {

    	public InterstitialView(Context context) {
    		super(context);
    		setAutorefreshEnabled(false);
    	}

    	@Override
    	public void load(Map<String, String> paramsMap) {
    		if (paramsMap == null) {
    			Log.d("Adatrix", "Couldn't invoke custom event because the server did not specify one.");
    			loadFailUrl(ErrorCode.ADAPTER_NOT_FOUND);
    			return;
    		}

    		if (mInterstitialAdapter != null) {
    			mInterstitialAdapter.invalidate();
    		}

    		Log.d("Adatrix", "Loading custom event interstitial adapter.");

    		mInterstitialAdapter = new InterstitialAdapter(Interstitial.this,
    				paramsMap.get(ResponseHeader.CUSTOM_EVENT_NAME.getKey()),
    				paramsMap.get(ResponseHeader.CUSTOM_EVENT_DATA.getKey()));
    		mInterstitialAdapter.setAdapterListener(Interstitial.this);
    		mInterstitialAdapter.loadInterstitial();
    	}

    	protected void trackImpression() {
    		Log.d("Adatrix", "Tracking impression for interstitial.");
    		if (mAdViewController != null) mAdViewController.trackImpression();
    	}

    	@Override
    	protected void adFailed(ErrorCode errorCode) {
    		if(mInterstitialListener != null)
    			mInterstitialListener.onInterstitialFailed(errorCode);    		
    	}    	
    }
    
    public void setInterstitialView(InterstitialView interstitialView){
		mInterstitialView = interstitialView;
	}
}
