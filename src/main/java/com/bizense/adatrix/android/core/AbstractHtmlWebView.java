package com.bizense.adatrix.android.core;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.utils.AndroidVersion;
import com.bizense.adatrix.android.views.ViewGestureDetector;

public class AbstractHtmlWebView extends AbstractWebView implements ViewGestureDetector.UserClickListener {
	private final ViewGestureDetector mViewGestureDetector;
	private boolean mClicked;

	public AbstractHtmlWebView(Context context, AdxConfiguration adxConfiguration) {
		super(context);

		disableScrollingAndZoom();
		getSettings().setJavaScriptEnabled(true);

		mViewGestureDetector = new ViewGestureDetector(context, this, adxConfiguration);
		mViewGestureDetector.setUserClickListener(this);

		if (AndroidVersion.currentApiLevel().isAtLeast(AndroidVersion.ICE_CREAM_SANDWICH)) {
			enablePlugins(true);
		}
		setBackgroundColor(Color.TRANSPARENT);
	}

	public void init(boolean isScrollable) {
		initializeOnTouchListener(isScrollable);
	}

	@Override
	public void loadUrl(String url) {
		if (url == null) return;

		Log.d("Adatrix", "Loading url: " + url);
		if (url.startsWith("javascript:")) {
			super.loadUrl(url);
		}
	}

	private void disableScrollingAndZoom() {
		setHorizontalScrollBarEnabled(false);
		setHorizontalScrollbarOverlay(false);
		setVerticalScrollBarEnabled(false);
		setVerticalScrollbarOverlay(false);
		getSettings().setSupportZoom(false);
	}

	public void loadHtmlResponse(String htmlResponse) {
		loadDataWithBaseURL("http://" + AdxConstants.HOST_URL, htmlResponse, "text/html", "utf-8", null);
	}

	void initializeOnTouchListener(final boolean isScrollable) {
		setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				mViewGestureDetector.sendTouchEvent(event);

				// We're not handling events if the current action is ACTION_MOVE
				return (event.getAction() == MotionEvent.ACTION_MOVE) && !isScrollable;
			}
		});
	}

	@Override
	public void onUserClick() {
		mClicked = true;
	}

	@Override
	public void onResetUserClick() {
		mClicked = false;
	}

	@Override
	public boolean wasClicked() {
		return mClicked;
	}
}
