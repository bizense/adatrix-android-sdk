package com.bizense.adatrix.android.core;

import android.content.Context;
import android.widget.VideoView;

public abstract class AbstractVideoView extends VideoView{
	public interface VideoViewListener {
        void showCloseButton();
        void videoError(boolean shouldFinish);
        void videoCompleted(boolean shouldFinish);
        void videoClicked();
    }
	
	public AbstractVideoView(Context context) {
		super(context);
	}

    public void onResume() {

    }

    public void onPause() {

    }
}
