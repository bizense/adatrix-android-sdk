package com.bizense.adatrix.android.core;

import static android.Manifest.permission.ACCESS_NETWORK_STATE;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.core.AdView.LocationAwareness;
import com.bizense.adatrix.android.managers.HttpClientManager;
import com.bizense.adatrix.android.network.AdFetcher;
import com.bizense.adatrix.android.network.AdUrlGenerator;
import com.bizense.adatrix.android.utils.Dips;
import com.bizense.adatrix.android.utils.ErrorCode;

public class AdViewController {
	public static final int MINIMUM_REFRESH_TIME_MILLISECONDS = 10000;
	public static final int DEFAULT_REFRESH_TIME_MILLISECONDS = 60000;
	private static final FrameLayout.LayoutParams WRAP_AND_CENTER_LAYOUT_PARAMS =
			new FrameLayout.LayoutParams(
					FrameLayout.LayoutParams.WRAP_CONTENT,
					FrameLayout.LayoutParams.WRAP_CONTENT,
					Gravity.CENTER);
	private static WeakHashMap<View,Boolean> sViewShouldHonorServerDimensions = new WeakHashMap<View, Boolean>();;

	private final Context mContext;
	private AdView mAdView;
	private final AdUrlGenerator mUrlGenerator;
	private AdFetcher mAdFetcher;
	private AdxConfiguration mAdxConfiguration;
	private final Runnable mRefreshRunnable;

	private boolean mIsDestroyed;
	private Handler mHandler;
	private boolean mIsLoading;
	private String mUrl;

	private Map<String, Object> mLocalExtras = new HashMap<String, Object>();
	private boolean mAutoRefreshEnabled = true;
	private String mKeywords;
	private Location mLocation;
	private LocationAwareness mLocationAwareness = LocationAwareness.LOCATION_AWARENESS_NORMAL;
	private int mLocationPrecision = AdView.DEFAULT_LOCATION_PRECISION;
	private boolean mIsFacebookSupported = true;
	private boolean mIsTesting;

	public static void setShouldHonorServerDimensions(View view) {
		sViewShouldHonorServerDimensions.put(view, true);
	}

	private static boolean getShouldHonorServerDimensions(View view) {
		return sViewShouldHonorServerDimensions.get(view) != null;
	}

	public AdViewController(Context context, AdView view) {
		mContext = context;
		mAdView = view;

		mUrlGenerator = new AdUrlGenerator(context);
		mAdxConfiguration = new AdxConfiguration(mContext);

		mAdFetcher = new AdFetcher(this, mAdxConfiguration.getUserAgent());

		mRefreshRunnable = new Runnable() {
			public void run() {
				loadAd();
			}
		};

		mHandler = new Handler();
	}

	public AdView getAdView() {
		return mAdView;
	}

	public void loadAd() {
	    
		if (mAdxConfiguration.getZone() == null) {
			Log.d("Adatrix", "Can't load an ad in this ad view because the zone id is null. " +
					"Did you forget to call setZone()?");
			return;
		}

		if (!isNetworkAvailable()) {
			Log.d("Adatrix", "Can't load an ad because there is no network connectivity.");
			scheduleRefreshTimerIfEnabled();
			return;
		}

		if (mLocation == null) {
			mLocation = getLastKnownLocation();
		}

		// tested (remove me when the rest of this is tested)
		String adUrl = generateAdUrl();
		loadNonJavascript(adUrl);
	}

	void loadNonJavascript(String url) {
		if (url == null) return;

		Log.d("Adatrix", "Loading url: " + url);
		if (mIsLoading) {
			if (mAdxConfiguration.getZone() != null) {
				Log.i("Adatrix", "Already loading an ad for " + mAdxConfiguration.getZone() + ", wait to finish.");
			}
			return;
		}

		mUrl = url;
		mAdxConfiguration.setFailUrl(null);
		mIsLoading = true;

		fetchAd(mUrl);
	}

	public void reload() {
		Log.d("Adatrix", "Reload ad: " + mUrl);
		loadNonJavascript(mUrl);
	}

	void loadFailUrl(ErrorCode errorCode) {
		mIsLoading = false;

		Log.v("Adatrix", "ErrorCode: " + (errorCode == null ? "" : errorCode.toString()));

		if (mAdxConfiguration.getFailUrl() != null) {
			Log.d("Adatrix", "Loading failover url: " + mAdxConfiguration.getFailUrl());
			loadNonJavascript(mAdxConfiguration.getFailUrl());
		} else {
			// No other URLs to try, so signal a failure.
			adDidFail(ErrorCode.NO_FILL);
		}
	}

	void setFailUrl(String failUrl) {
		mAdxConfiguration.setFailUrl(failUrl);
	}

	public void setNotLoading() {
		this.mIsLoading = false;
	}

	public String getKeywords() {
		return mKeywords;
	}

	public void setKeywords(String keywords) {
		mKeywords = keywords;
	}

	public boolean isFacebookSupported() {
		return mIsFacebookSupported;
	}

	public void setFacebookSupported(boolean enabled) {
		mIsFacebookSupported = enabled;
	}

	public Location getLocation() {
		return mLocation;
	}

	public void setLocation(Location location) {
		mLocation = location;
	}

	public String getViewId() {
		return mAdxConfiguration.getViewId();
	}

	public void setViewId(String viewId) {
		mAdxConfiguration.setViewId(viewId);
	}
 	
	public String getZone() {
		return mAdxConfiguration.getZone();
	}

	public void setZone(String zone) {
		mAdxConfiguration.setZone(zone);
	}
	
	public String getCategory() {
		return mAdxConfiguration.getCategory();
	}

	public void setCategory(String category) {
		mAdxConfiguration.setCategory(category);
	}

	public void setTimeout(int milliseconds) {
		if (mAdFetcher != null) {
			mAdFetcher.setTimeout(milliseconds);
		}
	}

	public int getAdWidth() {
		return mAdxConfiguration.getWidth();
	}

	public int getAdHeight() {
		return mAdxConfiguration.getHeight();
	}

	public String getClickthroughUrl() {
		return mAdxConfiguration.getClickthroughUrl();
	}

	public String getRedirectUrl() {
		return mAdxConfiguration.getRedirectUrl();
	}

	public String getResponseString() {
		return mAdxConfiguration.getResponseString();
	}

	public boolean getAutorefreshEnabled() {
		return mAutoRefreshEnabled;
	}

	public void setAutorefreshEnabled(boolean enabled) {
		mAutoRefreshEnabled = enabled;

		if (mAdxConfiguration.getZone() != null) {
			Log.d("Adatrix", "Automatic refresh for " + mAdxConfiguration + " set to: " + enabled + ".");

		}

		if (mAutoRefreshEnabled) {
			scheduleRefreshTimerIfEnabled();
		} else {
			cancelRefreshTimer();
		}
	}

	public boolean getTesting() {
		return mIsTesting;
	}

	public void setTesting(boolean enabled) {
		mIsTesting = enabled;
	}

	int getLocationPrecision() {
		return mLocationPrecision;
	}

	void setLocationPrecision(int precision) {
		mLocationPrecision = Math.max(0, precision);
	}

	public AdxConfiguration getAdxConfiguration() {
		return mAdxConfiguration;
	}

	public boolean isDestroyed() {
		return mIsDestroyed;
	}

	/*
	 * Clean up the internal state of the AdViewController.
	 */
	void cleanup() {
		if (mIsDestroyed) {
			return;
		}

		setAutorefreshEnabled(false);
		cancelRefreshTimer();

		// WebView subclasses are not garbage-collected in a timely fashion on Froyo and below,
		// thanks to some persistent references in WebViewCore. We manually release some resources
		// to compensate for this "leak".

		mAdFetcher.cleanup();
		mAdFetcher = null;

		mAdxConfiguration.cleanup();

		mAdView = null;

		// Flag as destroyed. LoadUrlTask checks this before proceeding in its onPostExecute().
		mIsDestroyed = true;
	}

	public void configureUsingHttpResponse(final HttpResponse response) {
		mAdxConfiguration.addHttpResponse(response);
	}

	Integer getAdTimeoutDelay() {
		return mAdxConfiguration.getAdTimeoutDelay();
	}

	public int getRefreshTimeMilliseconds() {
		return mAdxConfiguration.getRefreshTimeMilliseconds();
	}


	void trackImpression() {
		new Thread(new Runnable() {
			public void run () {
				if (mAdxConfiguration.getImpressionUrl() == null) return;

				DefaultHttpClient httpClient = (DefaultHttpClient) HttpClientManager.create(0);
				try {
					HttpGet httpget = new HttpGet(mAdxConfiguration.getImpressionUrl());
					httpget.addHeader("User-Agent", mAdxConfiguration.getUserAgent());
					httpClient.execute(httpget);
				} catch (Exception e) {
					Log.d("Adatrix", "Impression tracking failed : " + mAdxConfiguration.getImpressionUrl(), e);
				} finally {
					httpClient.getConnectionManager().shutdown();
				}
			}
		}).start();
	}

	void registerClick() {
		new Thread(new Runnable() {
			public void run () {
				if (mAdxConfiguration.getClickthroughUrl() == null) return;

				DefaultHttpClient httpClient = (DefaultHttpClient) HttpClientManager.create(0);
				try {
					Log.d("Adatrix", "Tracking click for: " + mAdxConfiguration.getClickthroughUrl());
					HttpGet httpget = new HttpGet(mAdxConfiguration.getClickthroughUrl());
					httpget.addHeader("User-Agent", mAdxConfiguration.getUserAgent());
					httpClient.execute(httpget);
				} catch (Exception e) {
					Log.d("Adatrix", "Click tracking failed: " + mAdxConfiguration.getClickthroughUrl(), e);
				} finally {
					httpClient.getConnectionManager().shutdown();
				}
			}
		}).start();
	}

	void fetchAd(String mUrl) {
		if (mAdFetcher != null) {
			mAdFetcher.fetchAdForUrl(mUrl);
		}
	}

	void forceRefresh() {
		setNotLoading();
		loadAd();
	}

    String generateAdUrl() {
		return mUrlGenerator
				.withZone(mAdxConfiguration.getZone())
				.withViewId(mAdxConfiguration.getViewId())
				.withCategory(mAdxConfiguration.getCategory())
				.withKeywords(mKeywords)
				.withLocalExtras(mLocalExtras)
				.withFacebookSupported(mIsFacebookSupported)
				.withLocation(mLocation)
				.generateUrlString(getServerHostname());
	}

	public void adDidFail(ErrorCode errorCode) {
		Log.i("Adatrix", "Ad failed to load.");
		setNotLoading();
		scheduleRefreshTimerIfEnabled();
		getAdView().adFailed(errorCode);
	}

	void scheduleRefreshTimerIfEnabled() {
		cancelRefreshTimer();
		if (mAutoRefreshEnabled && mAdxConfiguration.getRefreshTimeMilliseconds() > 0) {
			mHandler.postDelayed(mRefreshRunnable, mAdxConfiguration.getRefreshTimeMilliseconds());
		}

	}

	void setLocalExtras(Map<String, Object> localExtras) {
		mLocalExtras = (localExtras != null)
				? new HashMap<String,Object>(localExtras)
						: new HashMap<String,Object>();
	}

	Map<String, Object> getLocalExtras() {
		return (mLocalExtras != null)
				? new HashMap<String,Object>(mLocalExtras)
						: new HashMap<String,Object>();
	}

	private void cancelRefreshTimer() {
		mHandler.removeCallbacks(mRefreshRunnable);
	}

	private String getServerHostname() {
		return mIsTesting ? AdView.HOST_FOR_TESTING : AdxConstants.HOST_URL;
	}

	private boolean isNetworkAvailable() {
		// If we don't have network state access, just assume the network is up.
		int result = mContext.checkCallingPermission(ACCESS_NETWORK_STATE);
		if (result == PackageManager.PERMISSION_DENIED) return true;

		// Otherwise, perform the connectivity check.
		ConnectivityManager cm
		= (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		return networkInfo != null && networkInfo.isConnected();
	}

	public void setAdContentView(final View view) {
		// XXX: This method is called from the WebViewClient's callbacks, which has caused an error on a small portion of devices
		// We suspect that the code below may somehow be running on the wrong UI Thread in the rare case.
		// see: http://stackoverflow.com/questions/10426120/android-got-calledfromwrongthreadexception-in-onpostexecute-how-could-it-be
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				AdView adView = getAdView();
				if (adView == null) {
					return;
				}
				adView.removeAllViews();
				adView.addView(view, getAdLayoutParams(view));
			}
		});
	}

	private FrameLayout.LayoutParams getAdLayoutParams(View view) {
		int width = mAdxConfiguration.getWidth();
		int height = mAdxConfiguration.getHeight();

		if (getShouldHonorServerDimensions(view) && width > 0 && height > 0) {
			int scaledWidth = Dips.asIntPixels(width, mContext);
			int scaledHeight = Dips.asIntPixels(height, mContext);

			return new FrameLayout.LayoutParams(scaledWidth, scaledHeight, Gravity.CENTER);
		} else {
			return WRAP_AND_CENTER_LAYOUT_PARAMS;
		}
	}

	/*
	 * Returns the last known location of the device using its GPS and network location providers.
	 * May be null if:
	 * - Location permissions are not requested in the Android manifest file
	 * - The location providers don't exist
	 * - Location awareness is disabled in the parent AdView
	 */
	private Location getLastKnownLocation() {
		Location result;

		if (mLocationAwareness == LocationAwareness.LOCATION_AWARENESS_DISABLED) {
			return null;
		}

		LocationManager lm = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
		Location gpsLocation = null;
		try {
			gpsLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		} catch (SecurityException e) {
			Log.d("Adatrix", "Failed to retrieve GPS location: access appears to be disabled.");
		} catch (IllegalArgumentException e) {
			Log.d("Adatrix", "Failed to retrieve GPS location: device has no GPS provider.");
		}

		Location networkLocation = null;
		try {
			networkLocation = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		} catch (SecurityException e) {
			Log.d("Adatrix", "Failed to retrieve network location: access appears to be disabled.");
		} catch (IllegalArgumentException e) {
			Log.d("Adatrix", "Failed to retrieve network location: device has no network provider.");
		}

		if (gpsLocation == null && networkLocation == null) {
			return null;
		}
		else if (gpsLocation != null && networkLocation != null) {
			if (gpsLocation.getTime() > networkLocation.getTime()) result = gpsLocation;
			else result = networkLocation;
		}
		else if (gpsLocation != null) result = gpsLocation;
		else result = networkLocation;

		// Truncate latitude/longitude to the number of digits specified by locationPrecision.
		if (mLocationAwareness == LocationAwareness.LOCATION_AWARENESS_TRUNCATED) {
			double lat = result.getLatitude();
			double truncatedLat = BigDecimal.valueOf(lat)
					.setScale(mLocationPrecision, BigDecimal.ROUND_HALF_DOWN)
					.doubleValue();
			result.setLatitude(truncatedLat);

			double lon = result.getLongitude();
			double truncatedLon = BigDecimal.valueOf(lon)
					.setScale(mLocationPrecision, BigDecimal.ROUND_HALF_DOWN)
					.doubleValue();
			result.setLongitude(truncatedLon);
		}

		return result;
	}
	
	public void setRefreshTimeMilliseconds(int refreshTimeMilliseconds) {
        mAdxConfiguration.setRefreshTimeMilliseconds(refreshTimeMilliseconds);
    }
}
