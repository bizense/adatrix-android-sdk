package com.bizense.adatrix.android.resources;

import static org.fest.assertions.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;

import android.app.Activity;

import com.bizense.adatrix.android.test.SdkTestRunner;

@RunWith(SdkTestRunner.class)
public class DrawablesTest {
    @Test
    public void decodeImage_shouldCacheDrawables() throws Exception {
        assertThat(Drawables.BACKGROUND.decodeImage(new Activity()))
                .isSameAs(Drawables.BACKGROUND.decodeImage(new Activity()));
    }
}
