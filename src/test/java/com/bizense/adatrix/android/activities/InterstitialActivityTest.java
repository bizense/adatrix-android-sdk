package com.bizense.adatrix.android.activities;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.robolectric.Robolectric.shadowOf;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowLocalBroadcastManager;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.view.View;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.core.InterstitialListener;
import com.bizense.adatrix.android.html.HtmlInterstitialWebView;
import com.bizense.adatrix.android.html.HtmlInterstitialWebView.AdxUriJavascriptFireFinishLoadListener;
import com.bizense.adatrix.android.test.SdkTestRunner;
import com.bizense.adatrix.android.test.TestHtmlInterstitialWebViewFactory;
import com.bizense.adatrix.android.utils.ErrorCode;

@RunWith(SdkTestRunner.class)
public class InterstitialActivityTest extends AbstractInterstitialActivityTest {
    public static final String EXPECTED_HTML_DATA = "htmlData";
    public static final boolean EXPECTED_IS_SCROLLABLE = true;
    public static final String EXPECTED_REDIRECT_URL = "redirectUrl";
    public static final String EXPECTED_CLICKTHROUGH_URL = "http://expected_url";

    private HtmlInterstitialWebView htmlInterstitialWebView;
    private Activity context;
    private InterstitialListener interstitialListener;

    @Before
    public void setUp() throws Exception {
        super.setup();
        Intent interstitialActivityIntent = createInterstitialActivityIntent(EXPECTED_HTML_DATA, EXPECTED_IS_SCROLLABLE, EXPECTED_REDIRECT_URL, EXPECTED_CLICKTHROUGH_URL, adxConfiguration);
        htmlInterstitialWebView = TestHtmlInterstitialWebViewFactory.getSingletonMock();
        resetMockedView(htmlInterstitialWebView);
        subject = Robolectric.buildActivity(InterstitialActivity.class).withIntent(interstitialActivityIntent).create().get();

        context = new Activity();
        interstitialListener = mock(InterstitialListener.class);

        reset(htmlInterstitialWebView);
        resetMockedView(htmlInterstitialWebView);
    }

    @Test
    public void preRenderHtml_shouldPreloadTheHtml() throws Exception {
        String htmlData = "this is nonsense";
        InterstitialActivity.preRenderHtml(context, interstitialListener, htmlData);

        verify(htmlInterstitialWebView).enablePlugins(eq(false));
        verify(htmlInterstitialWebView).addAdxUriJavascriptInterface(any(HtmlInterstitialWebView.AdxUriJavascriptFireFinishLoadListener.class));
        verify(htmlInterstitialWebView).loadHtmlResponse(htmlData);
    }

    @Test
    public void preRenderHtml_shouldHaveAWebViewClientThatForwardsFinishLoad() throws Exception {
    	InterstitialActivity.preRenderHtml(context, interstitialListener, null);

        ArgumentCaptor<WebViewClient> webViewClientCaptor = ArgumentCaptor.forClass(WebViewClient.class);
        verify(htmlInterstitialWebView).setWebViewClient(webViewClientCaptor.capture());
        WebViewClient webViewClient = webViewClientCaptor.getValue();

        webViewClient.shouldOverrideUrlLoading(null, "adx://finishLoad");

        verify(interstitialListener).onInterstitialLoaded();
        verify(interstitialListener, never()).onInterstitialFailed(any(ErrorCode.class));
    }

    @Test
    public void preRenderHtml_shouldHaveAWebViewClientThatForwardsFailLoad() throws Exception {
    	InterstitialActivity.preRenderHtml(context, interstitialListener, null);

        ArgumentCaptor<WebViewClient> webViewClientCaptor = ArgumentCaptor.forClass(WebViewClient.class);
        verify(htmlInterstitialWebView).setWebViewClient(webViewClientCaptor.capture());
        WebViewClient webViewClient = webViewClientCaptor.getValue();

        webViewClient.shouldOverrideUrlLoading(null, "adx://failLoad");

        verify(interstitialListener, never()).onInterstitialLoaded();
        verify(interstitialListener).onInterstitialFailed(any(ErrorCode.class));
    }

    @Test
    public void preRenderHtml_shouldHaveAAdxUriInterfaceThatForwardsOnInterstitialLoaded() throws Exception {
    	InterstitialActivity.preRenderHtml(context, interstitialListener, null);

        ArgumentCaptor<AdxUriJavascriptFireFinishLoadListener> adxUriJavascriptFireFinishLoadListenerCaptor = ArgumentCaptor.forClass(AdxUriJavascriptFireFinishLoadListener.class);
        verify(htmlInterstitialWebView).addAdxUriJavascriptInterface(adxUriJavascriptFireFinishLoadListenerCaptor.capture());
        AdxUriJavascriptFireFinishLoadListener adxUriJavascriptFireFinishLoadListener = adxUriJavascriptFireFinishLoadListenerCaptor.getValue();

        adxUriJavascriptFireFinishLoadListener.onInterstitialLoaded();

        verify(interstitialListener).onInterstitialLoaded();
    }

    @Test
    public void onCreate_shouldLayoutWebView() throws Exception {
        subject.onCreate(null);

        ArgumentCaptor<RelativeLayout.LayoutParams> captor = ArgumentCaptor.forClass(RelativeLayout.LayoutParams.class);
        verify(htmlInterstitialWebView).setLayoutParams(captor.capture());
        RelativeLayout.LayoutParams actualLayoutParams = captor.getValue();

        assertThat(actualLayoutParams.width).isEqualTo(RelativeLayout.LayoutParams.FILL_PARENT);
        assertThat(actualLayoutParams.height).isEqualTo(RelativeLayout.LayoutParams.WRAP_CONTENT);
        assertOnlyOneRuleSet(actualLayoutParams, RelativeLayout.CENTER_IN_PARENT);
    }

    @Test
    public void getAdView_shouldReturnPopulatedHtmlWebView() throws Exception {
        View adView = subject.getAdView();

        assertThat(adView).isSameAs(htmlInterstitialWebView);
        assertThat(TestHtmlInterstitialWebViewFactory.getLatestListener()).isNotNull();
        assertThat(TestHtmlInterstitialWebViewFactory.getLatestIsScrollable()).isEqualTo(EXPECTED_IS_SCROLLABLE);
        assertThat(TestHtmlInterstitialWebViewFactory.getLatestClickthroughUrl()).isEqualTo(EXPECTED_CLICKTHROUGH_URL);
        assertThat(TestHtmlInterstitialWebViewFactory.getLatestRedirectUrl()).isEqualTo(EXPECTED_REDIRECT_URL);
        verify(htmlInterstitialWebView).loadHtmlResponse(EXPECTED_HTML_DATA);
    }

    @Test
    public void onDestroy_shouldDestroyAdView() throws Exception {
        subject.onCreate(null);
        subject.onDestroy();

        verify(htmlInterstitialWebView).destroy();
        assertThat(getContentView(subject).getChildCount()).isEqualTo(0);
    }

    @Test
    public void onDestroy_shouldFireJavascriptWebviewDidClose() throws Exception {
        subject.onCreate(null);
        subject.onDestroy();

        verify(htmlInterstitialWebView).loadUrl(eq("javascript:webviewDidClose();"));
    }

    @Test
    public void start_shouldStartInterstitialActivityWithCorrectParameters() throws Exception {
    	InterstitialActivity.start(subject, "expectedResponse", true, "redirectUrl", "clickthroughUrl", adxConfiguration);

        Intent nextStartedActivity = Robolectric.getShadowApplication().getNextStartedActivity();
        assertThat(nextStartedActivity.getStringExtra(AdxConstants.HTML_RESPONSE_BODY_KEY)).isEqualTo("expectedResponse");
        assertThat(nextStartedActivity.getBooleanExtra(AdxConstants.SCROLLABLE_KEY, false)).isTrue();
        assertThat(nextStartedActivity.getStringExtra(AdxConstants.REDIRECT_URL_KEY)).isEqualTo("redirectUrl");
        assertThat(nextStartedActivity.getStringExtra(AdxConstants.CLICKTHROUGH_URL_KEY)).isEqualTo("clickthroughUrl");
        assertThat(nextStartedActivity.getFlags() & Intent.FLAG_ACTIVITY_NEW_TASK).isNotEqualTo(0);
        assertThat(nextStartedActivity.getComponent().getClassName()).isEqualTo("com.bizense.adatrix.android.activities.InterstitialActivity");
    }

    @Test
    public void getAdView_shouldCreateHtmlInterstitialWebViewAndLoadResponse() throws Exception {
        subject.getAdView();

        assertThat(TestHtmlInterstitialWebViewFactory.getLatestListener()).isNotNull();
        assertThat(TestHtmlInterstitialWebViewFactory.getLatestIsScrollable()).isEqualTo(EXPECTED_IS_SCROLLABLE);
        assertThat(TestHtmlInterstitialWebViewFactory.getLatestRedirectUrl()).isEqualTo(EXPECTED_REDIRECT_URL);
        assertThat(TestHtmlInterstitialWebViewFactory.getLatestClickthroughUrl()).isEqualTo(EXPECTED_CLICKTHROUGH_URL);
        verify(htmlInterstitialWebView).loadHtmlResponse(EXPECTED_HTML_DATA);
    }

    @Test
    public void getAdView_shouldSetUpForBroadcastingClicks() throws Exception {
        subject.getAdView();
        BroadcastReceiver broadcastReceiver = mock(BroadcastReceiver.class);
        ShadowLocalBroadcastManager.getInstance(subject).registerReceiver(broadcastReceiver, AbstractInterstitialActivity.HTML_INTERSTITIAL_INTENT_FILTER);

        TestHtmlInterstitialWebViewFactory.getLatestListener().onInterstitialClicked();

        ArgumentCaptor<Intent> intentCaptor = ArgumentCaptor.forClass(Intent.class);
        verify(broadcastReceiver).onReceive(eq(subject), intentCaptor.capture());
        Intent intent = intentCaptor.getValue();
        assertThat(intent.getAction()).isEqualTo(AbstractInterstitialActivity.ACTION_INTERSTITIAL_CLICK);
    }

    @Test
    public void getAdView_shouldSetUpForBroadcastingFail() throws Exception {
        subject.getAdView();
        BroadcastReceiver broadcastReceiver = mock(BroadcastReceiver.class);
        ShadowLocalBroadcastManager.getInstance(subject).registerReceiver(broadcastReceiver, AbstractInterstitialActivity.HTML_INTERSTITIAL_INTENT_FILTER);

        TestHtmlInterstitialWebViewFactory.getLatestListener().onInterstitialFailed(ErrorCode.UNSPECIFIED);

        ArgumentCaptor<Intent> intentCaptor = ArgumentCaptor.forClass(Intent.class);
        verify(broadcastReceiver).onReceive(eq(subject), intentCaptor.capture());
        Intent intent = intentCaptor.getValue();
        assertThat(intent.getAction()).isEqualTo(AbstractInterstitialActivity.ACTION_INTERSTITIAL_FAIL);

        assertThat(shadowOf(subject).isFinishing()).isTrue();
    }

    @Test
    public void broadcastingInterstitialListener_onInterstitialLoaded_shouldCallJavascriptWebViewDidAppear() throws Exception {
        InterstitialActivity.BroadcastingInterstitialListener broadcastingInterstitialListener = ((InterstitialActivity) subject).new BroadcastingInterstitialListener();

        broadcastingInterstitialListener.onInterstitialLoaded();

        verify(htmlInterstitialWebView).loadUrl(eq("javascript:webviewDidAppear();"));
    }

    @Test
    public void broadcastingInterstitialListener_onInterstitialFailed_shouldBroadcastFailAndFinish() throws Exception {
        Intent expectedIntent = new Intent(AbstractInterstitialActivity.ACTION_INTERSTITIAL_FAIL);
        ShadowLocalBroadcastManager.getInstance(subject).registerReceiver(broadcastReceiver, AbstractInterstitialActivity.HTML_INTERSTITIAL_INTENT_FILTER);

        InterstitialActivity.BroadcastingInterstitialListener broadcastingInterstitialListener = ((InterstitialActivity) subject).new BroadcastingInterstitialListener();
        broadcastingInterstitialListener.onInterstitialFailed(null);

        verify(broadcastReceiver).onReceive(eq(subject), eq(expectedIntent));
        assertThat(shadowOf(subject).isFinishing()).isTrue();
    }

    @Test
    public void broadcastingInterstitialListener_onInterstitialClicked_shouldBroadcastClick() throws Exception {
        Intent expectedIntent = new Intent(AbstractInterstitialActivity.ACTION_INTERSTITIAL_CLICK);
        ShadowLocalBroadcastManager.getInstance(subject).registerReceiver(broadcastReceiver, AbstractInterstitialActivity.HTML_INTERSTITIAL_INTENT_FILTER);

        InterstitialActivity.BroadcastingInterstitialListener broadcastingInterstitialListener = ((InterstitialActivity) subject).new BroadcastingInterstitialListener();
        broadcastingInterstitialListener.onInterstitialClicked();

        verify(broadcastReceiver).onReceive(eq(subject), eq(expectedIntent));
    }

    private Intent createInterstitialActivityIntent(String htmlData, boolean isScrollable, String redirectUrl, String clickthroughUrl, AdxConfiguration adxConfiguration) {
        return InterstitialActivity.createIntent(new Activity(), htmlData, isScrollable, redirectUrl, clickthroughUrl, adxConfiguration);
    }

    private void assertOnlyOneRuleSet(LayoutParams layoutParams, int desiredRule) {
        int[] rules = layoutParams.getRules();
        for (int ruleIndex = 0; ruleIndex < rules.length; ruleIndex++) {
            int currentRule = rules[ruleIndex];
            if (ruleIndex == desiredRule) {
                assertThat(currentRule).isNotEqualTo(0);
            } else {
                assertThat(currentRule).isEqualTo(0);
            }
        }
    }
}

