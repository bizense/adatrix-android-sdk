package com.bizense.adatrix.android.test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;
import android.content.Context;
import android.webkit.WebSettings;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.core.BannerListener;
import com.bizense.adatrix.android.html.HtmlBannerWebView;

public class TestHtmlBannerWebViewFactory {
	protected static TestHtmlBannerWebViewFactory instance = new TestHtmlBannerWebViewFactory();
    private HtmlBannerWebView mockHtmlBannerWebView = mock(HtmlBannerWebView.class);
    private BannerListener latestListener;
    private boolean latestIsScrollable;
    private String latestRedirectUrl;
    private String latestClickthroughUrl;

    public TestHtmlBannerWebViewFactory() {
        WebSettings webSettings = mock(WebSettings.class);
        stub(mockHtmlBannerWebView.getSettings()).toReturn(webSettings);
        stub(webSettings.getUserAgentString()).toReturn("Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
    }

    public static HtmlBannerWebView getSingletonMock() {
        return getTestFactory().mockHtmlBannerWebView;
    }

    private static TestHtmlBannerWebViewFactory getTestFactory() {
        return (TestHtmlBannerWebViewFactory) instance;
    }

   public HtmlBannerWebView create(
            Context context, BannerListener
            customEventBannerListener,
            boolean isScrollable,
            String redirectUrl,
            String clickthroughUrl, AdxConfiguration adxConfiguration) {
        latestListener = customEventBannerListener;
        latestIsScrollable = isScrollable;
        latestRedirectUrl = redirectUrl;
        latestClickthroughUrl = clickthroughUrl;
        return mockHtmlBannerWebView;
    }

    public static BannerListener getLatestListener() {
        return getTestFactory().latestListener;
    }

    public static boolean getLatestIsScrollable() {
        return getTestFactory().latestIsScrollable;
    }

    public static String getLatestRedirectUrl() {
        return getTestFactory().latestRedirectUrl;
    }

    public static String getLatestClickthroughUrl() {
        return getTestFactory().latestClickthroughUrl;
    }
}
