package com.bizense.adatrix.android.test;

import static org.mockito.Mockito.mock;

import com.bizense.adatrix.android.core.AdView;
import com.bizense.adatrix.android.core.BannerAdapter;

public class TestBannerAdapterFactory {
	protected static TestBannerAdapterFactory instance = new TestBannerAdapterFactory();
    private BannerAdapter mockBannerAdapter = mock(BannerAdapter.class);
    private AdView adView;
    private String className;
    private String classData;

    public static TestBannerAdapterFactory getTestFactory() {
        return ((TestBannerAdapterFactory) instance);
    }

    public BannerAdapter create(AdView adView, String className, String classData) {
        this.adView = adView;
        this.className = className;
        this.classData = classData;
        return mockBannerAdapter;
    }

    public static AdView getLatestAdView() {
        return getTestFactory().adView;
    }

    public static String getLatestClassName() {
        return getTestFactory().className;
    }

    public static String getLatestClassData() {
        return getTestFactory().classData;
    }
}
