package com.bizense.adatrix.android.test;

import static org.mockito.Mockito.mock;
import android.content.Context;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.core.InterstitialListener;
import com.bizense.adatrix.android.html.HtmlInterstitialWebView;

public class TestHtmlInterstitialWebViewFactory {
    private HtmlInterstitialWebView mockHtmlInterstitialWebView = mock(HtmlInterstitialWebView.class);
    protected static TestHtmlInterstitialWebViewFactory instance = new TestHtmlInterstitialWebViewFactory();
    
    private InterstitialListener latestListener;
    private boolean latestIsScrollable;
    private String latestRedirectUrl;
    private String latestClickthroughUrl;
    private AdxConfiguration latestAdxConfiguration;

    public static HtmlInterstitialWebView getSingletonMock() {
        return getTestFactory().mockHtmlInterstitialWebView;
    }

    private static TestHtmlInterstitialWebViewFactory getTestFactory() {
        return (TestHtmlInterstitialWebViewFactory) instance;
    }

    public HtmlInterstitialWebView create(Context context, InterstitialListener interstitialListener, boolean isScrollable, String redirectUrl, String clickthroughUrl, AdxConfiguration adxConfiguration) {
        latestListener = interstitialListener;
        latestIsScrollable = isScrollable;
        latestRedirectUrl = redirectUrl;
        latestClickthroughUrl = clickthroughUrl;
        latestAdxConfiguration = adxConfiguration;
        return getTestFactory().mockHtmlInterstitialWebView;
    }

    public static InterstitialListener getLatestListener() {
        return getTestFactory().latestListener;
    }

    public static boolean getLatestIsScrollable() {
        return getTestFactory().latestIsScrollable;
    }
    public static String getLatestRedirectUrl() {
        return getTestFactory().latestRedirectUrl;
    }

    public static String getLatestClickthroughUrl() {
        return getTestFactory().latestClickthroughUrl;
    }

    public static AdxConfiguration getLatestAdxConfiguration() {
        return getTestFactory().latestAdxConfiguration;
    }
}
