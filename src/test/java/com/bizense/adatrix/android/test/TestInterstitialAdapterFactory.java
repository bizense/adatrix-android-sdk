package com.bizense.adatrix.android.test;

import static org.mockito.Mockito.mock;

import com.bizense.adatrix.android.core.Interstitial;
import com.bizense.adatrix.android.core.InterstitialAdapter;

public class TestInterstitialAdapterFactory{
	protected static TestInterstitialAdapterFactory instance = new TestInterstitialAdapterFactory();
    private InterstitialAdapter mockInterstitalAdapter = mock(InterstitialAdapter.class);
    private Interstitial latestInterstitial;
    private String latestClassName;
    private String latestClassData;

    public static InterstitialAdapter getSingletonMock() {
        return getTestFactory().mockInterstitalAdapter;
    }

    private static TestInterstitialAdapterFactory getTestFactory() {
        return ((TestInterstitialAdapterFactory)instance);
    }

    public static Interstitial getLatestInterstitial() {
        return getTestFactory().latestInterstitial;
    }

    public static String getLatestClassName() {
        return getTestFactory().latestClassName;
    }

    public static String getLatestClassData() {
        return getTestFactory().latestClassData;
    }
    
    public InterstitialAdapter create(Interstitial interstitial, String className, String classData) {
        latestInterstitial = interstitial;
        latestClassName = className;
        latestClassData = classData;
        return mockInterstitalAdapter;
    }
}
