package com.bizense.adatrix.android.utils;

import static android.os.AsyncTask.THREAD_POOL_EXECUTOR;
import static junit.framework.Assert.fail;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;

import android.os.AsyncTask;
import android.os.Build;

import com.bizense.adatrix.android.managers.AsyncTaskManager;
import com.bizense.adatrix.android.test.SdkTestRunner;

@RunWith(SdkTestRunner.class)
public class AsyncTaskManagerTest {

    private AsyncTask<String, ?, ?> asyncTask;

    @Before
    public void setUp() throws Exception {
        asyncTask = spy(new AsyncTask<String, Void, Void>() {
            @Override
            protected Void doInBackground(String... strings) {
                return null;
            }
        });
    };

    @Test
    public void safeExecuteOnExecutor_beforeICS_shouldCallExecuteWithParams() throws Exception {
        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", AndroidVersion.HONEYCOMB_MR2.getApiLevel());

        AsyncTaskManager.safeExecuteOnExecutor(asyncTask, "hello");

        verify(asyncTask).execute(eq("hello"));
    }

    @Test
    public void safeExecutorOnExecutor_beforeICS_withNullParam_shouldCallExecute() throws Exception {
        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", AndroidVersion.HONEYCOMB_MR2.getApiLevel());

        AsyncTaskManager.safeExecuteOnExecutor(asyncTask, (String) null);

        verify(asyncTask).execute(eq((String) null));
    }

    @Test
    public void safeExecutorOnExecutor_beforeICS_withNullAsyncTask_shouldThrowIllegalArgumentException() throws Exception {
        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", AndroidVersion.HONEYCOMB_MR2.getApiLevel());

        try {
            AsyncTaskManager.safeExecuteOnExecutor(null, "hello");
            fail("Should have thrown IllegalArgumentException");
        } catch (IllegalArgumentException exception) {
            // pass
        }
    }

    @Test
    public void safeExecuteOnExecutor_atLeastICS_shouldCallExecuteWithParamsWithExecutor() throws Exception {
        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", AndroidVersion.ICE_CREAM_SANDWICH.getApiLevel());

        AsyncTaskManager.safeExecuteOnExecutor(asyncTask, "goodbye");

        verify(asyncTask).executeOnExecutor(eq(THREAD_POOL_EXECUTOR), eq("goodbye"));
    }

    @Test
    public void safeExecutorOnExecutor_atLeastICS_withNullParam_shouldCallExecuteWithParamsWithExecutor() throws Exception {
        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", AndroidVersion.ICE_CREAM_SANDWICH.getApiLevel());

        AsyncTaskManager.safeExecuteOnExecutor(asyncTask, (String) null);

        verify(asyncTask).executeOnExecutor(eq(THREAD_POOL_EXECUTOR), eq((String) null));

    }


    @Test
    public void safeExecutorOnExecutor_atLeastICS_withNullAsyncTask_shouldThrowIllegalArgumentException() throws Exception {
        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", AndroidVersion.ICE_CREAM_SANDWICH.getApiLevel());

        try {
            AsyncTaskManager.safeExecuteOnExecutor(null, "hello");
            fail("Should have thrown IllegalArgumentException");
        } catch (IllegalArgumentException exception) {
            // pass
        }
    }
}
