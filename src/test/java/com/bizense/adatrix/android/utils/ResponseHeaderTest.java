package com.bizense.adatrix.android.utils;

import static org.fest.assertions.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.bizense.adatrix.android.test.SdkTestRunner;
import com.bizense.adatrix.android.test.TestHttpResponseWithHeaders;

@RunWith(SdkTestRunner.class)
public class ResponseHeaderTest {
    private TestHttpResponseWithHeaders response;

    @Before
    public void setup() {
        response = new TestHttpResponseWithHeaders(200, "all is well");
    }

    @Test
    public void extractBooleanHeader_whenValueIsZero_shouldReturnFalse() throws Exception {
        response.addHeader(ResponseHeader.SCROLLABLE.getKey(), "0");
        assertThat(ResponseHeader.SCROLLABLE.extractBoolean(response, false)).isFalse();

        response.addHeader(ResponseHeader.SCROLLABLE.getKey(), "0");
        assertThat(ResponseHeader.SCROLLABLE.extractBoolean(response, true)).isFalse();
    }

    @Test
    public void extractBooleanHeader_whenValueIsOne_shouldReturnTrue() throws Exception {
        response.addHeader(ResponseHeader.SCROLLABLE.getKey(), "1");
        assertThat(ResponseHeader.SCROLLABLE.extractBoolean(response, false)).isTrue();

        response.addHeader(ResponseHeader.SCROLLABLE.getKey(), "1");
        assertThat(ResponseHeader.SCROLLABLE.extractBoolean(response, true)).isTrue();
    }

    @Test
    public void extractBooleanHeader_shouldReturnDefaultValue() throws Exception {
        // no header added to response

        assertThat(ResponseHeader.SCROLLABLE.extractBoolean(response, false)).isFalse();
        assertThat(ResponseHeader.SCROLLABLE.extractBoolean(response, true)).isTrue();
    }

    @Test
    public void extractIntegerHeader_shouldReturnIntegerValue() throws Exception {
        response.addHeader(ResponseHeader.AD_TIMEOUT.getKey(), "10");
        assertThat(ResponseHeader.AD_TIMEOUT.extractInteger(response)).isEqualTo(10);

        response.addHeader(ResponseHeader.AD_TIMEOUT.getKey(), "0");
        assertThat(ResponseHeader.AD_TIMEOUT.extractInteger(response)).isEqualTo(0);

        response.addHeader(ResponseHeader.AD_TIMEOUT.getKey(), "-2");
        assertThat(ResponseHeader.AD_TIMEOUT.extractInteger(response)).isEqualTo(-2);
    }

    @Test
    public void extractIntegerHeader_withDoubleValue_shouldTruncateValue() throws Exception {
        response.addHeader(ResponseHeader.AD_TIMEOUT.getKey(), "3.14");
        assertThat(ResponseHeader.AD_TIMEOUT.extractInteger(response)).isEqualTo(3);

        response.addHeader(ResponseHeader.AD_TIMEOUT.getKey(), "-3.14");
        assertThat(ResponseHeader.AD_TIMEOUT.extractInteger(response)).isEqualTo(-3);
    }

    @Test
    public void extractIntegerHeader_whenNoHeaderPresent_shouldReturnNull() throws Exception {
        // no header added to response
        assertThat(ResponseHeader.AD_TIMEOUT.extractInteger(response)).isNull();

        response.addHeader(ResponseHeader.AD_TIMEOUT.getKey(), null);
        assertThat(ResponseHeader.AD_TIMEOUT.extractInteger(response)).isNull();
    }

    @Test
    public void extractIntegerHeader_withNonsenseStringValue_shouldReturnNull() throws Exception {
        response.addHeader(ResponseHeader.AD_TIMEOUT.getKey(), "llama!!guy");
        assertThat(ResponseHeader.AD_TIMEOUT.extractInteger(response)).isNull();
    }

    @Test
    public void extractIntHeader_withInvalidHeader_shouldUseDefaultValue() throws Exception {
        response.addHeader(ResponseHeader.AD_TIMEOUT.getKey(), "5");
        assertThat(ResponseHeader.AD_TIMEOUT.extractInteger(response, 10)).isEqualTo(5);

        response.addHeader(ResponseHeader.AD_TIMEOUT.getKey(), "five!");
        assertThat(ResponseHeader.AD_TIMEOUT.extractInteger(response, 10)).isEqualTo(10);
    }
}
