package com.bizense.adatrix.android.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import static org.fest.assertions.api.Assertions.*;
import android.os.Build;

import com.bizense.adatrix.android.test.SdkTestRunner;

@RunWith(SdkTestRunner.class)
public class AndroidVersionTest {
    @Test
    public void currentApiLevel_shouldReflectActualApiLevel() throws Exception {
        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", 4);
        assertThat(AndroidVersion.currentApiLevel()).isEqualTo(AndroidVersion.DONUT);

        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", 8);
        assertThat(AndroidVersion.currentApiLevel()).isEqualTo(AndroidVersion.FROYO);

        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", 14);
        assertThat(AndroidVersion.currentApiLevel()).isEqualTo(AndroidVersion.ICE_CREAM_SANDWICH);
    }

    @Test
    public void currentApiLevel_whenUnknownApiLevel_shouldReturnCurDevelopment() throws Exception {
        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", 900);
        assertThat(AndroidVersion.currentApiLevel()).isEqualTo(AndroidVersion.CUR_DEVELOPMENT);
    }

    @Test
    public void isAtMost_shouldCompareVersions() throws Exception {
        assertThat(AndroidVersion.ICE_CREAM_SANDWICH.isAtMost(AndroidVersion.BASE)).isFalse();
        assertThat(AndroidVersion.ICE_CREAM_SANDWICH.isAtMost(AndroidVersion.JELLY_BEAN)).isTrue();
        assertThat(AndroidVersion.ICE_CREAM_SANDWICH.isAtMost(AndroidVersion.ICE_CREAM_SANDWICH)).isTrue();
    }

    @Test
    public void isAtLeast_shouldCompareVersions() throws Exception {
        assertThat(AndroidVersion.ICE_CREAM_SANDWICH.isAtLeast(AndroidVersion.BASE)).isTrue();
        assertThat(AndroidVersion.ICE_CREAM_SANDWICH.isAtLeast(AndroidVersion.JELLY_BEAN)).isFalse();
        assertThat(AndroidVersion.ICE_CREAM_SANDWICH.isAtLeast(AndroidVersion.ICE_CREAM_SANDWICH)).isTrue();
    }

    @Test
    public void isBelow_shouldCompareVersions() throws Exception {
        assertThat(AndroidVersion.ICE_CREAM_SANDWICH.isBelow(AndroidVersion.BASE)).isFalse();
        assertThat(AndroidVersion.ICE_CREAM_SANDWICH.isBelow(AndroidVersion.JELLY_BEAN)).isTrue();
        assertThat(AndroidVersion.ICE_CREAM_SANDWICH.isBelow(AndroidVersion.ICE_CREAM_SANDWICH)).isFalse();
    }
}
