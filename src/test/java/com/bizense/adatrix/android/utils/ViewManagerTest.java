package com.bizense.adatrix.android.utils;

import static org.fest.assertions.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bizense.adatrix.android.managers.ViewManager;
import com.bizense.adatrix.android.test.SdkTestRunner;

@RunWith(SdkTestRunner.class)
public class ViewManagerTest {
    private Context context;
    private View subject;
    private RelativeLayout parent;

    @Before
    public void setup() {
        context = new Activity();
        subject = new View(context);
        parent = new RelativeLayout(context);
    }

    @Test
    public void removeFromParent_shouldRemoveViewFromParent() throws Exception {
        assertThat(parent.getChildCount()).isEqualTo(0);

        parent.addView(subject);
        assertThat(parent.getChildCount()).isEqualTo(1);
        assertThat(subject.getParent()).isEqualTo(parent);

        ViewManager.removeFromParent(subject);

        assertThat(parent.getChildCount()).isEqualTo(0);
        assertThat(subject.getParent()).isNull();
    }

    @Test
    public void removeFromParent_withMultipleChildren_shouldRemoveCorrectChild() throws Exception {
        parent.addView(new TextView(context));

        assertThat(parent.getChildCount()).isEqualTo(1);

        parent.addView(subject);

        assertThat(parent.getChildCount()).isEqualTo(2);

        ViewManager.removeFromParent(subject);
        assertThat(parent.getChildCount()).isEqualTo(1);

        assertThat(parent.getChildAt(0)).isInstanceOf(TextView.class);
    }

    @Test
    public void removeFromParent_whenViewIsNull_shouldPass() throws Exception {
        ViewManager.removeFromParent(null);

        // pass
    }

    @Test
    public void removeFromParent_whenViewsParentIsNull_shouldPass() throws Exception {
        assertThat(subject.getParent()).isNull();

        ViewManager.removeFromParent(subject);

        // pass
    }
}
