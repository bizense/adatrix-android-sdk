package com.bizense.adatrix.android.utils;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.stub;
import static org.mockito.Mockito.verify;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;

import com.bizense.adatrix.android.managers.HttpClientManager;
import com.bizense.adatrix.android.test.SdkTestRunner;

@RunWith(SdkTestRunner.class)
public class HttpClientManagerTest {

    private HttpClient httpClient;
    private ClientConnectionManager clientConnectionManager;

    @Before
    public void setUp() throws Exception {
        httpClient = mock(HttpClient.class);
        clientConnectionManager = mock(ClientConnectionManager.class);
        stub(httpClient.getConnectionManager()).toReturn(clientConnectionManager);
    }

    @Ignore("pending")
    @Test
    public void safeShutdown_shouldShutdownHttpClient() throws Exception {
        HttpClientManager.safeShutdown(httpClient);

        Robolectric.runBackgroundTasks();

        verify(clientConnectionManager).shutdown();
    }

    @Test
    public void safeShutdown_withNullHttpClient_shouldNotBlowUp() throws Exception {
        HttpClientManager.safeShutdown(null);

        Robolectric.runBackgroundTasks();

        verify(clientConnectionManager, never()).shutdown();
    }

    @Test
    public void safeShutdown_withNullConnectionManager_shouldNotBlowUp() throws Exception {
        stub(httpClient.getConnectionManager()).toReturn(null);
        HttpClientManager.safeShutdown(httpClient);

        Robolectric.runBackgroundTasks();

        verify(clientConnectionManager, never()).shutdown();
    }
    
    @Test
    public void cookieParsing_httpClientManager() throws Exception{
    	DefaultHttpClient client = HttpClientManager.create();
    	HttpGet httpget = new HttpGet("http://adx1.adatrix.com/get.ad?zone=/16/17/41");
    	HttpResponse response = client.execute(httpget);
    	System.out.println(response);
    }
}