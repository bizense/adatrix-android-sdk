package com.bizense.adatrix.android.utils;

import static org.fest.assertions.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.bizense.adatrix.android.managers.StringManager;
import com.bizense.adatrix.android.test.SdkTestRunner;

@RunWith(SdkTestRunner.class)
public class StringManagerTest {
    @Test
    public void isEmpty_shouldReturnValidResponse() throws Exception {
        assertThat(StringManager.isEmpty("")).isTrue();

        assertThat(StringManager.isEmpty("test")).isFalse();

        assertThat(StringManager.isEmpty(null)).isFalse();
    }
}
