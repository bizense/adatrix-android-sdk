package com.bizense.adatrix.android.core;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.app.Activity;

import com.bizense.adatrix.android.test.SdkTestRunner;
import com.bizense.adatrix.android.test.TestBannerAdapterFactory;
import com.bizense.adatrix.android.utils.ErrorCode;
import com.bizense.adatrix.android.utils.ResponseHeader;

@RunWith(SdkTestRunner.class)
public class AdViewTest {
    private AdView subject;
    private Map<String,String> paramsMap = new HashMap<String, String>();
    private BannerAdapter bannerAdapter;
    private AdViewController adViewController;

    @Before
    public void setup() {
        subject = new AdView(new Activity());
        bannerAdapter = TestBannerAdapterFactory.getTestFactory().create(subject, "name", "data");
        subject.setBannerListener(bannerAdapter);
        reset(bannerAdapter);
        adViewController = mock(AdViewController.class);
    }

    @Test
    public void loadCustomEvent_shouldInitializeBannerAdapter() throws Exception {
        paramsMap.put(ResponseHeader.CUSTOM_EVENT_NAME.getKey(), "name");
        paramsMap.put(ResponseHeader.CUSTOM_EVENT_DATA.getKey(), "data");
        paramsMap.put(ResponseHeader.CUSTOM_EVENT_HTML_DATA.getKey(), "html");
        subject.load(paramsMap);

        assertThat(TestBannerAdapterFactory.getLatestAdView()).isEqualTo(subject);
        assertThat(TestBannerAdapterFactory.getLatestClassName()).isEqualTo("name");
        assertThat(TestBannerAdapterFactory.getLatestClassData()).isEqualTo("data");

        verify(bannerAdapter).loadAd();
    }

    @Test
    public void loadCustomEvent_whenParamsMapIsNull_shouldCallLoadFailUrl() throws Exception {
        subject.load(null);

        verify(adViewController).loadFailUrl(eq(ErrorCode.ADAPTER_NOT_FOUND));
        verify(bannerAdapter, never()).invalidate();
        verify(bannerAdapter, never()).loadAd();
    }
}
