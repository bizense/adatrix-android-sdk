package com.bizense.adatrix.android.core;


import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.robolectric.Robolectric.shadowOf;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowWebView;

import android.app.Activity;
import android.os.Build;
import android.view.ViewGroup;
import android.webkit.WebSettings;

import com.bizense.adatrix.android.test.SdkTestRunner;
import com.bizense.adatrix.android.utils.AndroidVersion;

@RunWith(SdkTestRunner.class)
public class AbstractWebViewTest {

    private Activity context;
    private AbstractWebView subject;

    @Before
    public void setup() {
        context = new Activity();
    }

    @Test
    public void beforeFroyo_shouldDisablePluginsByDefault() throws Exception {
        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", AndroidVersion.ECLAIR_MR1.getApiLevel());
        subject = new AbstractWebView(context);

        WebSettings webSettings = subject.getSettings();   
        try {
            Method method = Class.forName("android.webkit.WebSettings").getDeclaredMethod("setPluginsEnabled", boolean.class);
            assertThat(method.invoke(webSettings)).isEqualTo(false);
            
            subject.enablePlugins(true);
            assertThat(method.invoke(webSettings)).isEqualTo(true);
        } catch (Exception e) {
        }
    }

    @Test
    public void froyoAndAfter_shouldDisablePluginsByDefault() throws Exception {
        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", AndroidVersion.FROYO.getApiLevel());
        subject = new AbstractWebView(context);

        WebSettings webSettings = subject.getSettings();
        assertThat(webSettings.getPluginState()).isEqualTo(WebSettings.PluginState.OFF);

        subject.enablePlugins(true);
        assertThat(webSettings.getPluginState()).isEqualTo(WebSettings.PluginState.ON);
    }

    @Test
    public void jellyBeanMr2AndAfter_shouldPass() throws Exception {
        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", AndroidVersion.JELLY_BEAN_MR2.getApiLevel());
        subject = new AbstractWebView(context);

        subject.enablePlugins(true);

        // pass
    }

    @Test
    public void destroy_shouldRemoveSelfFromParent_beforeCallingDestroy() throws Exception {
        subject = new AbstractWebView(context);
        ViewGroup parent = mock(ViewGroup.class);
        ShadowWebView shadow = shadowOf(subject);
        shadow.setMyParent(parent);

        subject.destroy();

        verify(parent).removeView(eq(subject));
        assertThat(shadow.wasDestroyCalled()).isTrue();
    }
}
