package com.bizense.adatrix.android.core;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.app.Activity;

import com.bizense.adatrix.android.core.AdView.LocationAwareness;
import com.bizense.adatrix.android.test.SdkTestRunner;
import com.bizense.adatrix.android.test.TestInterstitialAdapterFactory;
import com.bizense.adatrix.android.utils.ErrorCode;
import com.bizense.adatrix.android.utils.ResponseHeader;

@RunWith(SdkTestRunner.class)
public class InterstitialTest {

    private static final String KEYWORDS_VALUE = "expected_keywords";
    private static final String ZONE_VALUE = "expected_zoneid";
    private static final String SOURCE_VALUE = "expected_source";
    private static final String CLICKTHROUGH_URL_VALUE = "expected_clickthrough_url";
    private Activity activity;
    private Interstitial subject;
    private Map<String, String> paramsMap;
    private InterstitialAdapter interstitialAdapter;
    private InterstitialListener interstitialListener;
    private Interstitial.InterstitialView interstitialView;
    private AdViewController adViewController;

    @Before
    public void setUp() throws Exception {
        activity = new Activity();
        subject = new Interstitial(activity, ZONE_VALUE);
        interstitialListener = mock(InterstitialListener.class);
        subject.setInterstitialAdListener(interstitialListener);

        interstitialView = mock(Interstitial.InterstitialView.class);

        paramsMap = new HashMap<String, String>();
        paramsMap.put(ResponseHeader.CUSTOM_EVENT_NAME.getKey(), "class name");
        paramsMap.put(ResponseHeader.CUSTOM_EVENT_DATA.getKey(), "class data");

        interstitialAdapter = TestInterstitialAdapterFactory.getSingletonMock();
        reset(interstitialAdapter);
        adViewController = mock(AdViewController.class);
    }

    @Test
    public void forceRefresh_shouldResetInterstitialViewAndMarkNotDestroyed() throws Exception {
        subject.setInterstitialView(interstitialView);
        subject.onInterstitialLoaded();
        subject.forceRefresh();

        assertThat(subject.isReady()).isFalse();
        assertThat(subject.isDestroyed()).isFalse();
        verify(interstitialView).forceRefresh();
    }

    @Test
    public void setKeywordsTest() throws Exception {
        subject.setInterstitialView(interstitialView);
        String keywords = "these_are_keywords";

        subject.setKeywords(keywords);
        verify(interstitialView).setKeywords(eq(keywords));
    }
    @Test
    public void getKeywordsTest() throws Exception {
        subject.setInterstitialView(interstitialView);

        subject.getKeywords();
        verify(interstitialView).getKeywords();
    }

    @Test
    public void getInterstitialAdListenerTest() throws Exception {
        interstitialListener = mock(InterstitialListener.class);
        subject.setInterstitialAdListener(interstitialListener);
        assertThat(subject.getInterstitialAdListener()).isSameAs(interstitialListener);
    }

    @Test
    public void setLocationAwarenessTest() throws Exception {
        subject.setInterstitialView(interstitialView);
        subject.setLocationAwareness(LocationAwareness.LOCATION_AWARENESS_NORMAL);
        verify(interstitialView).setLocationAwareness(eq(LocationAwareness.LOCATION_AWARENESS_NORMAL));
    }

    @Test
    public void getLocationAwarenessTest() throws Exception {
        subject.setInterstitialView(interstitialView);
        subject.getLocationAwareness();
        verify(interstitialView).getLocationAwareness();
    }

    @Test
    public void setLocationPrecisionTest() throws Exception {
        subject.setInterstitialView(interstitialView);
        subject.setLocationPrecision(10);
        verify(interstitialView).setLocationPrecision(eq(10));
    }

    @Test
    public void getLocationPrecisionTest() throws Exception {
        subject.setInterstitialView(interstitialView);
        subject.getLocationPrecision();
        verify(interstitialView).getLocationPrecision();
    }


    @Test
    public void setTestingTest() throws Exception {
        subject.setInterstitialView(interstitialView);
        subject.setTesting(true);
        verify(interstitialView).setTesting(eq(true));
    }

    @Test
    public void getTestingTest() throws Exception {
        subject.setInterstitialView(interstitialView);
        subject.getTesting();
        verify(interstitialView).getTesting();
    }

    @Test
    public void setLocalExtrasTest() throws Exception {
        subject.setInterstitialView(interstitialView);

        Map<String,Object> localExtras = new HashMap<String, Object>();
        localExtras.put("guy", new Activity());
        localExtras.put("other guy", new BigDecimal(27f));

        subject.setLocalExtras(localExtras);
        verify(interstitialView).setLocalExtras(eq(localExtras));
    }

    @Test
    public void load_shouldCreateAndLoadInterstitialAdapter() throws Exception {
        Interstitial.InterstitialView interstitialView = subject.new InterstitialView(activity);
        interstitialView.load(paramsMap);

        assertThat(TestInterstitialAdapterFactory.getLatestInterstitial()).isSameAs(subject);
        assertThat(TestInterstitialAdapterFactory.getLatestClassName()).isEqualTo("class name");
        assertThat(TestInterstitialAdapterFactory.getLatestClassData()).isEqualTo("class data");
    }

    @Test
    public void onInterstitialLoaded_shouldNotifyListener() throws Exception {
        subject.setInterstitialView(interstitialView);

        subject.onInterstitialLoaded();
        verify(interstitialListener).onInterstitialLoaded();

        verify(interstitialView, never()).trackImpression();
    }

    @Test
    public void onInterstitialLoaded_whenInterstitialAdListenerIsNull_shouldNotNotifyListenerOrTrackImpression() throws Exception {
        subject.setInterstitialView(interstitialView);
        subject.setInterstitialAdListener(null);

        subject.onInterstitialLoaded();

        verify(interstitialView, never()).trackImpression();
        verify(interstitialListener, never()).onInterstitialLoaded();
    }

    @Test
    public void onInterstitialFailed_shouldLoadFailUrl() throws Exception {
        subject.setInterstitialView(interstitialView);

        subject.onInterstitialFailed(ErrorCode.INTERNAL_ERROR);

        verify(interstitialView).loadFailUrl(ErrorCode.INTERNAL_ERROR);
    }

    @Test
    public void onInterstitialShown_shouldTrackImpressionAndNotifyListener() throws Exception {
        subject.setInterstitialView(interstitialView);
        subject.onInterstitialShown();

        verify(interstitialView).trackImpression();
        verify(interstitialListener).onInterstitialShown();
    }

    @Test
    public void onInterstitialShown_whenInterstitialAdListenerIsNull_shouldNotNotifyListener() throws Exception {
        subject.setInterstitialAdListener(null);
        subject.onInterstitialShown();
        verify(interstitialListener, never()).onInterstitialShown();
    }

    @Test
    public void onInterstitialClicked_shouldRegisterClickAndNotifyListener() throws Exception {
        subject.setInterstitialView(interstitialView);

        subject.onInterstitialClicked();

        verify(interstitialView).registerClick();
        verify(interstitialListener).onInterstitialClicked();
    }

    @Test
    public void onInterstitialClicked_whenInterstitialAdListenerIsNull_shouldNotNotifyListener() throws Exception {
        subject.setInterstitialAdListener(null);

        subject.onInterstitialClicked();

        verify(interstitialListener, never()).onInterstitialClicked();
    }

    @Test
    public void onInterstitialDismissed_shouldNotifyListener() throws Exception {
        subject.onInterstitialDismissed();

        verify(interstitialListener).onInterstitialDismissed();
    }

    @Test
    public void onInterstitialDismissed_whenInterstitialAdListenerIsNull_shouldNotNotifyListener() throws Exception {
        subject.setInterstitialAdListener(null);
        subject.onInterstitialDismissed();
        verify(interstitialListener, never()).onInterstitialDismissed();
    }

    @Test
    public void destroy_shouldPreventOnInterstitialLoadedNotification() throws Exception {
        subject.destroy();

        subject.onInterstitialLoaded();

        verify(interstitialListener, never()).onInterstitialLoaded();
    }

    @Test
    public void destroy_shouldPreventOnInterstitialFailedNotification() throws Exception {
        subject.setInterstitialView(interstitialView);
        subject.destroy();

        subject.onInterstitialFailed(ErrorCode.UNSPECIFIED);

        verify(interstitialView, never()).loadFailUrl(ErrorCode.UNSPECIFIED);
    }

    @Test
    public void destroy_shouldPreventOnInterstitialClickedFromRegisteringClick() throws Exception {
        subject.setInterstitialView(interstitialView);
        subject.destroy();

        subject.onInterstitialClicked();

        verify(interstitialView, never()).registerClick();
    }

    @Test
    public void destroy_shouldPreventOnShownNotification() throws Exception {
        subject.destroy();

        subject.onInterstitialShown();

        verify(interstitialListener, never()).onInterstitialShown();
    }

    @Test
    public void destroy_shouldPreventOnInterstitialDismissedNotification() throws Exception {
        subject.destroy();

        subject.onInterstitialDismissed();

        verify(interstitialListener, never()).onInterstitialDismissed();
    }

    @Test
    public void newlyCreated_shouldNotBeReadyAndNotShow() throws Exception {
        assertShowsInterstitial(false);
    }

    @Test
    public void loadingInterstitial_shouldBecomeReadyToShowAd() throws Exception {
        subject.onInterstitialLoaded();

        assertShowsInterstitial(true);
    }

    @Test
    public void dismissingHtmlInterstitial_shouldNotBecomeReadyToShowHtmlAd() throws Exception {
//        EventForwardingBroadcastReceiver broadcastReceiver = new EventForwardingBroadcastReceiver(subject.mInterstitialAdListener);
//
//        subject.onInterstitialLoaded();
//        broadcastReceiver.onHtmlInterstitialDismissed();
//
//        assertShowsInterstitial(false);
    }

    @Test
    public void failingInterstitial_shouldNotBecomeReadyToShowAd() throws Exception {
        subject.onInterstitialLoaded();
        subject.onInterstitialFailed(ErrorCode.CANCELLED);

        assertShowsInterstitial(false);
    }

    @Test
    public void dismissingInterstitial_shouldNotBecomeReadyToShowAd() throws Exception {
        subject.onInterstitialLoaded();
        subject.onInterstitialDismissed();

        assertShowsInterstitial(false);
    }

    @Test
    public void load_shouldInitializeBannerAdapter() throws Exception {
        Interstitial.InterstitialView interstitialView = subject.new InterstitialView(activity);

        paramsMap.put(ResponseHeader.CUSTOM_EVENT_NAME.getKey(), "name");
        paramsMap.put(ResponseHeader.CUSTOM_EVENT_DATA.getKey(), "data");
        paramsMap.put(ResponseHeader.CUSTOM_EVENT_HTML_DATA.getKey(), "html");
        interstitialView.load(paramsMap);

        assertThat(TestInterstitialAdapterFactory.getLatestInterstitial()).isEqualTo(subject);
        assertThat(TestInterstitialAdapterFactory.getLatestClassName()).isEqualTo("name");
        assertThat(TestInterstitialAdapterFactory.getLatestClassData()).isEqualTo("data");

        verify(interstitialAdapter).setAdapterListener(eq(subject));
        verify(interstitialAdapter).loadInterstitial();
    }

    @Test
    public void load_whenParamsMapIsNull_shouldCallLoadFailUrl() throws Exception {
        Interstitial.InterstitialView interstitialView = subject.new InterstitialView(activity);

        interstitialView.load(null);

        verify(adViewController).loadFailUrl(eq(ErrorCode.ADAPTER_NOT_FOUND));
        verify(interstitialAdapter, never()).invalidate();
        verify(interstitialAdapter, never()).loadInterstitial();
    }

    @Test
    public void adFailed_shouldNotifyInterstitialAdListener() throws Exception {
        Interstitial.InterstitialView interstitialView = subject.new InterstitialView(activity);
        interstitialView.adFailed(ErrorCode.CANCELLED);

        verify(interstitialListener).onInterstitialFailed(eq(ErrorCode.CANCELLED));
    }

    private void load() {
        Interstitial.InterstitialView interstitialView = subject.new InterstitialView(activity);

        paramsMap.put(ResponseHeader.CUSTOM_EVENT_NAME.getKey(), "name");
        paramsMap.put(ResponseHeader.CUSTOM_EVENT_DATA.getKey(), "data");
        paramsMap.put(ResponseHeader.CUSTOM_EVENT_HTML_DATA.getKey(), "html");
        interstitialView.load(paramsMap);
    }

    private void assertShowsInterstitial(boolean shouldBeReady) {
        Interstitial.InterstitialView interstitialView = subject.new InterstitialView(activity);
        interstitialView.load(paramsMap);

        assertThat(subject.isReady()).isEqualTo(shouldBeReady);
        assertThat(subject.show()).isEqualTo(shouldBeReady);

        if (shouldBeReady) {
            verify(interstitialAdapter).showInterstitial();
        } else {
            verify(interstitialAdapter, never()).showInterstitial();
        }
    }
}
