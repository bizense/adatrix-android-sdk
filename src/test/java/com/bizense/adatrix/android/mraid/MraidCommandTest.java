package com.bizense.adatrix.android.mraid;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.util.Map;

import org.junit.Test;

import com.bizense.adatrix.android.mraid.MraidCommandFactory.MraidJavascriptCommand;
import com.bizense.adatrix.android.mraid.MraidView.PlacementType;

public class MraidCommandTest {
    @Test
    public void createCommand_shouldReturnTheRightKindOfCommand() throws Exception {
        assertThat(MraidCommandFactory.create("bogus", null, null)).isNull();

        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.CLOSE.getCommand(), null, null)).isInstanceOf(MraidCommandClose.class);
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.CLOSE.getCommand(), null, null)).isNotSameAs(MraidCommandFactory.create("close", null, null));

        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.EXPAND.getCommand(), null, null)).isInstanceOf(MraidCommandExpand.class);
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.USECUSTOMCLOSE.getCommand(), null, null)).isInstanceOf(MraidCommandUseCustomClose.class);
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.OPEN.getCommand(), null, null)).isInstanceOf(MraidCommandOpen.class);
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.RESIZE.getCommand(), null, null)).isInstanceOf(MraidCommandResize.class);
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.SET_RESIZE_PROPERTIES.getCommand(), null, null)).isInstanceOf(MraidCommandSetResizeProperties.class);
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.GET_RESIZE_PROPERTIES.getCommand(), null, null)).isInstanceOf(MraidCommandGetResizeProperties.class);
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.PLAY_VIDEO.getCommand(), null, null)).isInstanceOf(MraidCommandPlayVideo.class);
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.STORE_PICTURE.getCommand(), null, null)).isInstanceOf(MraidCommandStorePicture.class);
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.GET_CURRENT_POSITION.getCommand(), null, null)).isInstanceOf(MraidCommandGetCurrentPosition.class);
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.GET_DEFAULT_POSITION.getCommand(), null, null)).isInstanceOf(MraidCommandGetDefaultPosition.class);
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.GET_MAX_SIZE.getCommand(), null, null)).isInstanceOf(MraidCommandGetMaxSize.class);
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.GET_SCREEN_SIZE.getCommand(), null, null)).isInstanceOf(MraidCommandGetScreenSize.class);
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.CREATE_CALENDAR_EVENT.getCommand(), null, null)).isInstanceOf(MraidCommandCreateCalendarEvent.class);
    }

    @Test
    public void createCommand_shouldPassParameters() throws Exception {
        MraidView expectedView = mock(MraidView.class);
        Map<String, String> expectedMap = mock(Map.class);

        MraidCommand command = MraidCommandFactory.create(MraidJavascriptCommand.EXPAND.getCommand(), expectedMap, expectedView);
        assertThat(command.mParams).isEqualTo(expectedMap);
        assertThat(command.mView).isEqualTo(expectedView);
    }

    @Test
    public void createCommand_close_shouldSetDependentOnUserClick() throws Exception {
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.CLOSE.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INLINE)).isFalse();
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.CLOSE.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INTERSTITIAL)).isFalse();
    }

    @Test
    public void createCommand_expand_shouldSetDependentOnUserClick() throws Exception {
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.EXPAND.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INLINE)).isTrue();
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.EXPAND.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INTERSTITIAL)).isFalse();
    }

    @Test
    public void createCommand_useCustomClose_shouldSetDependentOnUserClick() throws Exception {
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.USECUSTOMCLOSE.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INLINE)).isFalse();
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.USECUSTOMCLOSE.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INTERSTITIAL)).isFalse();
    }

    @Test
    public void createCommand_open_shouldSetDependentOnUserClick() throws Exception {
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.OPEN.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INLINE)).isTrue();
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.OPEN.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INTERSTITIAL)).isTrue();
    }

    @Test
    public void createCommand_resize_shouldSetDependentOnUserClick() throws Exception {
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.RESIZE.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INLINE)).isFalse();
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.RESIZE.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INTERSTITIAL)).isFalse();
    }

    @Test
    public void createCommand_setResizeProperties_shouldSetDependentOnUserClick() throws Exception {
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.SET_RESIZE_PROPERTIES.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INLINE)).isFalse();
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.SET_RESIZE_PROPERTIES.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INTERSTITIAL)).isFalse();
    }

    @Test
    public void createCommand_getResizeProperties_shouldSetDependentOnUserClick() throws Exception {
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.GET_RESIZE_PROPERTIES.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INLINE)).isFalse();
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.GET_RESIZE_PROPERTIES.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INTERSTITIAL)).isFalse();
    }

    @Test
    public void createCommand_playVideo_shouldSetDependentOnUserClick() throws Exception {
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.PLAY_VIDEO.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INLINE)).isTrue();
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.PLAY_VIDEO.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INTERSTITIAL)).isFalse();
    }

    @Test
    public void createCommand_storePicture_shouldSetDependentOnUserClick() throws Exception {
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.STORE_PICTURE.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INLINE)).isTrue();
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.STORE_PICTURE.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INTERSTITIAL)).isTrue();
    }

    @Test
    public void createCommand_getCurrentPosition_shouldSetDependentOnUserClick() throws Exception {
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.GET_CURRENT_POSITION.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INLINE)).isFalse();
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.GET_CURRENT_POSITION.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INTERSTITIAL)).isFalse();
    }

    @Test
    public void createCommand_getDefaultPosition_shouldSetDependentOnUserClick() throws Exception {
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.GET_DEFAULT_POSITION.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INLINE)).isFalse();
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.GET_DEFAULT_POSITION.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INTERSTITIAL)).isFalse();
    }

    @Test
    public void createCommand_getMaxSize_shouldSetDependentOnUserClick() throws Exception {
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.GET_MAX_SIZE.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INLINE)).isFalse();
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.GET_MAX_SIZE.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INTERSTITIAL)).isFalse();
    }

    @Test
    public void createCommand_getScreenSize_shouldSetDependentOnUserClick() throws Exception {
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.GET_SCREEN_SIZE.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INLINE)).isFalse();
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.GET_SCREEN_SIZE.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INTERSTITIAL)).isFalse();
    }

    @Test
    public void createCommand_createCalendarEvent_shouldSetDependentOnUserClick() throws Exception {
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.CREATE_CALENDAR_EVENT.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INLINE)).isTrue();
        assertThat(MraidCommandFactory.create(MraidJavascriptCommand.CREATE_CALENDAR_EVENT.getCommand(), null, null).isCommandDependentOnUserClick(PlacementType.INTERSTITIAL)).isTrue();
    }
}
