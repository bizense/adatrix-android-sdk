package com.bizense.adatrix.android.mraid;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.app.Activity;

import com.bizense.adatrix.android.test.SdkTestRunner;

@RunWith(SdkTestRunner.class)
public class MraidCommandPlayVideoTest {
    public static final String EXPECTED_URI = "http://expected.uri";

    private MraidCommandPlayVideo subject;
    private MraidView mraidView;

    @Before
    public void setup() {
        Map<String, String> params = new HashMap<String, String>();

        params.put(MraidCommandPlayVideo.URI_KEY, EXPECTED_URI);

        mraidView = mock(MraidView.class);
        stub(mraidView.getContext()).toReturn(new Activity());
        MraidDisplayController displayController = new MraidDisplayController(mraidView, null, null);
        stub(mraidView.getDisplayController()).toReturn(displayController);
        subject = new MraidCommandPlayVideo(params, mraidView);
    }

    @Test
    public void execute_shouldPlayVideo() throws Exception {
        subject.execute();

        MraidVideoPlayerActivityTest.assertMraidVideoPlayerActivityStarted("com.bizense.adatrix.android.mraid.MraidVideoPlayerActivity", EXPECTED_URI);
    }
}
