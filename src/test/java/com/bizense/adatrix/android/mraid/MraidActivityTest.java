package com.bizense.adatrix.android.mraid;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.robolectric.Robolectric.shadowOf;

import org.fest.assertions.api.ANDROID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowLocalBroadcastManager;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.activities.AbstractInterstitialActivity;
import com.bizense.adatrix.android.activities.AbstractInterstitialActivityTest;
import com.bizense.adatrix.android.core.InterstitialListener;
import com.bizense.adatrix.android.mraid.MraidView.MraidListener;
import com.bizense.adatrix.android.test.SdkTestRunner;
import com.bizense.adatrix.android.utils.ErrorCode;

@RunWith(SdkTestRunner.class)
public class MraidActivityTest extends AbstractInterstitialActivityTest {

    private MraidView mraidView;
    private InterstitialListener interstitialListener;

    @Before
    public void setUp() throws Exception {
        super.setup();
        Intent mraidActivityIntent = createMraidActivityIntent(EXPECTED_SOURCE);
        mraidView = mock(MraidView.class);
        resetMockedView(mraidView);
        subject = Robolectric.buildActivity(MraidActivity.class).withIntent(mraidActivityIntent).create().get();
        resetMockedView(mraidView);
        interstitialListener = mock(InterstitialListener.class);
    }

    @Test
    public void preRenderHtml_shouldDisablePluginsSetListenersAndLoadHtml() throws Exception {
        MraidActivity.preRenderHtml(null, interstitialListener, "3:27");

        verify(mraidView).enablePlugins(eq(false));
        verify(mraidView).setMraidListener(any(MraidListener.class));
        verify(mraidView).setWebViewClient(any(WebViewClient.class));
        verify(mraidView).loadHtmlData(eq("3:27"));
    }

    @Test
    public void preRenderHtml_shouldCallCustomEventInterstitialOnInterstitialLoaded_whenMraidListenerOnReady() throws Exception {
        MraidActivity.preRenderHtml(null, interstitialListener, "");

        ArgumentCaptor<MraidListener> mraidListenerArgumentCaptorr = ArgumentCaptor.forClass(MraidListener.class);
        verify(mraidView).setMraidListener(mraidListenerArgumentCaptorr.capture());
        MraidListener mraidListener = mraidListenerArgumentCaptorr.getValue();

        mraidListener.onReady(null);

        verify(interstitialListener).onInterstitialLoaded();
    }

    @Test
    public void preRenderHtml_shouldCallCustomEventInterstitialOnInterstitialFailed_whenMraidListenerOnFailure() throws Exception {
        MraidActivity.preRenderHtml(null, interstitialListener, "");

        ArgumentCaptor<MraidListener> mraidListenerArgumentCaptorr = ArgumentCaptor.forClass(MraidListener.class);
        verify(mraidView).setMraidListener(mraidListenerArgumentCaptorr.capture());
        MraidListener mraidListener = mraidListenerArgumentCaptorr.getValue();

        mraidListener.onFailure(null);

        verify(interstitialListener).onInterstitialFailed(null);
    }

    @Test
    public void preRenderHtml_whenWebViewClientShouldOverrideUrlLoading_shouldReturnTrue() throws Exception {
        MraidActivity.preRenderHtml(null, interstitialListener, "");

        ArgumentCaptor<WebViewClient> webViewClientArgumentCaptor = ArgumentCaptor.forClass(WebViewClient.class);
        verify(mraidView).setWebViewClient(webViewClientArgumentCaptor.capture());
        WebViewClient webViewClient = webViewClientArgumentCaptor.getValue();

        boolean consumeUrlLoading = webViewClient.shouldOverrideUrlLoading(null, null);

        assertThat(consumeUrlLoading).isTrue();
        verify(interstitialListener, never()).onInterstitialLoaded();
        verify(interstitialListener, never()).onInterstitialFailed(any(ErrorCode.class));
    }

    @Test
    public void preRenderHtml_shouldCallCustomEventInterstitialOnInterstitialLoaded_whenWebViewClientOnPageFinished() throws Exception {
        MraidActivity.preRenderHtml(null, interstitialListener, "");

        ArgumentCaptor<WebViewClient> webViewClientArgumentCaptor = ArgumentCaptor.forClass(WebViewClient.class);
        verify(mraidView).setWebViewClient(webViewClientArgumentCaptor.capture());
        WebViewClient webViewClient = webViewClientArgumentCaptor.getValue();

        webViewClient.onPageFinished(null, null);

        verify(interstitialListener).onInterstitialLoaded();
    }

    @Test
    public void onCreate_shouldSetupAnMraidView() throws Exception {
        subject.onCreate(null);

        assertThat(getContentView(subject).getChildAt(0)).isSameAs(mraidView);
        verify(mraidView).setMraidListener(any(MraidListener.class));
        verify(mraidView).setOnCloseButtonStateChange(any(MraidView.OnCloseButtonStateChangeListener.class));

        verify(mraidView).loadHtmlData(EXPECTED_SOURCE);
    }

    @Test
    public void onCreate_whenICS_shouldSetHardwareAcceleratedFlag() throws Exception {
        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", 14);

        subject.onCreate(null);

        boolean hardwareAccelerated = shadowOf(subject.getWindow()).getFlag(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        assertThat(hardwareAccelerated).isTrue();
    }

    @Test
    public void onCreate_whenPreICS_shouldNotSetHardwareAcceleratedFlag() throws Exception {
        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", 13);

        subject.onCreate(null);

        boolean hardwareAccelerated = shadowOf(subject.getWindow()).getFlag(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        assertThat(hardwareAccelerated).isFalse();
    }

    @Test
    public void onDestroy_DestroyMraidView() throws Exception {
        Intent expectedIntent = new Intent(AbstractInterstitialActivity.ACTION_INTERSTITIAL_DISMISS);
        ShadowLocalBroadcastManager.getInstance(subject).registerReceiver(broadcastReceiver, AbstractInterstitialActivity.HTML_INTERSTITIAL_INTENT_FILTER);

        subject.onCreate(null);
        subject.onDestroy();

        verify(broadcastReceiver).onReceive(eq(subject), eq(expectedIntent));
        verify(mraidView).destroy();
        assertThat(getContentView(subject).getChildCount()).isEqualTo(0);
    }

    @Test
    public void getAdView_shouldSetupOnReadyListener() throws Exception {
        subject.onCreate(null);
        resetMockedView(mraidView);
        ArgumentCaptor<MraidListener> captor = ArgumentCaptor.forClass(MraidListener.class);
        View actualAdView = subject.getAdView();

        assertThat(actualAdView).isSameAs(mraidView);
        verify(mraidView).setMraidListener(captor.capture());

        subject.hideInterstitialCloseButton();
        captor.getValue().onReady(null);
        ImageButton closeButton = (ImageButton) getContentView(subject).getChildAt(1);
        assertThat(closeButton).isNotNull();
    }

    @Test
    public void baseMraidListenerOnReady_shouldFireJavascriptWebViewDidAppear() throws Exception {
        subject.onCreate(null);
        resetMockedView(mraidView);
        ArgumentCaptor<MraidListener> captor = ArgumentCaptor.forClass(MraidListener.class);
        View actualAdView = subject.getAdView();

        assertThat(actualAdView).isSameAs(mraidView);
        verify(mraidView).setMraidListener(captor.capture());

        MraidListener baseMraidListener = captor.getValue();
        baseMraidListener.onReady(null);

        verify(mraidView).loadUrl(eq("javascript:webviewDidAppear();"));
    }

    @Test
    public void baseMraidListenerOnClose_shouldFireJavascriptWebViewDidClose() throws Exception {
        subject.onCreate(null);
        resetMockedView(mraidView);
        ArgumentCaptor<MraidListener> captor = ArgumentCaptor.forClass(MraidListener.class);
        View actualAdView = subject.getAdView();

        assertThat(actualAdView).isSameAs(mraidView);
        verify(mraidView).setMraidListener(captor.capture());

        MraidListener baseMraidListener = captor.getValue();
        baseMraidListener.onClose(null, null);

        verify(mraidView).loadUrl(eq("javascript:webviewDidClose();"));
    }

    @Test
    public void getAdView_shouldSetupOnCloseButtonStateChangeListener() throws Exception {
        subject.onCreate(null);
        resetMockedView(mraidView);
        ArgumentCaptor<MraidView.OnCloseButtonStateChangeListener> captor = ArgumentCaptor.forClass(MraidView.OnCloseButtonStateChangeListener.class);
        View actualAdView = subject.getAdView();

        assertThat(actualAdView).isSameAs(mraidView);
        verify(mraidView).setOnCloseButtonStateChange(captor.capture());
        MraidView.OnCloseButtonStateChangeListener listener = captor.getValue();

        ANDROID.assertThat(getCloseButton()).isVisible();

        listener.onCloseButtonStateChange(null, false);
        ANDROID.assertThat(getCloseButton()).isNotVisible();

        listener.onCloseButtonStateChange(null, true);
        ANDROID.assertThat(getCloseButton()).isVisible();
    }

    @Test
    public void getAdView_shouldSetupOnCloseListener() throws Exception {
        subject.onCreate(null);
        resetMockedView(mraidView);
        ArgumentCaptor<MraidListener> captor = ArgumentCaptor.forClass(MraidListener.class);
        View actualAdView = subject.getAdView();

        assertThat(actualAdView).isSameAs(mraidView);
        verify(mraidView).setMraidListener(captor.capture());

        captor.getValue().onClose(null, null);

        ANDROID.assertThat(subject).isFinishing();
    }

    @Test
    public void onPause_shouldOnPauseMraidView() throws Exception {
        subject.onCreate(null);
        ((MraidActivity)subject).onPause();

        verify(mraidView).onPause();
    }

    @Test
    public void onResume_shouldResumeMraidView() throws Exception {
        subject.onCreate(null);
        ((MraidActivity)subject).onPause();
        ((MraidActivity)subject).onResume();

        verify(mraidView).onResume();
    }

    private Intent createMraidActivityIntent(String expectedSource) {
        Intent mraidActivityIntent = new Intent();
        mraidActivityIntent.setComponent(new ComponentName("", ""));
        mraidActivityIntent.putExtra(AdxConstants.HTML_RESPONSE_BODY_KEY, expectedSource);
        return mraidActivityIntent;
    }
}
