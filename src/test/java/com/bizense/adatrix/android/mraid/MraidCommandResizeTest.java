package com.bizense.adatrix.android.mraid;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.bizense.adatrix.android.mraid.MraidCommandFactory.MraidJavascriptCommand;
import com.bizense.adatrix.android.test.SdkTestRunner;

@RunWith(SdkTestRunner.class)
public class MraidCommandResizeTest {
    private MraidCommandResize subjectResize;
    private MraidCommandGetResizeProperties subjectGetResizeProperties;
    private MraidCommandSetResizeProperties subjectSetResizeProperties;
    private MraidView mraidView;
    @Before
    public void setup() {
        mraidView = mock(MraidView.class);
        subjectResize = new MraidCommandResize(new HashMap<String, String>(), mraidView);
        subjectGetResizeProperties = new MraidCommandGetResizeProperties(new HashMap<String, String>(), mraidView);
        subjectSetResizeProperties = new MraidCommandSetResizeProperties(new HashMap<String, String>(), mraidView);
    }

    @Test
    public void mraidCommandResizeExecute_shouldFireErrorEvent() throws Exception {
        reset(mraidView);
        subjectResize.execute();
        verify(mraidView).fireErrorEvent(eq(MraidJavascriptCommand.RESIZE), any(String.class));
    }

    @Test
    public void mraidCommandSetResizePropertiesExecute_shouldFireErrorEvent() throws Exception {
        reset(mraidView);
        subjectSetResizeProperties.execute();
        verify(mraidView).fireErrorEvent(eq(MraidJavascriptCommand.SET_RESIZE_PROPERTIES), any(String.class));
    }

    @Test
    public void mraidCommandGetResizePropertiesExecute_shouldFireErrorEvent() throws Exception {
        reset(mraidView);
        subjectGetResizeProperties.execute();
        verify(mraidView).fireErrorEvent(eq(MraidJavascriptCommand.GET_RESIZE_PROPERTIES), any(String.class));
    }
}
