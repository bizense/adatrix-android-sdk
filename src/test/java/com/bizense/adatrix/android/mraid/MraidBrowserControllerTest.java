package com.bizense.adatrix.android.mraid;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import com.bizense.adatrix.android.test.SdkTestRunner;

@RunWith(SdkTestRunner.class)
public class MraidBrowserControllerTest {
    private MraidBrowserController subject;
    private MraidView view;
    private Context context;
    private MraidView.OnOpenListener onOpenListener;

    @Before
    public void setUp() throws Exception {
        context = new Activity();
        view = new MraidView(context, null);
        onOpenListener = mock(MraidView.OnOpenListener.class);
        view.setOnOpenListener(onOpenListener);

        subject = new MraidBrowserController(view);
    }

    @Test
    public void open_withApplicationUrl_shouldStartNewIntent() throws Exception {
        String applicationUrl = "amzn://blah";
        Robolectric.packageManager.addResolveInfoForIntent(new Intent(Intent.ACTION_VIEW, Uri.parse(applicationUrl)), new ResolveInfo());

        subject.open(applicationUrl);

        Intent startedIntent = Robolectric.getShadowApplication().getNextStartedActivity();
        assertThat(startedIntent).isNotNull();
        assertThat(startedIntent.getFlags() & Intent.FLAG_ACTIVITY_NEW_TASK).isNotEqualTo(0);
        assertThat(startedIntent.getComponent()).isNull();
    }

    @Test
    public void open_withHttpApplicationUrl_shouldStartMraidBrowser() throws Exception {
        String applicationUrl = "http://blah";

        subject.open(applicationUrl);

        Intent startedIntent = Robolectric.getShadowApplication().getNextStartedActivity();
        assertThat(startedIntent).isNotNull();
        assertThat(startedIntent.getFlags() & Intent.FLAG_ACTIVITY_NEW_TASK).isNotEqualTo(0);
        assertThat(startedIntent.getComponent().getClassName()).isEqualTo("com.bizense.adatrix.android.mraid.MraidBrowser");
    }

    @Test
    public void open_withApplicationUrlThatCantBeHandled_shouldDefaultToMraidBrowser() throws Exception {
        String applicationUrl = "canthandleme://blah";

        subject.open(applicationUrl);

        Intent startedIntent = Robolectric.getShadowApplication().getNextStartedActivity();
        assertThat(startedIntent).isNotNull();
        assertThat(startedIntent.getFlags() & Intent.FLAG_ACTIVITY_NEW_TASK).isNotEqualTo(0);
        assertThat(startedIntent.getComponent().getClassName()).isEqualTo("com.bizense.adatrix.android.mraid.MraidBrowser");
        assertThat(startedIntent.getStringExtra("extra_url")).isEqualTo(applicationUrl);
    }

    @Test
    public void open_withHttpApplicationUrl_shouldCallMraidListenerOnOpenCallback() throws Exception {
        String applicationUrl = "http://blah";
        Robolectric.packageManager.addResolveInfoForIntent(new Intent(Intent.ACTION_VIEW, Uri.parse(applicationUrl)), new ResolveInfo());

        subject.open(applicationUrl);

        verify(onOpenListener).onOpen(eq(view));
    }

    @Test
    public void open_withApplicationUrl_shouldCallMraidListenerOnOpenCallback() throws Exception {
        String applicationUrl = "app://blah";

        subject.open(applicationUrl);

        verify(onOpenListener).onOpen(eq(view));
    }
}
