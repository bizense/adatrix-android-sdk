package com.bizense.adatrix.android.mraid;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.robolectric.Robolectric.shadowOf_;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowLocalBroadcastManager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.activities.AbstractInterstitialActivity;
import com.bizense.adatrix.android.core.InterstitialListener;
import com.bizense.adatrix.android.html.ResponseBodyInterstitialTest;
import com.bizense.adatrix.android.test.SdkTestRunner;
import com.bizense.adatrix.android.utils.ErrorCode;

@RunWith(SdkTestRunner.class)
public class MraidInterstitialTest extends ResponseBodyInterstitialTest {
    private InterstitialListener interstitialListener;
    private Map<String,Object> localExtras;
    private Map<String,String> serverExtras;
    private Context context;
    private static final String INPUT_HTML_DATA = "%3Chtml%3E%3C%2Fhtml%3E";
    private static final String EXPECTED_HTML_DATA = "<html></html>";

    @Before
    public void setUp() throws Exception {
        subject = new MraidInterstitial();
        context = new Activity();
        interstitialListener = mock(InterstitialListener.class);
        localExtras = new HashMap<String, Object>();
        serverExtras = new HashMap<String, String>();
        serverExtras.put(AdxConstants.HTML_RESPONSE_BODY_KEY, INPUT_HTML_DATA);
    }

    @Test
    public void loadBanner_withMalformedServerExtras_shouldNotifyInterstitialFailed() throws Exception {
        serverExtras.remove(AdxConstants.HTML_RESPONSE_BODY_KEY);
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);

        verify(interstitialListener).onInterstitialFailed(ErrorCode.NETWORK_INVALID_STATE);
        verify(interstitialListener, never()).onInterstitialLoaded();
    }

    @Test
    public void loadInterstitial_shouldNotifyInterstitialLoaded() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);

//        verify(customEventInterstitialListener).onInterstitialLoaded();
    }

    @Test
    public void loadInterstitial_shouldConnectListenerToBroadcastReceiver() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);

        Intent intent;
        intent = new Intent(AbstractInterstitialActivity.ACTION_INTERSTITIAL_SHOW);
        ShadowLocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        verify(interstitialListener).onInterstitialShown();

        intent = new Intent(AbstractInterstitialActivity.ACTION_INTERSTITIAL_DISMISS);
        ShadowLocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        verify(interstitialListener).onInterstitialDismissed();
    }

    @Test
    public void showInterstitial_shouldStartActivityWithIntent() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);
        subject.showInterstitial();

        ShadowActivity shadowActivity = shadowOf_(context);
        Intent intent = shadowActivity.getNextStartedActivityForResult().intent;

        assertThat(intent.getComponent().getPackageName()).isEqualTo("com.bizense.adatrix.android.mraid");
        assertThat(intent.getComponent().getClassName()).isEqualTo("com.bizense.adatrix.android.mraid.MraidActivity");
        assertThat(intent.getExtras().get(AdxConstants.HTML_RESPONSE_BODY_KEY)).isEqualTo(EXPECTED_HTML_DATA);
        assertThat(intent.getFlags() & Intent.FLAG_ACTIVITY_NEW_TASK).isNotEqualTo(0);
    }

    @Test
    public void onInvalidate_shouldDisconnectListenerToBroadcastReceiver() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);
        subject.onInvalidate();

        Intent intent;
        intent = new Intent(AbstractInterstitialActivity.ACTION_INTERSTITIAL_SHOW);
        ShadowLocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        verify(interstitialListener, never()).onInterstitialShown();

        intent = new Intent(AbstractInterstitialActivity.ACTION_INTERSTITIAL_DISMISS);
        ShadowLocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        verify(interstitialListener, never()).onInterstitialDismissed();
    }
}
