package com.bizense.adatrix.android.html;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.robolectric.Robolectric.shadowOf;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;

import android.app.Activity;
import android.webkit.WebViewClient;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.core.InterstitialListener;
import com.bizense.adatrix.android.html.HtmlInterstitialWebView.HtmlInterstitialWebViewListener;
import com.bizense.adatrix.android.test.SdkTestRunner;
import com.bizense.adatrix.android.utils.ErrorCode;

@RunWith(SdkTestRunner.class)
public class HtmlInterstitialWebViewTest {

    private HtmlInterstitialWebView subject;
    private InterstitialListener interstitialListener;
    private String clickthroughUrl;
    private boolean isScrollable;
    private String redirectUrl;
    private AdxConfiguration adxConfiguration;

    @Before
    public void setUp() throws Exception {
        adxConfiguration = mock(AdxConfiguration.class);
        subject = new HtmlInterstitialWebView(new Activity(), adxConfiguration);
        interstitialListener = mock(InterstitialListener.class);
        isScrollable = false;
        clickthroughUrl = "clickthroughUrl";
        redirectUrl = "redirectUrl";
    }

    @Test
    public void init_shouldSetupWebViewClient() throws Exception {
        subject.init(interstitialListener, false, clickthroughUrl, redirectUrl);
        WebViewClient webViewClient = shadowOf(subject).getWebViewClient();
        assertThat(webViewClient).isNotNull();
        assertThat(webViewClient).isInstanceOf(HtmlWebViewClient.class);
    }

    @Test
    public void htmlBannerWebViewListener_shouldForwardCalls() throws Exception {
        HtmlInterstitialWebViewListener listenerSubject = new HtmlInterstitialWebViewListener(interstitialListener);

        listenerSubject.onLoaded(subject);

        listenerSubject.onFailed(ErrorCode.NETWORK_INVALID_STATE);
        verify(interstitialListener).onInterstitialFailed(eq(ErrorCode.NETWORK_INVALID_STATE));

        listenerSubject.onClicked();
        verify(interstitialListener).onInterstitialClicked();
    }

    @Test
    public void init_shouldAddJavascriptInterface() throws Exception {
        subject.init(interstitialListener, isScrollable, clickthroughUrl, redirectUrl);

        Object javascriptInterface = shadowOf(subject).getJavascriptInterface("adxUriInterface");
        assertThat(javascriptInterface).isNotNull();

        Method fireFinishLoad = javascriptInterface.getClass().getDeclaredMethod("fireFinishLoad");
        Robolectric.pauseMainLooper();
        boolean returnValue = (Boolean) fireFinishLoad.invoke(javascriptInterface);
        assertThat(returnValue).isTrue();
        verify(interstitialListener, never()).onInterstitialShown();

        Robolectric.unPauseMainLooper();
        verify(interstitialListener).onInterstitialLoaded();
    }
}
