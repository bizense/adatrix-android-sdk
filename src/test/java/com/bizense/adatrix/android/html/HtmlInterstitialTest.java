package com.bizense.adatrix.android.html;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowLocalBroadcastManager;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.activities.AbstractInterstitialActivity;
import com.bizense.adatrix.android.core.InterstitialListener;
import com.bizense.adatrix.android.test.SdkTestRunner;
import com.bizense.adatrix.android.test.TestHtmlInterstitialWebViewFactory;
import com.bizense.adatrix.android.utils.ErrorCode;

@RunWith(SdkTestRunner.class)
public class HtmlInterstitialTest extends ResponseBodyInterstitialTest {
    private InterstitialListener interstitialListener;
    private Activity context;
    private Map<String,Object> localExtras;
    private Map<String,String> serverExtras;
    private HtmlInterstitialWebView htmlInterstitialWebView;
    private String expectedResponse;

    @Before
    public void setUp() throws Exception {
        subject = new HtmlInterstitial();

        expectedResponse = "this is the response";
        htmlInterstitialWebView = TestHtmlInterstitialWebViewFactory.getSingletonMock();
        context = new Activity();
        interstitialListener = mock(InterstitialListener.class);
        localExtras = new HashMap<String, Object>();
        serverExtras = new HashMap<String, String>();
        serverExtras.put(AdxConstants.HTML_RESPONSE_BODY_KEY, Uri.encode(expectedResponse));
    }

    @Test
    public void loadInterstitial_shouldNotifyCustomEventInterstitialListenerOnLoaded() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);

//        verify(customEventInterstitialListener).onInterstitialLoaded();
    }

    @Test
    public void loadInterstitial_whenNoHtmlResponsePassedIn_shouldCallLoadFailUrl() throws Exception {
        serverExtras.remove(AdxConstants.HTML_RESPONSE_BODY_KEY);
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);

        assertThat(TestHtmlInterstitialWebViewFactory.getLatestListener()).isNull();
        assertThat(TestHtmlInterstitialWebViewFactory.getLatestIsScrollable()).isFalse();
        assertThat(TestHtmlInterstitialWebViewFactory.getLatestRedirectUrl()).isNull();
        assertThat(TestHtmlInterstitialWebViewFactory.getLatestClickthroughUrl()).isNull();
        verify(interstitialListener).onInterstitialFailed(ErrorCode.NETWORK_INVALID_STATE);
        verify(htmlInterstitialWebView, never()).loadHtmlResponse(anyString());
    }


    @Test
    public void showInterstitial_withMinimumExtras_shouldStartInterstitialActivityWithDefaults() throws Exception {

        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);
        subject.showInterstitial();

        Intent nextStartedActivity = Robolectric.getShadowApplication().getNextStartedActivity();
        assertThat(nextStartedActivity.getStringExtra(AdxConstants.HTML_RESPONSE_BODY_KEY)).isEqualTo(expectedResponse);
        assertThat(nextStartedActivity.getBooleanExtra(AdxConstants.SCROLLABLE_KEY, false)).isFalse();
        assertThat(nextStartedActivity.getStringExtra(AdxConstants.REDIRECT_URL_KEY)).isNull();
        assertThat(nextStartedActivity.getStringExtra(AdxConstants.CLICKTHROUGH_URL_KEY)).isNull();
        assertThat(nextStartedActivity.getFlags() & Intent.FLAG_ACTIVITY_NEW_TASK).isNotEqualTo(0);
        assertThat(nextStartedActivity.getComponent().getClassName()).isEqualTo("com.bizense.adatrix.android.activities.InterstitialActivity");
    }

    @Test
    public void showInterstitial_shouldStartInterstitialActivityWithAllExtras() throws Exception {
        serverExtras.put(AdxConstants.SCROLLABLE_KEY, "true");
        serverExtras.put(AdxConstants.REDIRECT_URL_KEY, "redirectUrl");
        serverExtras.put(AdxConstants.CLICKTHROUGH_URL_KEY, "clickthroughUrl");

        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);
        subject.showInterstitial();

        Intent nextStartedActivity = Robolectric.getShadowApplication().getNextStartedActivity();
        assertThat(nextStartedActivity.getStringExtra(AdxConstants.HTML_RESPONSE_BODY_KEY)).isEqualTo(expectedResponse);
        assertThat(nextStartedActivity.getBooleanExtra(AdxConstants.SCROLLABLE_KEY, false)).isTrue();
        assertThat(nextStartedActivity.getStringExtra(AdxConstants.REDIRECT_URL_KEY)).isEqualTo("redirectUrl");
        assertThat(nextStartedActivity.getStringExtra(AdxConstants.CLICKTHROUGH_URL_KEY)).isEqualTo("clickthroughUrl");
        assertThat(nextStartedActivity.getFlags() & Intent.FLAG_ACTIVITY_NEW_TASK).isNotEqualTo(0);
        assertThat(nextStartedActivity.getComponent().getClassName()).isEqualTo("com.bizense.adatrix.android.activities.InterstitialActivity");
    }

    @Test
    public void loadInterstitial_shouldConnectListenerToBroadcastReceiver() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);

        Intent intent;
        intent = new Intent(AbstractInterstitialActivity.ACTION_INTERSTITIAL_SHOW);
        ShadowLocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        verify(interstitialListener).onInterstitialShown();

        intent = new Intent(AbstractInterstitialActivity.ACTION_INTERSTITIAL_DISMISS);
        ShadowLocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        verify(interstitialListener).onInterstitialDismissed();
    }

    @Test
    public void onInvalidate_shouldDisconnectListenerToBroadcastReceiver() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);
        subject.onInvalidate();

        Intent intent;
        intent = new Intent(AbstractInterstitialActivity.ACTION_INTERSTITIAL_SHOW);
        ShadowLocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        verify(interstitialListener, never()).onInterstitialShown();

        intent = new Intent(AbstractInterstitialActivity.ACTION_INTERSTITIAL_DISMISS);
        ShadowLocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        verify(interstitialListener, never()).onInterstitialDismissed();
    }
}
