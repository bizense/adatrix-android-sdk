package com.bizense.adatrix.android.html;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.robolectric.Robolectric.shadowOf;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.app.Activity;
import android.webkit.WebViewClient;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.core.BannerListener;
import com.bizense.adatrix.android.test.SdkTestRunner;
import com.bizense.adatrix.android.utils.ErrorCode;

@RunWith(SdkTestRunner.class)
public class HtmlBannerWebViewTest {

    private AdxConfiguration adxConfiguration;
    private HtmlBannerWebView subject;
    private BannerListener bannerListener;
    private String clickthroughUrl;
    private String redirectUrl;

    @Before
    public void setup() throws Exception {
        adxConfiguration = mock(AdxConfiguration.class);
        subject = new HtmlBannerWebView(new Activity(), adxConfiguration);
        bannerListener = mock(BannerListener.class);
        clickthroughUrl = "clickthroughUrl";
        redirectUrl = "redirectUrl";
    }

    @Test
    public void init_shouldSetupWebViewClient() throws Exception {
        subject.init(bannerListener, false, clickthroughUrl, redirectUrl);
        WebViewClient webViewClient = shadowOf(subject).getWebViewClient();
        assertThat(webViewClient).isNotNull();
        assertThat(webViewClient).isInstanceOf(HtmlWebViewClient.class);
    }

    @Test
    public void htmlBannerWebViewListener_shouldForwardCalls() throws Exception {
        HtmlBannerWebView.HtmlBannerWebViewListener listenerSubject = new HtmlBannerWebView.HtmlBannerWebViewListener(bannerListener);

        listenerSubject.onClicked();
        verify(bannerListener).onBannerClicked();

        listenerSubject.onLoaded(subject);
        verify(bannerListener).onBannerLoaded(eq(subject));

        listenerSubject.onCollapsed();
        verify(bannerListener).onBannerCollapsed();

        listenerSubject.onFailed(ErrorCode.NETWORK_INVALID_STATE);
        verify(bannerListener).onBannerFailed(eq(ErrorCode.NETWORK_INVALID_STATE));
    }
}
