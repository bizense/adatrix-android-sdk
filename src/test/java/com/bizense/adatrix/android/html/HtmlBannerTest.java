package com.bizense.adatrix.android.html;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.stub;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;

import android.app.Activity;
import android.net.Uri;
import android.view.Gravity;
import android.widget.FrameLayout;

import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.core.AdView;
import com.bizense.adatrix.android.core.AdViewController;
import com.bizense.adatrix.android.core.BannerListener;
import com.bizense.adatrix.android.test.SdkTestRunner;
import com.bizense.adatrix.android.test.TestHtmlBannerWebViewFactory;
import com.bizense.adatrix.android.test.TestHttpResponseWithHeaders;
import com.bizense.adatrix.android.utils.ErrorCode;

@RunWith(SdkTestRunner.class)
public class HtmlBannerTest {

    private HtmlBanner subject;
    private HtmlBannerWebView htmlBannerWebView;
    private BannerListener bannerListener;
    private Map<String, Object> localExtras;
    private Map<String, String> serverExtras;
    private Activity context;
    private String responseBody;

    @Before
    public void setup() {
        subject = new HtmlBanner();
        htmlBannerWebView = mock(HtmlBannerWebView.class);
        bannerListener = mock(BannerListener.class);
        context = new Activity();
        localExtras = new HashMap<String, Object>();
        serverExtras = new HashMap<String, String>();
        responseBody = "expected response body";
        serverExtras.put(AdxConstants.HTML_RESPONSE_BODY_KEY, Uri.encode(responseBody));
        serverExtras.put(AdxConstants.SCROLLABLE_KEY, "false");
    }

    @Test
    public void loadBanner_shouldPopulateTheHtmlWebViewWithHtml() throws Exception {
        subject.loadBanner(context, bannerListener, localExtras, serverExtras);

        assertThat(TestHtmlBannerWebViewFactory.getLatestListener()).isSameAs(bannerListener);
        assertThat(TestHtmlBannerWebViewFactory.getLatestIsScrollable()).isFalse();
        assertThat(TestHtmlBannerWebViewFactory.getLatestRedirectUrl()).isNull();
        assertThat(TestHtmlBannerWebViewFactory.getLatestClickthroughUrl()).isNull();
        verify(htmlBannerWebView).loadHtmlResponse(responseBody);
    }

    @Test
    public void loadBanner_whenNoHtmlResponse_shouldNotifyBannerFailed() throws Exception {
        serverExtras.remove(AdxConstants.HTML_RESPONSE_BODY_KEY);
        subject.loadBanner(context, bannerListener, localExtras, serverExtras);

        verify(bannerListener).onBannerFailed(eq(ErrorCode.NETWORK_INVALID_STATE));
        assertThat(TestHtmlBannerWebViewFactory.getLatestListener()).isNull();
        assertThat(TestHtmlBannerWebViewFactory.getLatestIsScrollable()).isFalse();
        assertThat(TestHtmlBannerWebViewFactory.getLatestRedirectUrl()).isNull();
        assertThat(TestHtmlBannerWebViewFactory.getLatestClickthroughUrl()).isNull();
        verify(htmlBannerWebView, never()).loadHtmlResponse(anyString());
    }

    @Test
    public void loadBanner_shouldPassParametersThrough() throws Exception {
        serverExtras.put(AdxConstants.SCROLLABLE_KEY, "true");
        serverExtras.put(AdxConstants.REDIRECT_URL_KEY, "redirectUrl");
        serverExtras.put(AdxConstants.CLICKTHROUGH_URL_KEY, "clickthroughUrl");
        subject.loadBanner(context, bannerListener, localExtras, serverExtras);

        assertThat(TestHtmlBannerWebViewFactory.getLatestListener()).isSameAs(bannerListener);
        assertThat(TestHtmlBannerWebViewFactory.getLatestIsScrollable()).isTrue();
        assertThat(TestHtmlBannerWebViewFactory.getLatestRedirectUrl()).isEqualTo("redirectUrl");
        assertThat(TestHtmlBannerWebViewFactory.getLatestClickthroughUrl()).isEqualTo("clickthroughUrl");
        verify(htmlBannerWebView).loadHtmlResponse(responseBody);
    }

    @Test
    public void onInvalidate_shouldDestroyTheHtmlWebView() throws Exception {
        subject.loadBanner(context, bannerListener, localExtras, serverExtras);
        subject.onInvalidate();

        verify(htmlBannerWebView).destroy();
    }

    @Test
    public void loadBanner_shouldCauseServerDimensionsToBeHonoredWhenLayingOutView() throws Exception {
        subject.loadBanner(context, bannerListener, localExtras, serverExtras);
        AdView adView = mock(AdView.class);
        stub(adView.getContext()).toReturn(context);
        AdViewController adViewController = new AdViewController(context, adView);

        HttpResponse response = new TestHttpResponseWithHeaders(200, "I ain't got no-body");
        response.addHeader("X-Width", "320");
        response.addHeader("X-Height", "50");
        adViewController.configureUsingHttpResponse(response);

        adViewController.setAdContentView(htmlBannerWebView);
        ArgumentCaptor<FrameLayout.LayoutParams> layoutParamsCaptor = ArgumentCaptor.forClass(FrameLayout.LayoutParams.class);
        verify(adView).addView(eq(htmlBannerWebView), layoutParamsCaptor.capture());
        FrameLayout.LayoutParams layoutParams = layoutParamsCaptor.getValue();

        assertThat(layoutParams.width).isEqualTo(320);
        assertThat(layoutParams.height).isEqualTo(50);
        assertThat(layoutParams.gravity).isEqualTo(Gravity.CENTER);
    }
}
