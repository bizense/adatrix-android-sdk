package com.bizense.adatrix.android.video;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.stub;
import static org.mockito.Mockito.verify;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowLocalBroadcastManager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.bizense.adatrix.android.AdxConstants;
import com.bizense.adatrix.android.activities.AbstractInterstitialActivity;
import com.bizense.adatrix.android.core.InterstitialListener;
import com.bizense.adatrix.android.html.ResponseBodyInterstitialTest;
import com.bizense.adatrix.android.network.AdFetcher;
import com.bizense.adatrix.android.test.SdkTestRunner;
import com.bizense.adatrix.android.test.TestHttpResponseWithHeaders;
import com.bizense.adatrix.android.utils.DiskLRUCache;
import com.bizense.adatrix.android.utils.ErrorCode;
import com.bizense.adatrix.android.video.VastManager.VastManagerListener;

@RunWith(SdkTestRunner.class)
public class VastVideoInterstitialTest extends ResponseBodyInterstitialTest {
    private Context context;
    private InterstitialListener interstitialListener;
    private Map<String, Object> localExtras;
    private Map<String, String> serverExtras;
    private TestHttpResponseWithHeaders response;
    private String expectedResponse;
    private VastManager vastManager;
    private String videoUrl;
    private VastVideoDownloadTask vastVideoDownloadTask;

    @Before
    public void setUp() throws Exception {
        subject = new VastVideoInterstitial();

        vastVideoDownloadTask = mock(VastVideoDownloadTask.class);
        vastManager = mock(VastManager.class);
        expectedResponse = "<VAST>hello</VAST>";
        videoUrl = "http://www.video.com";

        context = new Activity();
        interstitialListener = mock(InterstitialListener.class);
        localExtras = new HashMap<String, Object>();
        serverExtras = new HashMap<String, String>();
        serverExtras.put(AdFetcher.HTML_RESPONSE_BODY_KEY, Uri.encode(expectedResponse));

        response = new TestHttpResponseWithHeaders(200, expectedResponse);
    }

    @After
    public void tearDown() throws Exception {
        reset(vastVideoDownloadTask);
    }

    @Test
    public void preRenderHtml_whenCreatingVideoCache_butItHasInitializationErrors_shouldSignalOnInterstitialFailedOnError() throws Exception {
        // context is null when loadInterstitial is not called, which causes DiskLruCache to not be created

        subject.preRenderHtml(interstitialListener);

        verify(interstitialListener).onInterstitialFailed(eq(ErrorCode.VIDEO_CACHE_ERROR));
        verify(vastManager, never()).processVast(anyString(), any(VastManagerListener.class));
    }

    @Test
    public void loadInterstitial_shouldParseHtmlResponseBodyServerExtra() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);

        assertThat(((VastVideoInterstitial) subject).getVastResponse()).isEqualTo(expectedResponse);
    }

    @Test
    public void loadInterstitial_shouldInitializeVideoCache() throws Exception {
        Robolectric.addPendingHttpResponse(response);

        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);

        DiskLRUCache videoCache = ((VastVideoInterstitial) subject).getVideoCache();
        assertThat(videoCache).isNotNull();
        assertThat(videoCache.getCacheDirectory().getName()).isEqualTo("adx_vast_video_cache");
        assertThat(videoCache.maxSize()).isEqualTo(100 * 1000 * 1000);
    }

    @Test
    public void loadInterstitial_shouldCreateVastManagerAndProcessVast() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);

        verify(vastManager).processVast(eq(expectedResponse), eq((VastVideoInterstitial) subject));
    }

    @Test
    public void loadInterstitial_whenServerExtrasDoesNotContainResponse_shouldSignalOnInterstitialFailed() throws Exception {
        serverExtras.remove(AdxConstants.HTML_RESPONSE_BODY_KEY);

        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);

        verify(interstitialListener).onInterstitialFailed(ErrorCode.NETWORK_INVALID_STATE);
        verify(vastManager, never()).processVast(anyString(), any(VastManagerListener.class));
    }

    @Test
    public void loadInterstitial_shouldConnectListenerToBroadcastReceiver() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);

        Intent intent;
        intent = new Intent(AbstractInterstitialActivity.ACTION_INTERSTITIAL_SHOW);
        ShadowLocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        verify(interstitialListener).onInterstitialShown();

        intent = new Intent(AbstractInterstitialActivity.ACTION_INTERSTITIAL_DISMISS);
        ShadowLocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        verify(interstitialListener).onInterstitialDismissed();
    }

    @Test
    public void showInterstitial_shouldStartVideoPlayerActivityWithAllValidTrackers() throws Exception {
        stub(vastManager.getMediaFileUrl()).toReturn(videoUrl);

        stub(vastManager.getVideoStartTrackers()).toReturn(wrapInList("start"));
        stub(vastManager.getVideoFirstQuartileTrackers()).toReturn(wrapInList("first"));
        stub(vastManager.getVideoMidpointTrackers()).toReturn(wrapInList("mid"));
        stub(vastManager.getVideoThirdQuartileTrackers()).toReturn(wrapInList("third"));
        stub(vastManager.getVideoCompleteTrackers()).toReturn(wrapInList("complete"));
        stub(vastManager.getImpressionTrackers()).toReturn(wrapInList("imp"));
        stub(vastManager.getClickThroughUrl()).toReturn("clickThrough");
        stub(vastManager.getClickTrackers()).toReturn(wrapInList("click"));

        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);
        ((VastVideoInterstitial) subject).onComplete(vastManager);
        ((VastVideoInterstitial) subject).onDownloadSuccess();

        subject.showInterstitial();

        Intent nextActivity = Robolectric.getShadowApplication().getNextStartedActivity();
        assertThat(nextActivity.getComponent().getClassName()).isEqualTo("com.bizense.adatrix.android.mraid.MraidVideoPlayerActivity");
        assertThat(nextActivity.getFlags() & Intent.FLAG_ACTIVITY_NEW_TASK).isNotEqualTo(0);

        assertThat(nextActivity.getStringArrayListExtra(VastVideoView.VIDEO_START_TRACKERS).get(0)).isEqualTo("start");
        assertThat(nextActivity.getStringArrayListExtra(VastVideoView.VIDEO_FIRST_QUARTER_TRACKERS).get(0)).isEqualTo("first");
        assertThat(nextActivity.getStringArrayListExtra(VastVideoView.VIDEO_MID_POINT_TRACKERS).get(0)).isEqualTo("mid");
        assertThat(nextActivity.getStringArrayListExtra(VastVideoView.VIDEO_THIRD_QUARTER_TRACKERS).get(0)).isEqualTo("third");
        assertThat(nextActivity.getStringArrayListExtra(VastVideoView.VIDEO_COMPLETE_TRACKERS).get(0)).isEqualTo("complete");
        assertThat(nextActivity.getStringArrayListExtra(VastVideoView.VIDEO_IMPRESSION_TRACKERS).get(0)).isEqualTo("imp");
        assertThat(nextActivity.getStringExtra(VastVideoView.VIDEO_CLICK_THROUGH_URL)).isEqualTo("clickThrough");
        assertThat(nextActivity.getStringArrayListExtra(VastVideoView.VIDEO_CLICK_THROUGH_TRACKERS).get(0)).isEqualTo("click");
    }

    @Test
    public void onInvalidate_shouldCancelVastManager() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);
        subject.onInvalidate();

        verify(vastManager).cancel();
    }

    @Test
    public void onInvalidate_whenVastManagerIsNull_shouldNotBlowUp() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);

        ((VastVideoInterstitial) subject).setVastManager(null);

        subject.onInvalidate();

        // pass
    }

    @Test
    public void onInvalidate_shouldDisconnectListenerToBroadcastReceiver() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);
        subject.onInvalidate();

        Intent intent;
        intent = new Intent(AbstractInterstitialActivity.ACTION_INTERSTITIAL_SHOW);
        ShadowLocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        verify(interstitialListener, never()).onInterstitialShown();

        intent = new Intent(AbstractInterstitialActivity.ACTION_INTERSTITIAL_DISMISS);
        ShadowLocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        verify(interstitialListener, never()).onInterstitialDismissed();
    }

    @Ignore("pending")
    @Test
    public void onComplete_whenVideoCacheHit_shouldCallOnDownloadSuccess() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);

        stub(vastManager.getMediaFileUrl()).toReturn(videoUrl);
        DiskLRUCache videoCache = ((VastVideoInterstitial) subject).getVideoCache();
        videoCache.putStream(videoUrl, new ByteArrayInputStream("some data".getBytes()));

        ((VastVideoInterstitial) subject).onComplete(vastManager);

        verify(interstitialListener).onInterstitialLoaded();
        verify(vastVideoDownloadTask, never()).execute((String[])anyVararg());
    }

    @Ignore("pending")
    @Test
    public void onComplete_whenVideoCacheMiss_shouldStartVastVideoDownloadTask() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);

        stub(vastManager.getMediaFileUrl()).toReturn(videoUrl);
        DiskLRUCache videoCache = ((VastVideoInterstitial) subject).getVideoCache();
        videoCache.putStream("another_video_not_in_cache", new ByteArrayInputStream("some data".getBytes()));

        ((VastVideoInterstitial) subject).onComplete(vastManager);

        verify(vastVideoDownloadTask).execute(eq(videoUrl));
        verify(interstitialListener, never()).onInterstitialLoaded();
    }

    @Test
    public void onDownloadSuccess_shouldSignalOnInterstitialLoaded() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);
        ((VastVideoInterstitial) subject).onDownloadSuccess();

        verify(interstitialListener).onInterstitialLoaded();
    }

    @Test
    public void onDownloadFailed_shouldSignalOnInterstitialFailed() throws Exception {
        subject.loadInterstitial(context, interstitialListener, localExtras, serverExtras);
        ((VastVideoInterstitial) subject).onDownloadFailed();

        verify(interstitialListener).onInterstitialFailed(eq(ErrorCode.VIDEO_DOWNLOAD_ERROR));
    }

    private <T> List<T> wrapInList(T object) {
        List<T> result = new ArrayList<T>();
        result.add(object);
        return result;
    }
}
