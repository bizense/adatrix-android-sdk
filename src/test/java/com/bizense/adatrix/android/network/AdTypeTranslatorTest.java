package com.bizense.adatrix.android.network;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.bizense.adatrix.android.core.AdView;
import com.bizense.adatrix.android.core.Interstitial;
import com.bizense.adatrix.android.test.SdkTestRunner;

@RunWith(SdkTestRunner.class)
public class AdTypeTranslatorTest {
    private AdTypeTranslator subject;
    private String customEventName;
    private AdView adView;
    private Interstitial.InterstitialView interstitialView;

    @Before
    public void setUp() throws Exception {
        subject = new AdTypeTranslator();
        adView = mock(AdView.class);
        interstitialView = mock(Interstitial.InterstitialView.class);
    }

    @Test
    public void getAdMobBanner() throws Exception {
        customEventName = AdTypeTranslator.getCustomEventNameForAdType(adView, "admob_native", null);

        assertThat(customEventName).isEqualTo(AdTypeTranslator.ADMOB_BANNER);
    }

    @Test
    public void getAdMobInterstitial() throws Exception {
        customEventName = AdTypeTranslator.getCustomEventNameForAdType(interstitialView, "interstitial", "admob_full");

        assertThat(customEventName).isEqualTo(AdTypeTranslator.ADMOB_INTERSTITIAL);
    }

    @Test
    public void getMillennialBanner() throws Exception {
        customEventName = AdTypeTranslator.getCustomEventNameForAdType(adView, "millennial_native", null);

        assertThat(customEventName).isEqualTo(AdTypeTranslator.MILLENNIAL_BANNER);
    }

    @Test
    public void getMillennnialInterstitial() throws Exception {
        customEventName = AdTypeTranslator.getCustomEventNameForAdType(interstitialView, "interstitial", "millennial_full");

        assertThat(customEventName).isEqualTo(AdTypeTranslator.MILLENNIAL_INTERSTITIAL);
    }

    @Test
    public void getMraidBanner() throws Exception {
        customEventName = AdTypeTranslator.getCustomEventNameForAdType(adView, "mraid", null);

        assertThat(customEventName).isEqualTo(AdTypeTranslator.MRAID_BANNER);
    }

    @Test
    public void getMraidInterstitial() throws Exception {
        customEventName = AdTypeTranslator.getCustomEventNameForAdType(interstitialView, "mraid", null);

        assertThat(customEventName).isEqualTo(AdTypeTranslator.MRAID_INTERSTITIAL);
    }

    @Test
    public void getHtmlBanner() throws Exception {
        customEventName = AdTypeTranslator.getCustomEventNameForAdType(adView, "html", null);

        assertThat(customEventName).isEqualTo(AdTypeTranslator.HTML_BANNER);
    }

    @Test
    public void getHtmlInterstitial() throws Exception {
        customEventName = AdTypeTranslator.getCustomEventNameForAdType(interstitialView, "html", null);

        assertThat(customEventName).isEqualTo(AdTypeTranslator.HTML_INTERSTITIAL);
    }

    @Test
    public void getVastInterstitial() throws Exception {
        customEventName = AdTypeTranslator.getCustomEventNameForAdType(interstitialView, "interstitial", "vast");

        assertThat(customEventName).isEqualTo(AdTypeTranslator.VAST_VIDEO_INTERSTITIAL);
    }

    @Test
    public void getCustomEventNameForAdType_whenSendingNonsense_shouldReturnNull() throws Exception {
        customEventName = AdTypeTranslator.getCustomEventNameForAdType(null, null, null);

        assertThat(customEventName).isNull();
    }
}
