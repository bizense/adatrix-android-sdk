package com.bizense.adatrix.android.network;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.core.AdViewController;
import com.bizense.adatrix.android.core.Interstitial;
import com.bizense.adatrix.android.test.SdkTestRunner;
import com.bizense.adatrix.android.test.TestHttpResponseWithHeaders;
import com.bizense.adatrix.android.utils.ResponseHeader;

@RunWith(SdkTestRunner.class)
public class AdLoadTaskTest {

    private AdViewController adViewController;
    private HttpResponse response;
    private String standardExpectedJson;
    private AdxConfiguration adxConfiguration;

    @Before
    public void setup() {
        adViewController = mock(AdViewController.class);
        adxConfiguration = mock(AdxConfiguration.class);
        stub(adViewController.getAdxConfiguration()).toReturn(adxConfiguration);
        response = new TestHttpResponseWithHeaders(200, "");
        standardExpectedJson = "{\"Scrollable\":\"false\",\"Redirect-Url\":\"redirect\",\"Clickthrough-Url\":\"clickthrough\",\"Html-Response-Body\":\"%3Chtml%3E%3C%2Fhtml%3E\"}";
    }

    @Test
    public void fromHttpResponse_whenCustomEvent_shouldGetNameAndData() throws Exception {
        String expectedCustomData = "Custom data";
        response.addHeader(ResponseHeader.AD_TYPE.getKey(), "custom");
        String expectedCustomEventName = "custom event name";
        response.addHeader(ResponseHeader.CUSTOM_EVENT_NAME.getKey(), expectedCustomEventName);
        response.addHeader(ResponseHeader.CUSTOM_EVENT_DATA.getKey(), expectedCustomData);

        AdLoadTask.CustomEventAdLoadTask customEventTask = (AdLoadTask.CustomEventAdLoadTask) AdLoadTask.fromHttpResponse(response, adViewController);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_NAME.getKey())).isEqualTo(expectedCustomEventName);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_DATA.getKey())).isEqualTo(expectedCustomData);
    }

    @Test
    public void fromHttpResponse_whenMraidBanner_shouldCreateAnEncodedJsonString() throws Exception {
        String htmlData = "<html></html>";
        response = new TestHttpResponseWithHeaders(200, htmlData);
        addExpectedResponseHeaders("mraid");

        AdLoadTask.CustomEventAdLoadTask customEventTask = (AdLoadTask.CustomEventAdLoadTask) AdLoadTask.fromHttpResponse(response, adViewController);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_NAME.getKey())).isEqualTo(AdTypeTranslator.MRAID_BANNER);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_DATA.getKey())).isEqualTo(standardExpectedJson);
    }

    @Test
    public void fromHttpResponse_whenMraidInterstitial_shouldCreateAnEncodedJsonString() throws Exception {
        String htmlData = "<html></html>";
        response = new TestHttpResponseWithHeaders(200, htmlData);
        addExpectedResponseHeaders("mraid");
        stub(adViewController.getAdView()).toReturn(mock(Interstitial.InterstitialView.class));

        AdLoadTask.CustomEventAdLoadTask customEventTask = (AdLoadTask.CustomEventAdLoadTask) AdLoadTask.fromHttpResponse(response, adViewController);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_NAME.getKey())).isEqualTo(AdTypeTranslator.MRAID_INTERSTITIAL);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_DATA.getKey())).isEqualTo(standardExpectedJson);
    }

    @Test
    public void fromHttpResponse_whenCustomEventDelegate_shouldConvertAdMobToCustomEvent() throws Exception {
        String expectedNativeParams = "{\"this is a json\":\"map\",\"whee\":\"look at me\"}";
        response.addHeader(ResponseHeader.AD_TYPE.getKey(), "admob_native");
        response.addHeader(ResponseHeader.NATIVE_PARAMS.getKey(), expectedNativeParams);

        AdLoadTask.CustomEventAdLoadTask customEventTask = (AdLoadTask.CustomEventAdLoadTask) AdLoadTask.fromHttpResponse(response, adViewController);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_NAME.getKey())).isEqualTo(AdTypeTranslator.ADMOB_BANNER);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_DATA.getKey())).isEqualTo(expectedNativeParams);
    }

    @Test
    public void fromHttpResponse_whenHtmlBanner_shouldConvertToCustomEventBanner() throws Exception {
        String htmlData = "<html></html>";
        response = new TestHttpResponseWithHeaders(200, htmlData);
        addExpectedResponseHeaders("html");

        AdLoadTask.CustomEventAdLoadTask customEventTask = (AdLoadTask.CustomEventAdLoadTask) AdLoadTask.fromHttpResponse(response, adViewController);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_NAME.getKey())).isEqualTo(AdTypeTranslator.HTML_BANNER);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_DATA.getKey())).isEqualTo(standardExpectedJson);
    }

    @Test
    public void fromHttpResponse_whenHtmlInterstitial_shouldConvertToCustomEventInterstitial() throws Exception {
        String htmlData = "<html></html>";
        response = new TestHttpResponseWithHeaders(200, htmlData);
        addExpectedResponseHeaders("html");
        stub(adViewController.getAdView()).toReturn(mock(Interstitial.InterstitialView.class));

        AdLoadTask.CustomEventAdLoadTask customEventTask = (AdLoadTask.CustomEventAdLoadTask) AdLoadTask.fromHttpResponse(response, adViewController);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_NAME.getKey())).isEqualTo(AdTypeTranslator.HTML_INTERSTITIAL);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_DATA.getKey())).isEqualTo(standardExpectedJson);
    }

    @Test
    public void fromHttpResponse_whenEntityIsNull_shouldCreateMinimumJsonString() throws Exception {
        String htmlData = "<html></html>";
        String expectedJson = "{\"Scrollable\":\"false\",\"Html-Response-Body\":\"\"}";
        response = new TestHttpResponseWithHeaders(200, htmlData) {
            @Override
            public HttpEntity getEntity() {
                return null;
            }
        };
        response.addHeader(ResponseHeader.AD_TYPE.getKey(), "html");

        AdLoadTask.CustomEventAdLoadTask customEventTask = (AdLoadTask.CustomEventAdLoadTask) AdLoadTask.fromHttpResponse(response, adViewController);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_NAME.getKey())).isEqualTo(AdTypeTranslator.HTML_BANNER);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_DATA.getKey())).isEqualTo(expectedJson);
    }

    @Test
    public void fromHttpResponse_whenScrollableIsOne_shouldBeReflectedInJson() throws Exception {
        String expectedJson = "{\"Scrollable\":\"true\",\"Html-Response-Body\":\"\"}";
        response.addHeader(ResponseHeader.SCROLLABLE.getKey(), "1");
        response.addHeader(ResponseHeader.AD_TYPE.getKey(), "html");


        AdLoadTask.CustomEventAdLoadTask customEventTask = (AdLoadTask.CustomEventAdLoadTask) AdLoadTask.fromHttpResponse(response, adViewController);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_NAME.getKey())).isEqualTo(AdTypeTranslator.HTML_BANNER);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_DATA.getKey())).isEqualTo(expectedJson);
    }

    @Test
    public void fromHttpResponse_whenScrollableIsNotSpecified_shouldDefaultToFalseInJson() throws Exception {
        String expectedJson = "{\"Scrollable\":\"false\",\"Html-Response-Body\":\"\"}";
        response.addHeader(ResponseHeader.AD_TYPE.getKey(), "html");

        AdLoadTask.CustomEventAdLoadTask customEventTask = (AdLoadTask.CustomEventAdLoadTask) AdLoadTask.fromHttpResponse(response, adViewController);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_NAME.getKey())).isEqualTo(AdTypeTranslator.HTML_BANNER);
        assertThat(customEventTask.getParamsMap().get(ResponseHeader.CUSTOM_EVENT_DATA.getKey())).isEqualTo(expectedJson);
    }

    private void addExpectedResponseHeaders(String adType) {
        response.addHeader(ResponseHeader.SCROLLABLE.getKey(), "0");
        response.addHeader(ResponseHeader.AD_TYPE.getKey(), adType);
        response.addHeader(ResponseHeader.REDIRECT_URL.getKey(), "redirect");
        response.addHeader(ResponseHeader.CLICKTHROUGH_URL.getKey(), "clickthrough");
    }
}
