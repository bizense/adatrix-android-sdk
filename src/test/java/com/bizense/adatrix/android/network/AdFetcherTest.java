package com.bizense.adatrix.android.network;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.stub;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;

import org.apache.http.HttpResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;

import android.os.Build;

import com.bizense.adatrix.android.AdxConfiguration;
import com.bizense.adatrix.android.core.AdView;
import com.bizense.adatrix.android.core.AdViewController;
import com.bizense.adatrix.android.core.Interstitial;
import com.bizense.adatrix.android.test.SdkTestRunner;
import com.bizense.adatrix.android.test.TestHttpResponseWithHeaders;
import com.bizense.adatrix.android.utils.AndroidVersion;
import com.bizense.adatrix.android.utils.ResponseHeader;

@RunWith(SdkTestRunner.class)
public class AdFetcherTest {
    private AdFetcher subject;
    private AdViewController adViewController;
    private AdView adView;
    private HttpResponse response;

    @Before
    public void setup() {
        adViewController = mock(AdViewController.class);
        adView = mock(AdView.class);
        stub(adViewController.getAdView()).toReturn(adView);

        subject = new AdFetcher(adViewController, "expected userAgent");
        response = new TestHttpResponseWithHeaders(200, "yahoo!!!");
    }

    @Test
    public void shouldSendResponseToAdView() {
        Robolectric.addPendingHttpResponse(response);

        subject.fetchAdForUrl("url");

        verify(adViewController).configureUsingHttpResponse(eq(response));
    }

    @Test
    public void fetchAdForUrl_shouldRouteMillennialBannerToCustomEventHandling() throws Exception {
        String json = "{\"adWidth\": 320, \"adHeight\": 50, \"adUnitID\": \"44310\"}";
        stub(adViewController.getAdxConfiguration()).toReturn(mock(AdxConfiguration.class));
        response.addHeader(ResponseHeader.AD_TYPE.getKey(), "millennial_native");
        response.addHeader(ResponseHeader.NATIVE_PARAMS.getKey(), json);
        Robolectric.addPendingHttpResponse(response);

        subject.fetchAdForUrl("ignored_url");

        Map<String, String> paramsMap = new HashMap<String, String>();
        paramsMap.put(ResponseHeader.CUSTOM_EVENT_NAME.getKey(), "com.bizense.adatrix.android.MillennialBanner");
        paramsMap.put(ResponseHeader.CUSTOM_EVENT_DATA.getKey(), json);

        verify(adView).load(eq(paramsMap));
    }

    @Test
    public void fetchAdForUrl_shouldRouteMillennialInterstitialToCustomEventHandling() throws Exception {
        AdViewController interstitialAdViewController = mock(AdViewController.class);
        Interstitial.InterstitialView interstitialView = mock(Interstitial.InterstitialView.class);
        stub(interstitialAdViewController.getAdView()).toReturn(interstitialView);
        stub(interstitialAdViewController.getAdxConfiguration()).toReturn(mock(AdxConfiguration.class));
        subject = new AdFetcher(interstitialAdViewController, "expected userAgent");

        String json = "{\"adWidth\": 320, \"adHeight\": 480, \"adUnitID\": \"44310\"}";
        response.addHeader(ResponseHeader.AD_TYPE.getKey(), "interstitial");
        response.addHeader(ResponseHeader.FULL_AD_TYPE.getKey(), "millennial_full");
        response.addHeader(ResponseHeader.NATIVE_PARAMS.getKey(), json);
        Robolectric.addPendingHttpResponse(response);

        subject.fetchAdForUrl("ignored_url");

        Map<String, String> paramsMap = new HashMap<String, String>();
        paramsMap.put(ResponseHeader.CUSTOM_EVENT_NAME.getKey(), "com.bizense.adatrix.android.MillennialInterstitial");
        paramsMap.put(ResponseHeader.CUSTOM_EVENT_DATA.getKey(), json);

        verify(interstitialView).load(eq(paramsMap));
    }

    @Test
    public void fetchAdForUrl_whenApiLevelIsAtLeastICS_shouldExecuteUsingAnExecutor() throws Exception {
        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", AndroidVersion.ICE_CREAM_SANDWICH.getApiLevel());
        AdFetchTask adFetchTask = mock(AdFetchTask.class);

        subject.fetchAdForUrl("some url");

        verify(adFetchTask).executeOnExecutor(eq(AdFetchTask.THREAD_POOL_EXECUTOR), eq("some url"));
        verify(adFetchTask, never()).execute(anyString());
    }

    @Test
    public void fetchAdForUrl_whenApiLevelIsBelowICS_shouldExecuteWithoutAnExecutor() throws Exception {
        Robolectric.Reflection.setFinalStaticField(Build.VERSION.class, "SDK_INT", AndroidVersion.HONEYCOMB_MR2.getApiLevel());        
        AdFetchTask adFetchTask = mock(AdFetchTask.class);

        subject.fetchAdForUrl("some url");

        verify(adFetchTask, never()).executeOnExecutor(any(Executor.class), anyString());
        verify(adFetchTask).execute(eq("some url"));
    }
}
